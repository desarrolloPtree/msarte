var LocalStrategy = require('passport-local').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

var User = require('../models/user');

module.exports = function(passport, configAuthEnv){

	passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    passport.use('local-login', new LocalStrategy({
	    	usernameField: 'username',
	    	passwordField: 'password',
	    	passReqToCallback: true
	    },
    	function(req, username, password, done){
            User.findOne({ 'local.username' :  username }, function(err, user){
                if(err){
                    return done(err);
                }
                if(!user){
                    return done(null, false, req.flash('loginMessage', 'Usuario o contraseña inválida.'));
                }
                user.validPassword(password, function(err, respuesta){
                    if(err){
                        return done(err);
                    } else {
                        if(!respuesta){
                            return done(null, false, req.flash('loginMessage', 'Usuario o contraseña inválida.'));
                        } else {
                            return done(null, user);
                        }
                    }
                });
            });
    	}
    ));

    passport.use('local-signup', new LocalStrategy({
                usernameField: 'username',
                emailField: 'email',
                nameField: 'name',
                companyField: 'company',
                passwordField: 'password',
                passReqToCallback: true
            },
            function(req, username, email, name, company, password, done){
              console.log(req);
                process.nextTick(function(){
                    User.findOne({'local.email': email}, function(err, user){
                        if(err){
                            return done(err);
                        }
                        if(user){
                            return done(null, false, req.flash('signupMessage', 'El usuario ya existe.'));
                        } else {
                            var newUser = new User();
                            newUser.local.username = username;
                            newUser.local.email = email;
                            newUser.local.name = name;
                            newUser.local.company = company;
                            newUser.local.password = password;

                            newUser.save(function(err){
                                if(err){
                                    throw err;
                                }
                                return done(null, newUser);
                            });
                        }
                    });
                });
            }
        )
    );

    passport.use(new GoogleStrategy({
            clientID: configAuthEnv.googleAuth.clientID,
            clientSecret: configAuthEnv.googleAuth.clientSecret,
            callbackURL: configAuthEnv.googleAuth.callbackURL
        },
        function(token, refreshToken, profile, done){
            process.nextTick(function() {
                User.findOne({'google.id' : profile.id}, function(err, user){
                    if(err){
                        return done(err);
                    }
                    if(user){
                        return done(null, user);
                    }
                    else {
                        var newUser = new User();
                        newUser.google.id = profile.id;
                        newUser.google.token = token;
                        newUser.google.name = profile.displayName;
                        newUser.google.email = profile.emails[0].value;
                        newUser.google.estatus = "A";
                        newUser.local = null;

                        newUser.save(function(err){
                            if(err){
                                throw err;
                            }
                            return done(null, newUser);
                        });
                    }
                });
            });
        }
    ));

    return passport;
}
var fs = require("fs");

//TODO Crear la carpeta de uploads para el inicio de la aplicación

module.exports = function(base, uploads, incidencias, callback) {
    checkDirectory(base, function(err, stats){
        if(err && err.code === "ENOENT"){
            createFolder(base, function(err){
                if(err){
                    throw "Error al crear la carpeta base."
                }
            });
        } else if(err){
            throw "Error al consultar la carpeta base."
        }
    });
    checkDirectory(uploads, function(err, stats){
        if(err && err.code === "ENOENT"){
            createFolder(uploads, function(err){
                if(err){
                    throw "Error al crear la carpeta base."
                }
                checkDirectory(incidencias, function(err, stats){
                    if(err && err.code === "ENOENT"){
                        createFolder(incidencias, function(err){
                            if(err){
                                throw "Error al crear la carpeta base."
                            }
                            callback();
                        });
                    } else if(err){
                        throw "Error al consultar la carpeta base."
                    } else {
                        callback();
                    }
                });
            });
        } else if(err){
            throw "Error al consultar la carpeta base."
        } else {
            checkDirectory(incidencias, function(err, stats){
                if(err && err.code === "ENOENT"){
                    createFolder(incidencias, function(err){
                        if(err){
                            throw "Error al crear la carpeta base."
                        }
                        callback();
                    });
                } else if(err){
                    throw "Error al consultar la carpeta base."
                } else {
                    callback();
                }
            });
        }
    });
}

function checkDirectory(path, callback){
    fs.stat(path, function(err, stats){
        if(err){
            stats = null;
        }
        callback(err, stats);
    });
}

function createFolder(path, callback){
    fs.mkdir(path, 0777, function(err){
        if(err){
            throw err;
        } else {
            callback(null);
        }
    });
}
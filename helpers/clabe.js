var clabe = {
    validacionCLABE: function(clabe){
        var productos = productosPesoClabe(clabe);
        var suma = sumaProductos(productos);
        var verif = 10 - (suma % 10);
        if(verif == clabe[17]){
            return true;
        }
        return false;
    }
}

module.exports = clabe;

var productosPesoClabe = function(clabe){
    var productos = [];
    for(var i = 0; i < 17; i ++){
        productos.push((clabe[i] * factorPeso(i)) % 10);
    }
    return productos;
}

var sumaProductos = function(productos){
    var suma = 0;
    for(var i=0; i < productos.length; i++){
        suma = productos[i] + suma;
    }
    return suma;
}

var factorPeso = function(digito){
    var res = digito % 3;
    if(res === 0){
        return 3;
    }
    if(res === 1){
        return 7;
    }
    if(res === 2){
        return 1;
    }
}
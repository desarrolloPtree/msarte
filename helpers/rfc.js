var strUtils = require("../helpers/strings.js");

var nombresInvalidos = [
    'MARIA',
    'JOSE',
    'DE',
    'DEL',
    'LA',
    'LOS',
    'LAS',
    'MA',
    'MA.'
];

var apellidosInvalidos = [
    'DE',
    'DEL',
    'LA',
    'LOS',
    'LAS'
];


var rfc = {
    crearRFC: function(fnacimiento, nombres, apsPaterno, apsMaterno){
        console.log(fnacimiento, nombres, apsPaterno, apsMaterno);
        return crearRFC(fnacimiento, nombres, apsPaterno, apsMaterno);
    },
    validarRFC: function(rfc, fnacimiento, nombres, apsPaterno, apsMaterno){
        console.log(fnacimiento, nombres, apsPaterno, apsMaterno);
        return (crearRFC(fnacimiento, nombres, apsPaterno, apsMaterno) === strUtils.aMayusculas(rfc))
    }
}

module.exports = rfc;

function isVowel(c) {
    return ['a', 'e', 'i', 'o', 'u'].indexOf(c.toLowerCase()) !== -1
}

var eliminarPalabras = function(arreglo, palabras){
    var nombre_resultado;
    var primerNombre = arreglo[0];
    var arrResultado = [];
    var invalida = false;
    
    arreglo.forEach(function(elemento){
        invalida = false;
        palabras.forEach(function(palabra){
            if(palabra === elemento){
                invalida = true;
                return;
            }
        });
        if(!invalida){
            arrResultado.push(elemento);
        }
    });
    if(arrResultado.length === 0){
        nombre_resultado = primerNombre;
    } else {
        nombre_resultado = arrResultado[0];
    }
    return nombre_resultado;
}

var crearRFC = function(fnacimiento, nombres, apsPaterno, apsMaterno){
    rfc =   prepararApPaterno(apsPaterno) +
            prepararApMaterno(apsMaterno) +
            prepararNombre(nombres) + 
            armarFecha(fnacimiento);
            console.log(rfc);
    return rfc;
}

var prepararNombre = function(nombres){
        nombres = strUtils.aMayusculas(nombres);
        nombres = strUtils.removeDiacritics(nombres);
        var nombre;
        var arrNombres = nombres.split(' ');
        if(arrNombres.length > 1){
            nombre = eliminarPalabras(arrNombres, nombresInvalidos);
        } else {
            nombre = arrNombres[0];
        }
        return nombre.substr(0,1);
    };

var prepararApPaterno = function (ap_paterno) {
        ap_paterno = strUtils.aMayusculas(ap_paterno);
        ap_paterno = strUtils.removeDiacritics(ap_paterno);
        var apellido;
        var primerLetra;
        var segundaLetra;
        
        var arrApellidos = ap_paterno.split(' ');
        if(arrApellidos.length > 1){
            apellido = eliminarPalabras(arrApellidos, apellidosInvalidos);
        } else {
            apellido = arrApellidos[0];
        }
        
        primerLetra = apellido.substr(0,1);
        
        var arrLetras = apellido.split('');
        arrLetras.some(function(letra){
            if(isVowel(letra)){
                segundaLetra = letra;
                return true;
            }
        });
        return primerLetra + segundaLetra;
    };
    
var prepararApMaterno = function (ap_materno) {
        ap_materno = strUtils.aMayusculas(ap_materno);
        ap_materno = strUtils.removeDiacritics(ap_materno);
        var apellido;
        var primerLetra;
        
        var arrApellidos = ap_materno.split(' ');
        if(arrApellidos.length > 1){
            apellido = eliminarPalabras(arrApellidos, apellidosInvalidos);
        } else {
            apellido = arrApellidos[0];
        }
        
        primerLetra = apellido.substr(0,1);
        
        return primerLetra;
    };
    
var armarFecha = function(fnacimiento){
        var arrfn = fnacimiento.split("/");
        return arrfn[0].substr(2, 2) + strUtils.pad(arrfn[1], 2) + strUtils.pad(arrfn[2], 2);
    };
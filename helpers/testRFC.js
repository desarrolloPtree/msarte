var rfcUtils = require("./rfc.js");

(function(){
    // console.log("########## Test prepararNombre ########");
    // console.log("++++++Prueba 1");
    // var nombres = "Julio César";
    // console.log(nombres);
    // var nombre = rfcUtils.prepararNombre(nombres);
    // console.log("******Resultado:", nombre);
    // if(nombre === "J"){
    //     console.log("------- Prueba Exitosa");
    // } else{
    //     console.log("------- Prueba Fallida");
    // }
    
    
    // console.log("++++++Prueba 2");
    // var nombres = "María Del Carmen";
    // console.log(nombres);
    // var nombre = rfcUtils.prepararNombre(nombres);
    // console.log("******Resultado:", nombre);
    // if(nombre === "C"){
    //     console.log("------- Prueba Exitosa");
    // } else{
    //     console.log("------- Prueba Fallida");
    // }
    
    
    // console.log("++++++Prueba 3");
    // var nombres = "José María";
    // console.log(nombres);
    // var nombre = rfcUtils.prepararNombre(nombres);
    // console.log("******Resultado:", nombre);
    // if(nombre === "J"){
    //     console.log("------- Prueba Exitosa");
    // } else{
    //     console.log("------- Prueba Fallida");
    // }
    
    // console.log("########## Test prepararNombre ########");
    // console.log("++++++Prueba 1");
    // var apsPaterno = "De La Cruzada";
    // console.log(apsPaterno);
    // var apPaterno = rfcUtils.prepararApPaterno(apsPaterno);
    // console.log("******Resultado:", apPaterno);
    // if(apPaterno === "CU"){
    //     console.log("------- Prueba Exitosa");
    // } else{
    //     console.log("------- Prueba Fallida");
    // }
    
    // console.log("++++++Prueba 2");
    // var apsPaterno = "Sánchez Pérez";
    // console.log(apsPaterno);
    // var apPaterno = rfcUtils.prepararApPaterno(apsPaterno);
    // console.log("******Resultado:", apPaterno);
    // if(apPaterno === "SA"){
    //     console.log("------- Prueba Exitosa");
    // } else{
    //     console.log("------- Prueba Fallida");
    // }
    
    // console.log("++++++Prueba 3");
    // var apsPaterno = "Chávez";
    // console.log(apsPaterno);
    // var apPaterno = rfcUtils.prepararApPaterno(apsPaterno);
    // console.log("******Resultado:", apPaterno);
    // if(apPaterno === "CA"){
    //     console.log("------- Prueba Exitosa");
    // } else{
    //     console.log("------- Prueba Fallida");
    // }
    
    // console.log("########## Test prepararNombre ########");
    // console.log("++++++Prueba 1");
    // var apsMaterno = "De La Cruzada";
    // console.log(apsMaterno);
    // var apMaterno = rfcUtils.prepararApMaterno(apsMaterno);
    // console.log("******Resultado:", apMaterno);
    // if(apMaterno === "C"){
    //     console.log("------- Prueba Exitosa");
    // } else{
    //     console.log("------- Prueba Fallida");
    // }
    
    // console.log("++++++Prueba 2");
    // var apsMaterno = "Salinas";
    // console.log(apsMaterno);
    // var apMaterno = rfcUtils.prepararApMaterno(apsMaterno);
    // console.log("******Resultado:", apMaterno);
    // if(apMaterno === "S"){
    //     console.log("------- Prueba Exitosa");
    // } else{
    //     console.log("------- Prueba Fallida");
    // }
    
    // console.log("++++++Prueba 1");
    // var apsMaterno = "Huerta";
    // console.log(apsMaterno);
    // var apMaterno = rfcUtils.prepararApMaterno(apsMaterno);
    // console.log("******Resultado:", apMaterno);
    // if(apMaterno === "H"){
    //     console.log("------- Prueba Exitosa");
    // } else{
    //     console.log("------- Prueba Fallida");
    // }
    
    console.log("########## Test crearRFC ########");
    console.log("++++++ Prueba 1");
    var fnacimiento = "1980/9/16";
    var nombres = "Julio César";
    var apsPaterno = "Sánchez";
    var apsMaterno = "Salinas"
    var rfc = rfcUtils.crearRFC(fnacimiento, nombres, apsPaterno, apsMaterno);
    console.log("******Resultado:", rfc);
    if(rfc === "SASJ800916"){
        console.log("------- Prueba Exitosa");
    } else{
        console.log("------- Prueba Fallida");
    }
    
    console.log("++++++ Prueba 2");
    var fnacimiento = "1980/9/16";
    var nombres = "Julio César";
    var apsPaterno = "Sánchez";
    var apsMaterno = "Salinas"
    var valido = rfcUtils.validarRFC("SASJ800916", fnacimiento, nombres, apsPaterno, apsMaterno);
    console.log("******Resultado:", valido);
    if(valido){
        console.log("------- Prueba Exitosa");
    } else{
        console.log("------- Prueba Fallida");
    }
    
})();
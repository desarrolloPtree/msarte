var express = require("express");
var General = require("../service/general");
var File = require("../service/file");
//var ParserGenerico = require("../app_modules/parser/ParserGenerico");
//var incidenciaConstructor = require("../models/incidenciaConstructor")
var router = express.Router();

router.get('/import', function(req, res, next){
    if(req.isAuthenticated()){
        res.render('incidencias/import', { submenu: 'importIncidencias'});
    } else {
        res.redirect('/');
    }
});

router.get('/log', function(req, res, next){
    res.redirect('/');
});

router.post('/import/create', function(req, res, next){
    General.uploadFile(req, res, function(err){
        if(err){
            return res.send({code: -1, message: "Error al subir el archivo."})
        }
        var file = req.files[0];
        
        var empresa = {
            opt: req.body.empresa_opt,
            desc: req.body.empresa_desc,
        }
        var centro = {
            opt: req.body.centro_opt,
            desc: req.body.centro_desc,
        }
        
        var fileName = file.originalname;
        var aplicacion = "incidencias/";
        
        File.moverFileImportar(fileName, aplicacion, empresa.opt, centro.opt, function(err){
            if(err){console.log(err)
            console.log(err);
                return res.send({code: -1, message: "Error al subir el archivo.", data: err});
            }
            var validator = ParserGenerico.validator;
            var asignator = ParserGenerico.asignator;
            var path = "./base/" + aplicacion + empresa.opt + "/" + centro.opt + "/" + file.originalname;
            
            ParserGenerico.parse(path, incidenciaConstructor, asignator, validator, function(errores, data){
                if(errores){
                    console.log(errores);
                    return res.send({ code: -1, errores: errores, empleados_error: data });
                }
                if(data === undefined || data === null || data.length <= 0){
                    return res.send({ code: -1, message: "El archivo no contiene registros." });
                }
                console.log(data);
                return res.send({code: 0, message: "Se subió el archivo correctamente."});
            });
            
            
        });
    });
    
});

module.exports = router;
var express = require("express");
var router = express.Router();

router.get('/altausuario', function(req, res, next){
    res.render('ctrlempresas/altaUsuario', {submenu: 'altaUsuario'})
});

router.get('/altacliente', function(req, res, next){
    res.render('ctrlempresas/altaCliente', {submenu: 'altaCliente'})
});

router.get('/listclientes', function(req, res, next){
    res.render('ctrlempresas/listClientes', {submenu: 'listadoClientes'})
});

router.get('/ctrlperfiles', function(req, res, next){
    res.render('ctrlempresas/ctrlPerfiles', {submenu: 'controlPerfiles'})
});

router.get('/ctrlusuarios', function(req, res, next){
    res.render('ctrlempresas/ctrlUsuarios', {submenu: 'controlUsuarios'})
});

module.exports = router;
//rutas
var express = require('express');
var router = express.Router();
var servicioReportes = require('../service/reportes');

router.get('/', function(req, res, next){
   res.render('/reportes');
});

module.exports = router;
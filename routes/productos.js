//rutas
var express = require('express');
var router = express.Router();
var servicioProductos = require('../service/productos');

router.get('/', function(req, res, next){
   res.render('productos/productos');
});

router.get('/entrada', function(req, res, next){
   res.render('productos/productosEntrada');
});

router.get('/api/listaProductos', function(req, res, next) {
    servicioProductos.mostrarProductos(function(error, listaProductos){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaProductos});
        
    });   
});

router.post('/api/nuevoProducto', function(req, res, next){
    var producto = req.body.nuevoProducto;   
    servicioProductos.nuevoProducto(producto, function(error, productoGuardado){
        if(error){
            return res.send({code:-1,message:"Error en la insercion",data:null});
        }
        
            return res.send({code:0,message:"Guardado Exitosamente",data:productoGuardado});
    });
});

router.put('/api/update', function(req, res, next){
    var productoId = req.body.id;
    var productoModificar = req.body.modificarProducto;
    servicioProductos.update(productoId,productoModificar, function(error, productoUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:productoUpdated});
    });
});

router.get('/api/listaFranquicias', function(req, res, next) {
    servicioProductos.mostrarFranquicias(function(error, listaFranquicias){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaFranquicias});
        
    });   
});

router.put('/api/updateStock', function(req, res, next){
    var productoId = req.body.id;
    var productoModificar = req.body.modificarProducto;
    servicioProductos.updateStock(productoId,productoModificar, function(error, productoUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:productoUpdated});
    });
});

router.post('/api/delete', function(req, res, next) {
    var productoId = req.body.id;
    servicioProductos.delete(productoId, function(err, deleted){
        if(err){
            res.send({
                code: -1,
                mensaje: "Ocurrió un error al eliminar el producto."
            })
        } else {
            res.send({
                code: 0,
                mensaje: "Se eliminó correctamente el producto."
            })
        }
    });
});


module.exports = router;
var express = require('express');
var router = express.Router();

module.exports = function(passport){

	router.post('/local', passport.authenticate('local-login', {
		successRedirect : '/',
		failureRedirect : '/',
		failureFlash : true
	}));

	router.get('/google', passport.authenticate('google', { scope : ['profile', 'email'] }));

	router.get('/google/callback',passport.authenticate('google', {failureRedirect: '/'})
		,function(req, res) {
			res.redirect('https://sites.google.com/a/proenfar.com/intranet-proenfar/'); //poner url proenfar
		}
	);

	router.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});

	return router;
}
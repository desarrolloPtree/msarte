//rutas
var express = require('express');
var router = express.Router();
//var servicioNuevaSolicitud = require('../service/clientes');

router.get('/', function(req, res, next){
   res.render('presupuesto/presupuesto');
});

router.get('/proyeccion', function(req, res, next){
   res.render('presupuesto/proyeccion');
});

router.get('/comparativo', function(req, res, next){
   res.render('presupuesto/comparativo');
});

router.get('/participacion', function(req, res, next){
   res.render('presupuesto/participacion');
});

module.exports = router;
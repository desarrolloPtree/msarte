var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
	if(req.isAuthenticated()){
		res.redirect('/dashboard/');
	} else {
		res.render('login/index');
	}
});

module.exports = router;


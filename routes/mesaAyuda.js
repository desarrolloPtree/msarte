//rutas
var express = require('express');
var router = express.Router();
var servicioNuevaSolicitud = require('../service/solicitudAyuda');

router.get('/', function(req, res, next){
   res.render('mesaAyuda/mesaAyuda');
});

router.get('/nuevaSolicitud', function(req, res, next){
   res.render('mesaAyuda/nuevaSolicitud');
});

router.post('/api/nuevaSolicitud', function(req, res, next){
    var solicitudAyuda = req.body.nuevaSolicitudAyuda;   
    servicioNuevaSolicitud.nuevaSolicitud(solicitudAyuda, function(error, solicitudGuardada){
        if(error){
            return res.send({code:-1,message:"Error en la insercion",data:null});
        }
        
            return res.send({code:0,message:"Guardado Exitosamente",data:solicitudGuardada});
    });
});

router.get('/api/listaSolicitudes', function(req, res, next) {
    servicioNuevaSolicitud.mostrarSolicitudes(function(error, listaSolicitudes){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaSolicitudes});
        
    });   
});

router.post('/api/delete', function(req, res, next) {
    var solicitudAyudaId = req.body.id;
    servicioNuevaSolicitud.delete(solicitudAyudaId, function(err, deleted){
        if(err){
            res.send({
                code: -1,
                mensaje: "Ocurrió un error al eliminar la solicitud."
            })
        } else {
            res.send({
                code: 0,
                mensaje: "Se eliminó correctamente la solicitud."
            })
        }
    });
});

router.put('/api/update', function(req, res, next){
    var solicitudAyudaId = req.body.id;
    var solicitudModificar = req.body.modificarSolicitud;
    servicioNuevaSolicitud.update(solicitudAyudaId,solicitudModificar, function(error, solicitudUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:solicitudUpdated});
    });
});

router.put('/api/updateAprobar', function(req, res, next){
    var solicitudAyudaId = req.body.id;
    var solicitudModificar = req.body.modificarSolicitud;
    servicioNuevaSolicitud.updateAprobar(solicitudAyudaId,solicitudModificar, function(error, solicitudUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:solicitudUpdated});
    });
});

router.put('/api/updateRechazar', function(req, res, next){
    var solicitudAyudaId = req.body.id;
    var solicitudModificar = req.body.modificarSolicitud;
    servicioNuevaSolicitud.updateRechazar(solicitudAyudaId,solicitudModificar, function(error, solicitudUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:solicitudUpdated});
    });
});


module.exports = router;
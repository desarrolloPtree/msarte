//rutas
var express = require('express');
var router = express.Router();
var servicioTalleres = require('../service/taller');

router.get('/', function(req, res, next){
   res.render('taller/taller');
});

router.get('/api/listaTalleres', function(req, res, next) {
    servicioTalleres.mostrarTalleres(function(error, listaTalleres){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaTalleres});
        
    });   
});
router.get('/api/listaProductos', function(req, res, next) {
    servicioTalleres.mostrarProductos(function(error, listaProductos){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaProductos});
        
    });   
});

router.get('/api/listaMaestros', function(req, res, next) {
    servicioTalleres.mostrarMaestros(function(error, listaMaestros){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaMaestros});
        
    });   
});

router.post('/api/nuevoTaller', function(req, res, next){
    var taller = req.body.nuevoTaller;   
    servicioTalleres.nuevoTaller(taller, function(error, tallerGuardado){
        if(error){
            return res.send({code:-1,message:"Error en la insercion",data:null});
        }
        
            return res.send({code:0,message:"Guardado Exitosamente",data:tallerGuardado});
    });
});

router.put('/api/update', function(req, res, next){
    var tallerId = req.body.id;
    var tallerModificar = req.body.modificarTaller;
    servicioTalleres.update(tallerId,tallerModificar, function(error, tallerUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:tallerUpdated});
    });
});

router.post('/api/delete', function(req, res, next) {
    var tallerId = req.body.id;
    servicioTalleres.delete(tallerId, function(err, deleted){
        if(err){
            res.send({
                code: -1,
                mensaje: "Ocurrió un error al eliminar el taller."
            })
        } else {
            res.send({
                code: 0,
                mensaje: "Se eliminó correctamente el taller."
            })
        }
    });
});

module.exports = router;
//rutas
var express = require('express');
var router = express.Router();
var servicioQRAsistencia = require('../service/QRAsistencia');

router.get('/', function(req, res, next){
   res.render('QRAsistencia/QRAsistencia');
});

router.get('/api/listaAlumnos', function(req, res, next) {
    servicioQRAsistencia.mostrarAlumnos(function(error, listaAlumnos){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaAlumnos});
        
    });   
});

router.get('/api/listaAsistencia', function(req, res, next) {
    servicioQRAsistencia.mostrarAsistencia(function(error, listaAsistencia){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaAsistencia});
        
    });   
});

router.post('/api/nuevaAsistencia', function(req, res, next){
    var asistencia = req.body.nuevaAsistencia;   
    servicioQRAsistencia.nuevaAsistencia(asistencia, function(error, asistenciaGuardada){
        if(error){
            return res.send({code:-1,message:"Error en la insercion",data:null});
        }
        
            return res.send({code:0,message:"Guardado Exitosamente",data:asistenciaGuardada});
    });
});

router.put('/api/update', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioQRAsistencia.update(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});
module.exports = router;
//rutas
var express = require('express');
var router = express.Router();
var servicioMobiliario = require('../service/mobiliario');

router.get('/', function(req, res, next){
   res.render('mobiliario/mobiliario');
});

router.get('/entrada', function(req, res, next){
   res.render('mobiliario/mobiliarioEntrada');
});

router.get('/api/listaMobiliario', function(req, res, next) {
    servicioMobiliario.mostrarMobiliario(function(error, listaMobiliario){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaMobiliario});
        
    });   
});

router.post('/api/nuevoMobiliario', function(req, res, next){
    var mobiliario = req.body.nuevoMobiliario;   
    servicioMobiliario.nuevoMobiliario(mobiliario, function(error, mobiliarioGuardado){
        if(error){
            return res.send({code:-1,message:"Error en la insercion",data:null});
        }
        
            return res.send({code:0,message:"Guardado Exitosamente",data:mobiliarioGuardado});
    });
});

router.put('/api/update', function(req, res, next){
    var mobiliarioId = req.body.id;
    var mobiliarioModificar = req.body.modificarMobiliario;
    servicioMobiliario.update(mobiliarioId,mobiliarioModificar, function(error, mobiliarioUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:mobiliarioUpdated});
    });
});

router.put('/api/updateStock', function(req, res, next){
    var mobiliarioId = req.body.id;
    var mobiliarioModificar = req.body.modificarMobiliario;
    servicioMobiliario.updateStock(mobiliarioId,mobiliarioModificar, function(error, mobiliarioUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:mobiliarioUpdated});
    });
});

router.post('/api/delete', function(req, res, next) {
    var mobiliarioId = req.body.id;
    servicioMobiliario.delete(mobiliarioId, function(err, deleted){
        if(err){
            res.send({
                code: -1,
                mensaje: "Ocurrió un error al eliminar el producto."
            })
        } else {
            res.send({
                code: 0,
                mensaje: "Se eliminó correctamente el producto."
            })
        }
    });
});

router.get('/api/listaFranquicias', function(req, res, next) {
    servicioMobiliario.mostrarFranquicias(function(error, listaFranquicias){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaFranquicias});
        
    });   
});

module.exports = router;
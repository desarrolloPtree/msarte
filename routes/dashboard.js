var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next){
   res.render('dashboard/dashboard');
   console.log('dashboard');
});

router.get('/compania', function(req, res, next){
   res.render('dashboard/compania');
   console.log('compania');
});

module.exports = router;
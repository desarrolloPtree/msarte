var express = require('express');
var router = express.Router();

var General = require('../service/general');

router.get('/api/nominas', function(req, res, next){
   var nominas = General.consultaNominas(function(nominas){
       res.send(nominas);
   });
});

module.exports = router;


//rutas
var express = require('express');
var router = express.Router();
var servicioFranquicias = require('../service/franquicias');

router.get('/', function(req, res, next){
   res.render('franquicias/franquicias');
});

router.get('/api/listaFranquicias', function(req, res, next) {
    servicioFranquicias.mostrarFranquicias(function(error, listaFranquicias){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaFranquicias});
        
    });   
});

router.post('/api/nuevaFranquicia', function(req, res, next){
    var franquicia = req.body.nuevaFranquicia;   
    servicioFranquicias.nuevaFranquicia(franquicia, function(error, franquiciaGuardada){
        if(error){
            return res.send({code:-1,message:"Error en la insercion",data:null});
        }
        
            return res.send({code:0,message:"Guardado Exitosamente",data:franquiciaGuardada});
    });
});

router.put('/api/update', function(req, res, next){
    var franquiciaId = req.body.id;
    var franquiciaModificar = req.body.modificarFranquicia;
    servicioFranquicias.update(franquiciaId,franquiciaModificar, function(error, franquiciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:franquiciaUpdated});
    });
});


module.exports = router;
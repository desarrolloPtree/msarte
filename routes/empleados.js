var express = require("express");
var router = express.Router();
var Empleado = require('../service/empleado');
var Importacion = require("../service/importacion");

var mensajesError = {
    "salarios.mensual": "Salario Mensual",
    'salarios.base':    "Salario Base",
    'banco.clabe':      "Clabe Bancaria",
    'salarios.sdi':     "SDI",
    'sexo':             "Sexo"
}

var traducirErrores = function(errores){
    var resultado = [];
    for(var error in errores) {
        resultado.push(mensajesError[error]);
    }
    return resultado;
}

router.get('/list', function(req, res, next){
    res.render('empleados/list', { submenu: 'listEmpleados'});
});

router.get('/alta', function(req, res, next){
    res.render('empleados/new', {submenu: 'altaEmpleados'});
});

router.post('/api/create', function(req, res, next){
    var empleado = req.body.empleado;
    var solicitud = {
        idUsuario: req.user.id,
        fsolicitud: Date.now(),
    }
    
    Empleado.create(empleado, solicitud, function(err, empleado){
        console.log(err);
        if(err){
            if(err.code === -2){
                var errores = traducirErrores(err.error.errors);
                res.send({code: -2, mensaje: err.message, detalle: errores});
            } else {
                res.send({code: -1, mensaje: err.message});
            }
        } else {
            res.send({code: 0, mensaje: "Alta efectuada correctamente."});
        }
    });
});

router.post('/api/filter', function(req, res, next) {
    var filter = {$and : [{"id":{$ne:""}}]};
    var orEnterprises = {$or: [{}]};
    var orCostCenters = {$or: [{}]};
    var orPayrollTypes = {$or: [{}]};
    
    if (req.body.selectedEnterprises.length > 0) {orEnterprises.$or = []};
    for (var i = 0; i < req.body.selectedEnterprises.length; i++) {
        orEnterprises.$or.push({"empresa.desc": req.body.selectedEnterprises[i].desc});
    }
    if (req.body.selectedCostCenters.length > 0) {orCostCenters.$or = []};
    for (var i = 0; i < req.body.selectedCostCenters.length; i++) {
        orCostCenters.$or.push({"centro.desc": req.body.selectedCostCenters[i].desc});
    }
    if (req.body.selectedPayrollTypes.length > 0) {orPayrollTypes.$or = []};
    for (var i = 0; i < req.body.selectedPayrollTypes.length; i++) {
        orPayrollTypes.$or.push({"tipoNomina.desc": req.body.selectedPayrollTypes[i].desc});
    }
    
    var nombre = req.body.txtName;
    
    var filter2 = {
      
    }
    filter2 = { $and: [
        { $or: [
            {'nombre.nombre' : {'$regex': nombre, $options: "i"}}, 
            {'nombre.appaterno' : {'$regex': nombre, $options: "i"}}, 
            {'nombre.apmaterno' : {'$regex': nombre, $options: "i"}}] 
        }
        ]
    }
    filter2.$and.push(orEnterprises);
    filter2.$and.push(orCostCenters);
    filter2.$and.push(orPayrollTypes);
    
    console.log(JSON.stringify(filter2));
    Empleado.listByFilters(filter2, function(err, empleados){
        if(err){
            console.log(err);
            res.send({code: -1, mensaje: "Error al buscar empleados."});
        } else {
            res.send({code:0, data: empleados});
        }
    });
});

router.get('/api/list', function(req, res, next) {
    var empresa = req.body.empresa;
    Empleado.listByEmpresa(empresa, function(err, empleados){
        if(err){
            res.send({code: -1, mensaje: "Error al buscar empleados."});
        } else {
            res.send({code: 0, data: empleados});
        }
    });
});

router.get('/import', function(req, res, next) {
   res.render('empleados/import', {submenu: 'importEmpleados'});
});

router.get('/logimport', function(req, res, next) {
    Importacion.findByUser(req.user.id, function(err, importaciones){
        if(err){
            res.send({code: -1, mensaje: "Error al buscar importaciones."});
        } else {
            res.render('empleados/logimport', {importaciones: importaciones, submenu: 'logimportEmpleados'});
        }
    });
});

router.post('/api/delete', function(req, res, next) {
    var empleadoId = req.body.id;
    Empleado.delete(empleadoId, function(err, deleted){
        if(err){
            res.send({
                code: -1,
                mensaje: "Ocurrió un error al eliminar el empleado."
            })
        } else {
            res.send({
                code: 0,
                mensaje: "Se eliminó correctamente el empleado."
            })
        }
    });
});

router.get('/api/find/:id', function(req, res, next) {
    var id = req.params.id;
    Empleado.findById(id, function(err, empleado){
        if(err){
            res.send({
                code: -1,
                mensaje: "Ocurrió un error al obtener el empleado."
            })
        } else {
            res.send({
                code: 0,
                mensaje: "",
                empleado: empleado,
            })
        }
    });
});

router.get('/edit/:id', function(req, res, next){
    var id = req.params.id;
    res.render('empleados/edit', {id: id});
});

router.put('/api/update', function(req, res, next){
    var empleado = req.body.empleado;
    Empleado.update(empleado, function(err, empleadoUpdated){
        if(err){
            if(err.name === 'ValidationError'){
                res.send({code: -1, mensaje: err.message});
            } else {
                res.send({code: -1, mensaje: "Hubo un problema al actulizar el empleado."});
            }
        } else {
            res.send({code: 0, mensaje: "Se realizo la actualización del empleado."})
        }
    });
});

module.exports = router;
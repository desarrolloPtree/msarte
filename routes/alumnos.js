//rutas
var express = require('express');
var router = express.Router();
var servicioAlumnos = require('../service/alumnos');
var servicioProductos = require('../service/productos');

router.get('/', function(req, res, next){
   res.render('alumnos/alumnos');
});

router.get('/api/listaAlumnos', function(req, res, next) {
    servicioAlumnos.mostrarAlumnos(function(error, listaAlumnos){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaAlumnos});
        
    });   
});

router.get('/api/listaFranquicias', function(req, res, next) {
    servicioAlumnos.mostrarFranquicias(function(error, listaFranquicias){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaFranquicias});
        
    });   
});

router.get('/api/listaProductos', function(req, res, next) {
    servicioProductos.mostrarProductos(function(error, listaProductos){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaProductos});
        
    });   
});

router.post('/api/nuevoAlumno', function(req, res, next){
    var alumno = req.body.nuevoAlumno;   
    servicioAlumnos.nuevoAlumno(alumno, function(error, alumnoGuardado){
        if(error){
            return res.send({code:-1,message:"Error en la insercion",data:null});
        }
        
            return res.send({code:0,message:"Guardado Exitosamente",data:alumnoGuardado});
    });
});
router.post('/api/nuevoPago', function(req, res, next){
    var pago = req.body.nuevoPago;   
    servicioAlumnos.nuevoPago(pago, function(error, pagoGuardado){
        if(error){
            return res.send({code:-1,message:"Error en la insercion",data:null});
        }
        
            return res.send({code:0,message:"Guardado Exitosamente",data:pagoGuardado});
    });
});
router.get('/api/listaTalleres', function(req, res, next) {
    servicioAlumnos.mostrarTalleres(function(error, listaTalleres){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaTalleres});
        
    });   
});

router.put('/api/update', function(req, res, next){
    var alumnoId = req.body.id;
    var alumnoModificar = req.body.modificarAlumno;
    servicioAlumnos.update(alumnoId,alumnoModificar, function(error, alumnoUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:alumnoUpdated});
    });
});

router.put('/api/updateBaja', function(req, res, next){
    var alumnoId = req.body.id;
    var alumnoModificar = req.body.modificarAlumno;
    servicioAlumnos.updateBaja(alumnoId,alumnoModificar, function(error, alumnoUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:alumnoUpdated});
    });
});

router.put('/api/updateActivar', function(req, res, next){
    var alumnoId = req.body.id;
    var alumnoModificar = req.body.modificarAlumno;
    servicioAlumnos.updateActivar(alumnoId,alumnoModificar, function(error, alumnoUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:alumnoUpdated});
    });
});

router.put('/api/updateFirma', function(req, res, next){
    var alumnoId = req.body.id;
    var alumnoModificar = req.body.modificarAlumno;
    servicioAlumnos.updateFirma(alumnoId,alumnoModificar, function(error, alumnoUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:alumnoUpdated});
    });
});

module.exports = router;
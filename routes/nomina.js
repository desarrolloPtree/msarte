var express = require('express');
var router = express.Router();

var odoo = require('../service/odoo');

router.get('/api', function(req, res, next){
   var cencostos = odoo.consultaCCostos(6, function(cencostos){
       res.send(cencostos);
   });
});

module.exports = router;
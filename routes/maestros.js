//rutas
var express = require('express');
var router = express.Router();
var servicioMaestros = require('../service/maestros');

router.get('/', function(req, res, next){
   res.render('maestros/maestros');
});

router.get('/api/listaFranquicias', function(req, res, next) {
    servicioMaestros.mostrarFranquicias(function(error, listaFranquicias){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaFranquicias});
        
    });   
});

router.get('/api/listaMaestros', function(req, res, next) {
    servicioMaestros.mostrarMaestros(function(error, listaMaestros){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaMaestros});
        
    });   
});
router.get('/api/listaTalleres', function(req, res, next) {
    servicioMaestros.mostrarTalleres(function(error, listaTalleres){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaTalleres});
        
    });   
});
router.post('/api/nuevoMaestro', function(req, res, next){
    var maestro = req.body.nuevoMaestro;   
    servicioMaestros.nuevoMaestro(maestro, function(error, maestroGuardado){
        if(error){
            return res.send({code:-1,message:"Error en la insercion",data:null});
        }
        
            return res.send({code:0,message:"Guardado Exitosamente",data:maestroGuardado});
    });
});

router.put('/api/update', function(req, res, next){
    var maestroId = req.body.id;
    var maestroModificar = req.body.modificarMaestro;
    servicioMaestros.update(maestroId,maestroModificar, function(error, maestroUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:maestroUpdated});
    });
});

router.put('/api/updateBaja', function(req, res, next){
    var maestroId = req.body.id;
    var maestroModificar = req.body.modificarMaestro;
    servicioMaestros.updateBaja(maestroId,maestroModificar, function(error, maestroUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:maestroUpdated});
    });
});

router.put('/api/updateActivar', function(req, res, next){
    var maestroId = req.body.id;
    var maestroModificar = req.body.modificarMaestro;
    servicioMaestros.updateActivar(maestroId,maestroModificar, function(error, maestroUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:maestroUpdated});
    });
});


module.exports = router;
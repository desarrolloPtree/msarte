var express = require('express');
var User = require('../service/user');
var router = express.Router();

router.get('/editarUsuarios', function(req, res, next){
   res.render('users/editarUsuarios');
});

router.get('/profile', function(req, res, next) {
	if(req.isAuthenticated()){
		var user = req.user;
			if(user['local'].email != undefined){
				user = user.local;
			} else{
				user = user.google;
			}
			console.log(user);
		res.render('users/profile', { message: req.flash('loginMessage'), user: user });
	} else {
		res.render('users/loginlocal', { message: req.flash('loginMessage') });
	}
});


router.get('/new', function(req, res, next){
	res.render('users/new', { message: req.flash('loginMessage') });
});


router.post('/api/create', function(req, res, next) {
    var user = req.body.user;
    var newuser = req.body.user;
    User.create(newuser, function(err, userCreated){
		if(err){
			res.send({ code: -1, mensaje: "Error en alta." });
		} else if (userCreated === null){
			res.send({ code: -2, mensaje: "Usuario ya existente." });
		} else {
			res.send({ code: 0, mensaje: "Usuario creado exitosamente." });
		}
	});
});


router.post('/api/updatepass', function(req, res, next) {
	console.log("--------------------------");
    var password = req.body.password;
    var user = req.user;
    console.log("-----------", user._id, password);
    User.updatePassword(user._id, password, function(err){
    	if(err){
    		res.send({code: -1, mensaje:"Ocurrió un problema al actualizar el password."});
    	} else {
    		req.logout();
    		res.send({code: 0, mensaje: "Contraseña actualizada, favor de iniciar sesión nuevamente."});
    	}
    });
});

router.get('/api/list', function(req, res, next) {
    User.listUsers(function(err, users){
    	if(err){
    		res.send({code: -1, mensaje: "Ocurrió un error en la búsqueda de usuarios."})
    	} else {
    		res.send({code: 0, mensaje: "Búsqueda corercta.", users, users})
    	}
    });
});

router.get('/api/listaUsuarios', function(req, res, next) {
    User.listUsers(function(err, users){
    	if(err){
    		res.send({code: -1, mensaje: "Ocurrió un error en la búsqueda de usuarios."})
    	} else {
    		res.send({code: 0, mensaje: "Búsqueda corercta.", data:users})
    	}
    });
});

router.put('/api/updateRol', function(req, res, next){
    var rolEmail = req.body.email;
    var rolModificarUsuario = req.body.modificarRolUsuario;
    var rolRol = req.body.modificarRol;
    User.updateRol(rolEmail, rolModificarUsuario, rolRol, function(error, rolUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Rol actualizado Exitosamente",data:rolUpdated});
    });
});

router.put('/api/updateRolG', function(req, res, next){
    var rolEmail = req.body.email;
    var rolModificarUsuario = req.body.modificarRolUsuario;
    var rolRol = req.body.modificarRol;
    User.updateRolG(rolEmail, rolModificarUsuario, rolRol, function(error, rolUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Rol actualizado Exitosamente",data:rolUpdated});
    });
});

router.post('/api/cambiarpass', function(req, res, next) {
	console.log("--------------------------");
    var password = req.body.password;
    var email = req.body.email;
    console.log("-----------", email, password);
    User.cambiarPassword(email, password, function(err){
    	if(err){
    		res.send({code: -1, mensaje:"Ocurrió un problema al actualizar el password."});
    	} else {
    		res.send({code: 0, mensaje: "Contraseña actualizada, favor de iniciar sesión nuevamente."});
    	}
    });
});

router.post('/api/cambiarpermisos', function(req, res, next) {
	console.log("--------------------------");
    var email = req.body.email;
    var idFranq = req.body.idFranq;
    var nombreFranq = req.body.nombreFranq;
    console.log("-----------", email, idFranq);
    User.cambiarPermisos(email, idFranq, nombreFranq, function(err){
    	if(err){
    		res.send({code: -1, mensaje:"Ocurrió un problema al actualizar los permisos."});
    	} else {
    		res.send({code: 0, mensaje: "Permiso actualizado"});
    	}
    });
});

router.get('/api/listaFranquicias', function(req, res, next) {
    User.mostrarFranquicias(function(error, listaFranquicias){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaFranquicias});
        
    });   
});

router.post('/api/delete', function(req, res, next) {
    var email = req.body.email;
    User.delete(email, function(err, deleted){
        if(err){
            res.send({
                code: -1,
                mensaje: "Ocurrió un error al eliminar el producto."
            })
        } else {
            res.send({
                code: 0,
                mensaje: "Se eliminó correctamente el producto."
            })
        }
    });
});

module.exports = router;
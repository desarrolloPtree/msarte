var express = require("express");
var router = express.Router();
var googleapis = require('googleapis');
var driveService = require("../service/drive");
var General = require("../service/general");
var File = require("../service/file");
//var Parser = require("../app_modules/parser/parser");
var Validador = require("../models/empleadoValidaciones");
var SolicitudModel = require('../models/solicitud');
var Empleado = require('../service/empleado');
var fs = require("fs");


var Importacion = require('../service/importacion');

var key = require('../config/INEXmx-c7744cde5362.json');
var jwtClient = new googleapis.auth.JWT(
    key.client_email,
    null,
    key.private_key,
    ['https://www.googleapis.com/auth/drive'],
    null
);



router.get('/api/last', function(req, res, next) {
    Importacion.findLast(req.user.id, function(err, importacion){
        if(err){
            res.send({code:-1, mensaje: "Error al obtener última importación."});
        } else {
            res.send({code:0, importacion: importacion});
        }
    });
});

router.get('/api/download/:idFile/:nameFile', function(req, res, next) {
    var idFile = req.params.idFile;
    driveService.downloadFile(jwtClient, idFile, res);
});


router.post('/api/create/', function(req, res, next){
    
    General.uploadFile(req, res, function(err){
        if(err){
            console.log(err);
            res.send({code: -2, mensaje: "Ha ocurrido un error al exportar el archivo."});
        }else{
            var file = req.files[0];
    
            var empresa = {
                opt: req.body.empresa_opt,
                desc: req.body.empresa_desc,
            }
            var centro = {
                opt: req.body.centro_opt,
                desc: req.body.centro_desc,
            }
        //Coloca Archivo en carpeta temporalcorrespondiente
        File.moverFileImportar(file.originalname, "", empresa.opt, centro.opt, function(err){
            if(err){
                console.log(err);
                res.send({code: -2, mensaje: "Ha ocurrido un error al guardar el archivo."});
            } else {
                //Realiza el parser del xml y la validacion de la informacion
                var constructor = Parser.constructorEmpleado;
                var path = "./base/" + empresa.opt + "/" + centro.opt + "/" + file.originalname;
                var condiciones = Validador;
                Parser.parse(path, constructor, condiciones, function(errores, data){
                    if(errores){
                        console.log(errores)
                        res.send({
                            code: -1,
                            errores: errores,
                            empleados_error: data
                        });
                    } else {
                        var solicitud = {
                            fsolicitud: Date.now(),
                            idUsuario: req.user.id,
                        }
                        //Crea una nueva solicitud de importación 
                        //para seguimiento por usuario y fecha
                        var newSolicitud = new SolicitudModel(solicitud);
                        newSolicitud.save(function(err, solCreated){
                            if(err){
                                console.log(err);
                                res.send({code: -2, mensaje: "Ha ocurrido al crear la solicitud."});
                            } else {
                                //Comienza con la insercion de los empleados en la BD
                                Empleado.createMasivo(data, solCreated.id, empresa, centro, function(err){
                                    if(err){
                                        console.log(err);
                                        res.send({
                                            code: -3,
                                            mensaje: "Error en el alta masiva de empleados.",
                                        });
                                    } else {
                                        var importacion = {
                                            archivo: file.originalname,
                                            fimportacion: Date.now(),
                                            idUsuario: req.user.id,
                                            idEmpresa: empresa.opt,
                                            idCentro: centro.opt,
                                            idDriveArchivo: "",
                                            idDriveCentro: "",
                                            idDriveEmpresa: "",
                                        }
                                        jwtClient.authorize(function (err, tokens) {
                                            if (err) {
                                                console.log(err);
                                                res.send({
                                                    code: -2,
                                                    mensaje: "Error en la creación del token de autorización.",
                                                });
                                            } else {
                                                // Creacion de la carpeta de empresa en drive
                                                var idCarpetaEmpresa;
                                                var idCarpetaCentro;
                                                driveService.createFolder(jwtClient, empresa.opt, 'root', function(err, folderId){
                                                    if(err){
                                                        console.log(err);
                                                        res.send({
                                                            code: -2,
                                                            mensaje: "Error en la creación del folder empresa.",
                                                        });
                                                    } else {
                                                        //Creacion de la carpeta del centro en drive
                                                        idCarpetaEmpresa = folderId;
                                                        driveService.createFolder(jwtClient, centro, idCarpetaEmpresa, function(err, folderId){
                                                            if(err){
                                                                console.log(err);
                                                                res.send({
                                                                    code: -2,
                                                                    mensaje: "Error en la creación del folder centro.",
                                                                });
                                                            } else {
                                                                //Colocacion del archivo en la carpeta de drive
                                                                idCarpetaCentro = folderId;
                                                                driveService.insertFile(jwtClient, path, file, function(err, idFile){
                                                                    if(err){
                                                                        res.send({
                                                                            code: -2,
                                                                            mensaje: "Error en la creación del archivo.",
                                                                        });
                                                                    } else {
                                                                        importacion.idDriveArchivo = idFile;
                                                                        importacion.idDriveEmpresa = idCarpetaEmpresa;
                                                                        importacion.idDriveCentro = idCarpetaCentro;
                                                                        //Se escribe la información de la importacion en BD
                                                                        Importacion.create(importacion, function(err, dataFile){
                                                                            if(err){
                                                                                res.send({
                                                                                    code: -2,
                                                                                    mensaje: "Error en la inserción del archivo.",
                                                                                });
                                                                            } else {
                                                                                //Todo correcto y envía el número de registros insertados
                                                                                res.send({
                                                                                    code: 0,
                                                                                    data: data.length,
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                    
                                                                }, idCarpetaCentro);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });}
    });
});

router.get('/api/plantillaEmpleados', function(req, res, next){
    var pathPlantilla = __dirname + '/../plantillas/Plantilla2017Enero.xlsx';
    var stat = fs.statSync(pathPlantilla);
    res.writeHead(200, {
        'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 
        'Content-Length': stat.size
    });
    const stream = fs.createReadStream(__dirname + '/../plantillas/Plantilla2017Enero.xlsx');
    stream.pipe(res);
});

module.exports = router;
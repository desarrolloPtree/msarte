//rutas
var express = require('express');
var router = express.Router();
var servicioAsistencia = require('../service/asistencia');

router.get('/', function(req, res, next){
   res.render('asistencia/asistencia');
});

router.get('/api/listaAlumnos', function(req, res, next) {
    servicioAsistencia.mostrarAlumnos(function(error, listaAlumnos){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaAlumnos});
        
    });   
});

router.get('/api/listaAsistencia', function(req, res, next) {
    servicioAsistencia.mostrarAsistencia(function(error, listaAlumnos){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaAlumnos});
        
    });   
});

router.get('/api/listaAsistenciaMaestro', function(req, res, next) {
    servicioAsistencia.mostrarAsistenciaMaestro(function(error, listaAlumnos){
    
    if(error){
        return res.send({code:-1, message:"Error al solicitar datos", data:null});
    }
    
        return res.send({code:0, message:"Respuesta existosa", data:listaAlumnos});
        
    });   
});

router.post('/api/nuevaAsistencia', function(req, res, next){
    var alumno = req.body.nuevoAlumno;
    servicioAsistencia.nuevaAsistencia(alumno, function(error, asistenciaGuardado){
        if(error){
            return res.send({code:-1,message:"Error en la insercion",data:null});
        }
        
            return res.send({code:0,message:"Guardado Exitosamente",data:asistenciaGuardado});
    });
});

router.put('/api/update', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioAsistencia.update(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});

router.put('/api/update2', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioAsistencia.update2(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});

router.put('/api/update3', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioAsistencia.update3(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});

router.put('/api/update4', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioAsistencia.update4(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});

router.put('/api/updateFalta', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioAsistencia.updateFalta(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});

router.put('/api/updateFalta2', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioAsistencia.updateFalta2(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});

router.put('/api/updateFalta3', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioAsistencia.updateFalta3(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});

router.put('/api/updateFalta4', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioAsistencia.updateFalta4(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});

router.put('/api/updateMaestro', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioAsistencia.updateMaestro(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});

router.put('/api/updateMaestro2', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioAsistencia.updateMaestro2(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});

router.put('/api/updateMaestro3', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioAsistencia.updateMaestro3(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});

router.put('/api/updateMaestro4', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioAsistencia.updateMaestro4(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});

router.put('/api/updateFaltaMaestro', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioAsistencia.updateFaltaMaestro(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});

router.put('/api/updateFaltaMaestro2', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioAsistencia.updateFaltaMaestro2(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});

router.put('/api/updateFaltaMaestro3', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioAsistencia.updateFaltaMaestro3(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});

router.put('/api/updateFaltaMaestro4', function(req, res, next){
    var asistenciaId = req.body.id;
    var asistenciaModificar = req.body.modificarAsistencia;
    servicioAsistencia.updateFaltaMaestro4(asistenciaId,asistenciaModificar, function(error, asistenciaUpdated){
        if(error){
            return res.send({code:-1,message:"Error en la operacion",data:null});
        }
        
            return res.send({code:0,message:"Solicitud actualizada Exitosamente",data:asistenciaUpdated});
    });
});


module.exports = router;
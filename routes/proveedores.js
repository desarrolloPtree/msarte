//rutas
var express = require('express');
var router = express.Router();
//var servicioNuevaSolicitud = require('../service/clientes');

router.get('/', function(req, res, next){
   res.render('proveedores/proveedores');
});

router.get('/alta', function(req, res, next){
   res.render('proveedores/altaproveedor');
});

router.get('/documentacion', function(req, res, next){
   res.render('proveedores/documentacion');
});

router.get('/propuestas', function(req, res, next){
   res.render('proveedores/propuestas');
});

router.get('/proyeccion', function(req, res, next){
   res.render('proveedores/proyeccion');
});

module.exports = router;
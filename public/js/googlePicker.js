    // The Browser API key obtained from the Google API Console.
    // Replace with your own Browser API key, or your own key.
    var developerKey = 'AIzaSyCdzrsyw4uMcq6hZ8oXv9FFOXsqg8Iadcw';

    // The Client ID obtained from the Google API Console. Replace with your own Client ID.
    var clientId = "983996302015-t29320qv89ivmcfe9stv6oal0cdv91j5.apps.googleusercontent.com"

    // Replace with your own project number from console.developers.google.com.
    // See "Project number" under "IAM & Admin" > "Settings"
    var appId = "983996302015";

   // Scope to use to access user's Drive items.
    var scope = ['https://www.googleapis.com/auth/drive'];

    var pickerApiLoaded = false;
    var oauthToken;
    var wdcarga="vacio";
    var filecarga="vacio";
    // Use the Google API Loader script to load the google.picker script.
    function loadPicker(ventana) {
      wdcarga=ventana.split("&")[0];
      filecarga=ventana.split("&")[1];
      gapi.load('auth', {'callback': onAuthApiLoad});
      gapi.load('picker', {'callback': onPickerApiLoad});
    }

    function onAuthApiLoad() {
      window.gapi.auth.authorize(
          {
            'client_id': clientId,
            'scope': scope,
            'immediate': false
          },
          handleAuthResult);
    }

    function onPickerApiLoad() {
      pickerApiLoaded = true;
      createPicker();
    }

    function handleAuthResult(authResult) {
      if (authResult && !authResult.error) {
        oauthToken = authResult.access_token;
        createPicker();
      }
    }

    // Create and render a Picker object for searching images.
    function createPicker() {
      if(wdcarga=='fileFranquicias'){
        var parentId="1v9hfE2iqJs8uetOCAfofOR9HMPnFWCjN"
      }else{
        var parentId="1NM5sb_1T88qhIjNOCnfTX3t4MSA0shGF"
      }
      
      if (pickerApiLoaded && oauthToken) {
        var view = new google.picker.View(google.picker.ViewId.DOCS_HIDDEN);
        view.setMimeTypes("image/png,image/jpeg,image/jpg");
        var picker = new google.picker.PickerBuilder()
            //.enableFeature(google.picker.Feature.MULTISELECT_ENABLED)
            .setAppId(appId)
            .setOAuthToken(oauthToken)
            .addView(view)
            //.addView(google.picker.ViewId.PDFS)
            .addView(new google.picker.DocsUploadView().setParent(parentId))
            .setDeveloperKey(developerKey)
            .setCallback(pickerCallback)
            .setLocale('es')
            .build();
         picker.setVisible(true);
      }
    }

    // A simple callback implementation.
    function pickerCallback(data) {
      if (data.action == google.picker.Action.PICKED) {
        var fileId = data.docs[0].url;
        if(wdcarga=='fileFranquicias'){
          angular.element('#listaFranquiciasController').scope().addFile(fileId,filecarga);
        }else{
          $('#idFotoAlumno').val(data.docs[0].id);
        } 
        
        
        
      }
    }
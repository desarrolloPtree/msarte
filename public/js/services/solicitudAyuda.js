//factoria
angular.module('solicitudAyudaService', [])

.factory('solicitudAyuda', function($http){
   return {
      nuevo: function(solicitud){
         var promise = $http({
               method: 'POST',
               url: '/mesaAyuda/api/nuevaSolicitud',
               data: {
                  nuevaSolicitudAyuda: solicitud
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      listaSolicitudes: function(lista){
         var promise = $http({
               method: 'GET',
               url: '/mesaAyuda/api/listaSolicitudes',
               data: {
                  nuevaLista: lista,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },
      delete: function(id){
         var promise = $http({
            method: 'POST',
            url: '/mesaAyuda/api/delete',
            data: {
               id: id
            }
         }).then(function(response) {
            return response.data; 
         });
         return promise;
      },
      update: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/mesaAyuda/api/update',
               data: {
                  id: id,
                  modificarSolicitud: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      updateAprobar: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/mesaAyuda/api/updateAprobar',
               data: {
                  id: id,
                  modificarSolicitud: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      updateRechazar: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/mesaAyuda/api/updateRechazar',
               data: {
                  id: id,
                  modificarSolicitud: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
   } 
});
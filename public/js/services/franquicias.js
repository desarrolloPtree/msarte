//factoria
angular.module('franquiciasService', [])

.factory('franquiciasFactoria', function($http){
   return {
      listaFranquicias: function(lista){
         var promise = $http({
               method: 'GET',
               url: '/franquicias/api/listaFranquicias',
               data: {
                  nuevaLista: lista,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },      
      nuevo: function(franquicia){
         var promise = $http({
               method: 'POST',
               url: '/franquicias/api/nuevaFranquicia',
               data: {
                  nuevaFranquicia: franquicia
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      update: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/franquicias/api/update',
               data: {
                  id: id,
                  modificarFranquicia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },      
   } 
});
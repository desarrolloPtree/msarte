//factoria
angular.module('talleresService', [])

.factory('talleresFactoria', function($http){
   return {
      listaTalleres: function(lista){
         var promise = $http({
               method: 'GET',
               url: '/taller/api/listaTalleres',
               data: {
                  nuevaLista: lista,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },
       listaProductos: function(listaP){
         var promise = $http({
               method: 'GET',
               url: '/productos/api/listaProductos',
               data: {
                  nuevaLista: listaP,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },
      listaMaestros: function(lista2){
         var promise = $http({
               method: 'GET',
               url: '/taller/api/listaMaestros',
               data: {
                  nuevaListaMaestro: lista2,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      }, 
      nuevo: function(taller){
         var promise = $http({
               method: 'POST',
               url: '/taller/api/nuevoTaller',
               data: {
                  nuevoTaller: taller
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      
       update: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/taller/api/update',
               data: {
                  id: id,
                  modificarTaller: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      delete: function(id){
         var promise = $http({
            method: 'POST',
            url: '/taller/api/delete',
            data: {
               id: id
            }
         }).then(function(response) {
            return response.data; 
         });
         return promise;
      },     
   } 
});
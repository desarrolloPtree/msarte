angular.module('generalesService', [])

.factory('Generales', function($http){
   return{
       consultaEmpresas: function(){
           var promise = $http({
               method: 'GET',
               url: '/empresas/api/'
           }).then(function(response){
               return response.data;
           })
           return promise;
       },
       consultaCentros: function(){
           var promise = $http({
               method: 'GET',
               url: '/cencostos/api/'
           }).then(function(response){
               return response.data;
           })
           return promise;
       },
       consultaNominas: function(){
           var promise = $http({
               method: 'GET',
               url: '/generales/api/nominas'
           }).then(function(response){
               return response.data;
           })
           return promise;
       },
   }
});
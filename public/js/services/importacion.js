angular.module('importacionService', [])

.factory('Importacion', function($http){
    return{
        findLast: function(){
            var promise = $http({
                method: 'GET',
                url: '/importaciones/api/last'
            }).then(function(response){
                return response.data;
            })
            return promise;
        },
        importacionCreate: function(file, idEmpresa, descEmpresa, idCentro, descCentro){
           var fd = new FormData();
           
           fd.append('file', file);
           fd.append('empresa_opt', idEmpresa);
           fd.append('empresa_desc', descEmpresa);
           fd.append('centro_opt', idCentro);
           fd.append('centro_desc', descCentro);
            var promise = 
            $http.post('/importaciones/api/create/', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
           }).then(function(response) {
               return response.data;
           });
           return promise;
       }
    }
})
//factoria
angular.module('asistenciaService', [])

.factory('asistenciaFactoria', function($http){
   return {
      listaAsistencia: function(lista){
         var promise = $http({
               method: 'GET',
               url: '/asistencia/api/listaAsistencia',
               data: {
                  nuevaLista: lista,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },
      listaAsistenciaMaestro: function(lista2){
         var promise = $http({
               method: 'GET',
               url: '/asistencia/api/listaAsistenciaMaestro',
               data: {
                  nuevaLista: lista2,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },      
      nuevo: function(alumno, clase1){
         var promise = $http({
               method: 'POST',
               url: '/asistencia/api/nuevaAsistencia',
               data: {
                  nuevoAlumno: alumno
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      update: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/asistencia/api/update',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      update2: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/asistencia/api/update2',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      update3: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/asistencia/api/update3',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      update4: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/asistencia/api/update4',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },       
      updateFalta: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/asistencia/api/updateFalta',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      updateFalta2: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/asistencia/api/updateFalta2',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      updateFalta3: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/asistencia/api/updateFalta3',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      updateFalta4: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/asistencia/api/updateFalta4',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      updateMaestro: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/asistencia/api/updateMaestro',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      updateMaestro2: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/asistencia/api/updateMaestro2',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      updateMaestro3: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/asistencia/api/updateMaestro3',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      updateMaestro4: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/asistencia/api/updateMaestro4',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      updateFaltaMaestro: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/asistencia/api/updateFaltaMaestro',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      updateFaltaMaestro2: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/asistencia/api/updateFaltaMaestro2',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      updateFaltaMaestro3: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/asistencia/api/updateFaltaMaestro3',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      updateFaltaMaestro4: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/asistencia/api/updateFaltaMaestro4',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },      
   } 
});
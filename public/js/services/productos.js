//factoria
angular.module('productosService', [])

.factory('productosFactoria', function($http){
   return {
      listaProductos: function(lista){
         var promise = $http({
               method: 'GET',
               url: '/productos/api/listaProductos',
               data: {
                  nuevaLista: lista,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },      
      nuevo: function(producto){
         var promise = $http({
               method: 'POST',
               url: '/productos/api/nuevoProducto',
               data: {
                  nuevoProducto: producto
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      update: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/productos/api/update',
               data: {
                  id: id,
                  modificarProducto: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      updateStock: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/productos/api/updateStock',
               data: {
                  id: id,
                  modificarProducto: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      listaFranquicias: function(lista){
         var promise = $http({
               method: 'GET',
               url: '/productos/api/listaFranquicias',
               data: {
                  nuevaLista: lista,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },       
      delete: function(id){
         var promise = $http({
            method: 'POST',
            url: '/productos/api/delete',
            data: {
               id: id
            }
         }).then(function(response) {
            return response.data; 
         });
         return promise;
      },      
   } 
});
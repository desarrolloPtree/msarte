angular.module('userService', [])

.factory('User', function($http){
    return {
        alta: function(user){
            var userCreate = {
                email: user.correo,
                username: user.usuario,
                name: user.nombre,
                properties: {
                    telefono: user.telefono,
                    address: user.direccion,
        			puesto: user.puesto,
        			area: user.area,
        			nominas: user.nominas,
        			idFranq: user.idFranq,
        			nombreFranq: user.nombreFranq,
                }
            }
            var promise = $http({
              method: 'POST',
              url: '/users/api/create',
              data: {
                  user: userCreate,
              }
            }).then(function(response){
                return response.data;
            })
            return promise;
        },
        actualizarPassword: function(password){
            var promise = $http({
                method: 'POST',
                url: '/users/api/updatepass',
                data: {
                    password: password
                },
            }).then(function(response){
                console.log(response.data)
                return response.data;
            })
            return promise;
        },
        list: function(){
            var promise = $http({
                method: 'GET',
                url: '/users/api/list',
                data: {},
            }).then(function(response) {
                return response.data;
            });
            return promise;
        },
        listaUsuarios: function(lista){
            var promise = $http({
                method: 'GET',
                url: '/users/api/listaUsuarios',
                data: {
                    nuevaLista: lista,
                },
            }).then(function(response) {
                return response.data;
            });
            return promise;
        },
      updateRol: function(email, modificar, rol){
         var promise = $http({
               method: 'PUT',
               url: '/users/api/updateRol',
               data: {
                  email: email,
                  modificarRolUsuario: modificar,
                  modificarRol: rol
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      updateRolG: function(email, modificar, rol){
         var promise = $http({
               method: 'PUT',
               url: '/users/api/updateRolG',
               data: {
                  email: email,
                  modificarRolUsuario: modificar,
                  modificarRol: rol
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
        cambiarPassword: function(password, email){
            var promise = $http({
                method: 'POST',
                url: '/users/api/cambiarpass',
                data: {
                    password: password,
                    email: email
                },
            }).then(function(response){
                console.log(response.data)
                return response.data;
            })
            return promise;
        },
      listaFranquicias: function(lista){
         var promise = $http({
               method: 'GET',
               url: '/users/api/listaFranquicias',
               data: {
                  nuevaLista: lista,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },        
        cambiarPermisos: function(email, idFranq, nombreFranq){
            var promise = $http({
                method: 'POST',
                url: '/users/api/cambiarpermisos',
                data: {
                    email: email,
                    idFranq: idFranq,
                    nombreFranq: nombreFranq
                },
            }).then(function(response){
                console.log(response.data)
                return response.data;
            })
            return promise;
        },
      delete: function(email){
         var promise = $http({
            method: 'POST',
            url: '/users/api/delete',
            data: {
               email: email
            }
         }).then(function(response) {
            return response.data; 
         });
         return promise;
      },          
    }
})
//factoria
angular.module('alumnosService', [])

.factory('alumnosFactoria', function($http){
   return {
      listaAlumnos: function(lista){
         var promise = $http({
               method: 'GET',
               url: '/alumnos/api/listaAlumnos',
               data: {
                  nuevaLista: lista,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      }, 
      listaFranquicias: function(lista){
         var promise = $http({
               method: 'GET',
               url: '/alumnos/api/listaFranquicias',
               data: {
                  nuevaLista: lista,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },
      listaTalleres: function(lista3){
         var promise = $http({
               method: 'GET',
               url: '/taller/api/listaTalleres',
               data: {
                  nuevaLista: lista3,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },
      listaProductos: function(listaP){
         var promise = $http({
               method: 'GET',
               url: '/productos/api/listaProductos',
               data: {
                  nuevaLista: listaP,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },
      nuevo: function(alumno){
         var promise = $http({
               method: 'POST',
               url: '/alumnos/api/nuevoAlumno',
               data: {
                  nuevoAlumno: alumno
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      nuevoPago: function(pago){
         var promise = $http({
               method: 'POST',
               url: '/alumnos/api/nuevoPago',
               data: {
                  nuevoPago: pago
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      update: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/alumnos/api/update',
               data: {
                  id: id,
                  modificarAlumno: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      updateBaja: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/alumnos/api/updateBaja',
               data: {
                  id: id,
                  modificarAlumno: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      updateActivar: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/alumnos/api/updateActivar',
               data: {
                  id: id,
                  modificarAlumno: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },  
      updateFirma: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/alumnos/api/updateFirma',
               data: {
                  id: id,
                  modificarAlumno: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },        
   } 
});
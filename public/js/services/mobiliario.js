//factoria
angular.module('mobiliarioService', [])

.factory('mobiliarioFactoria', function($http){
   return {
      listaMobiliario: function(lista){
         var promise = $http({
               method: 'GET',
               url: '/mobiliario/api/listaMobiliario',
               data: {
                  nuevaLista: lista,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },      
      nuevo: function(mobiliario){
         var promise = $http({
               method: 'POST',
               url: '/mobiliario/api/nuevoMobiliario',
               data: {
                  nuevoMobiliario: mobiliario
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      update: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/mobiliario/api/update',
               data: {
                  id: id,
                  modificarMobiliario: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      updateStock: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/mobiliario/api/updateStock',
               data: {
                  id: id,
                  modificarMobiliario: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      listaFranquicias: function(lista){
         var promise = $http({
               method: 'GET',
               url: '/mobiliario/api/listaFranquicias',
               data: {
                  nuevaLista: lista,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },      
      delete: function(id){
         var promise = $http({
            method: 'POST',
            url: '/mobiliario/api/delete',
            data: {
               id: id
            }
         }).then(function(response) {
            return response.data; 
         });
         return promise;
      },      
   } 
});
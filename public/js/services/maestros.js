//factoria
angular.module('maestrosService', [])

.factory('maestrosFactoria', function($http){
   return {
      listaMaestros: function(lista){
         var promise = $http({
               method: 'GET',
               url: '/maestros/api/listaMaestros',
               data: {
                  nuevaLista: lista,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      }, 
      listaTalleres: function(lista3){
         var promise = $http({
               method: 'GET',
               url: '/taller/api/listaTalleres',
               data: {
                  nuevaLista: lista3,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },    
      listaFranquicias: function(lista){
         var promise = $http({
               method: 'GET',
               url: '/franquicias/api/listaFranquicias',
               data: {
                  nuevaLista: lista,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      }, 
      nuevo: function(maestro){
         var promise = $http({
               method: 'POST',
               url: '/maestros/api/nuevoMaestro',
               data: {
                  nuevoMaestro: maestro
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      update: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/maestros/api/update',
               data: {
                  id: id,
                  modificarMaestro: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },  
      updateBaja: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/maestros/api/updateBaja',
               data: {
                  id: id,
                  modificarMaestro: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      }, 
      updateActivar: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/maestros/api/updateActivar',
               data: {
                  id: id,
                  modificarMaestro: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },        
   } 
});
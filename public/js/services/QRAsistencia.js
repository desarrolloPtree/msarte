//factoria
angular.module('QRAsistenciaService', [])

.factory('QRAsistenciaFactoria', function($http){
   return {
      listaAlumnos: function(lista){
         var promise = $http({
               method: 'GET',
               url: '/QRAsistencia/api/listaAlumnos',
               data: {
                  nuevaLista: lista,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      }, 
      listaAsistencia: function(listaAsistencia){
         var promise = $http({
               method: 'GET',
               url: '/QRAsistencia/api/listaAsistencia',
               data: {
                  nuevaListaAsistencia: listaAsistencia,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      }, 
      nuevo: function(asistencia){
         var promise = $http({
               method: 'POST',
               url: '/QRAsistencia/api/nuevaAsistencia',
               data: {
                  nuevaAsistencia: asistencia
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
       update: function(id, modificar){
         var promise = $http({
               method: 'PUT',
               url: '/QRAsistencia/api/update',
               data: {
                  id: id,
                  modificarAsistencia: modificar
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
   }
      
});
angular.module('empleadoService', [])

.factory('Empleado', function($http){
   return {
      alta: function(empleado){
         var promise = $http({
               method: 'POST',
               url: '/empleados/api/create',
               data: {
                  empleado: empleado
               }
            }).then(function(response){
               return response.data;
            });
         return promise;
      },
      update: function(empleado){
         var promise = $http({
            method: 'PUT',
            url: '/empleados/api/update',
            data: {
               empleado: empleado,
            }
         }).then(function(response) {
             return response.data;
         })
         return promise;
      },
      listByEmpresa: function(empresa){
         var promise = $http({
               method: 'GET',
               url: '/empleados/api/list',
               data: {
                  empresa: empresa,
               },
            }).then(function(response) {
                return response.data;
            });
         return promise;
      },
      listByFilters: function(filters){
         console.log(filters);
         var promise = $http({
            method: 'POST',
            url: '/empleados/api/filter',
            data: filters,
         }).then(function(response){
            return response.data;
         });
         return promise;
      },
      delete: function(id){
         var promise = $http({
            method: 'POST',
            url: '/empleados/api/delete',
            data: {
               id: id
            }
         }).then(function(response) {
            return response.data; 
         });
         return promise;
      },
      findById: function(id){
         var promise = $http({
            method: 'GET',
            url: '/empleados/api/find/' + id,
            data: {},
         }).then(function(response){
            return response.data;
         });
         return promise;
      },
   } 
});
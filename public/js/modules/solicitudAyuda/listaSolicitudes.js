//modulo controller
angular.module('listaSolicitudes', ['solicitudAyudaService'])

.controller('listaSolicitudesController', ['$scope', 'solicitudAyuda' ,function($scope, solicitudAyuda){
    console.log("Funciona Lista Solicitudes!!");

    solicitudAyuda.listaSolicitudes().then(function(response){
        
        $scope.lista = response.data;

            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
        
        
    });
    
    $scope.borrarSolicitud = function(id){

    solicitudAyuda.delete(id).then(function(response){
        
        $scope.id = response.data;

            if(response.code == 0){
                console.log("Eliminado!");
                console.log(response.data);
                alert("Solicitud Eliminada");
                    solicitudAyuda.listaSolicitudes().then(function(response){
                        $scope.lista = response.data;
                            if(response.code == 0){
                                console.log("Lista Exitosa");
                                console.log(response.data);
                            }
                            else
                            {
                                console.log("Fallo en la operacion");
                                console.log(response);
                            }        
                    });                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
}


    $scope.actualizarSolicitud = function(id, modificar){

    solicitudAyuda.update(id, modificar).then(function(response){
        
        $scope.id = response.data;

            if(response.code == 0){
                console.log("Actualizada!");
                console.log(response.data);
                alert("Solicitud Actualizada");
                    solicitudAyuda.listaSolicitudes().then(function(response){
                        $scope.lista = response.data;
                            if(response.code == 0){
                                console.log("Lista Exitosa");
                                console.log(response.data);
                            }
                            else
                            {
                                console.log("Fallo en la operacion");
                                console.log(response);
                            }        
                    });                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
}

    $scope.aprobarSolicitud = function(id, modificar){

    solicitudAyuda.updateAprobar(id, modificar).then(function(response){
        
        $scope.id = response.data;

            if(response.code == 0){
                console.log("Actualizada!");
                console.log(response.data);
                alert("Solicitud Aprobada");
                    solicitudAyuda.listaSolicitudes().then(function(response){
                        $scope.lista = response.data;
                            if(response.code == 0){
                                console.log("Lista Exitosa");
                                console.log(response.data);
                            }
                            else
                            {
                                console.log("Fallo en la operacion");
                                console.log(response);
                            }        
                    });                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
}


    $scope.rechazarSolicitud = function(id, modificar){

    solicitudAyuda.updateRechazar(id, modificar).then(function(response){
        
        $scope.id = response.data;

            if(response.code == 0){
                console.log("Actualizada!");
                console.log(response.data);
                alert("Solicitud Cancelada");
                    solicitudAyuda.listaSolicitudes().then(function(response){
                        $scope.lista = response.data;
                            if(response.code == 0){
                                console.log("Lista Exitosa");
                                console.log(response.data);
                            }
                            else
                            {
                                console.log("Fallo en la operacion");
                                console.log(response);
                            }        
                    });                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
}




$scope.editar = function(editar){
    //console.log(editar);
    //console.log(editar.Solicitud);
    $scope.modificar = editar;
}

    
}]);
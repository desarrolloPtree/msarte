//modulo controller
angular.module('nuevaSolicitud', ['solicitudAyudaService'])

.controller('nuevaSolicitudController', ['$scope', 'solicitudAyuda' ,function($scope, solicitudAyuda){
    console.log("Funciona Nueva Solicitud!!");
    
    $scope.AddSolicitud = function(){
        $scope.nuevaSolicitudModel.Comentarios = [{
                  "Fecha": new Date(),
  				  "LeidoAdmin": "No", 
  				  "LeidoUser": "No", 
  				  "Comentario": $scope.nuevaSolicitudModel.Comentario, 
  				  "Usuario": "Por Definir"
            }];
        $scope.nuevaSolicitudModel.Estatus = "En progreso";
            
        solicitudAyuda.nuevo($scope.nuevaSolicitudModel).then(function(response){
            if(response.code == 0 && response.data != null){
                console.log("Guardado Exitosamente");
                alert("Solicitud Enviada exitosamente!");
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }
            
        });
    }
    
}]);


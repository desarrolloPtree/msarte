//modulo controller
angular.module('listaMaestros', ['maestrosService'])

.controller('listaMaestrosController', ['$scope', 'maestrosFactoria' ,function($scope, maestrosFactoria){
    console.log("Funciona Lista Maestros!!");

    maestrosFactoria.listaMaestros().then(function(response){
        $scope.lista = response.data;
            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
    
    maestrosFactoria.listaTalleres().then(function(response){
        $scope.lista3 = response.data;
            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
    
    maestrosFactoria.listaFranquicias().then(function(response){
        $scope.lista4 = response.data;
            if(response.code == 0){
                console.log("Lista Franquicia");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
    $scope.registrarMaestro = function(){
        
        $scope.nuevoMaestro.Estatus = "Activo";
            $scope.nuevoMaestro.ComentarioBaja = [{
                      "Fecha": new Date(),
      				  "Comentario": "Maestro dado de alta"
                }];        
        maestrosFactoria.nuevo($scope.nuevoMaestro).then(function(response){
            if(response.code == 0 && response.data != null){
                console.log("Guardado Exitosamente");
                //alert("Solicitud Enviada exitosamente!");
                //mensaje de exito sweet alert
                swal(
                  'Maestro registrado exitosamente!',
                  ' ',
                  'success'
                ).then(function () {}) //fin de sweet alert
                //Refresca la lista
                maestrosFactoria.listaMaestros().then(function(response){
                    $scope.lista = response.data;
                        if(response.code == 0){
                            console.log("Lista Exitosa");
                            console.log(response.data);
                        }
                        else
                        {
                            console.log("Fallo en la operacion");
                            console.log(response);
                        }        
                });                
                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }
            
        });
    }   //termina Registrar maestro
    
    $scope.actualizarMaestro = function(id, modificar){

    maestrosFactoria.update(id, modificar).then(function(response){
        
        $scope.id = response.data;

            if(response.code == 0){
                console.log("Actualizada!");
                console.log(response.data);
                //alert("Franquicia Actualizada");
                //mensaje de exito sweet alert
                swal(
                  'Maestro actualizado',
                  ' ',
                  'success'
                );
                    maestrosFactoria.listaMaestros().then(function(response){
                        $scope.lista = response.data;
                            if(response.code == 0){
                                console.log("Lista Exitosa");
                                console.log(response.data);
                            }
                            else
                            {
                                console.log("Fallo en la operacion");
                                console.log(response);
                            }        
                    });                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
}   //termina  actualizar maestro

    $scope.actualizarMaestroBaja = function(id, modificar){

    maestrosFactoria.updateBaja(id, modificar).then(function(response){
        
        $scope.id = response.data;

            if(response.code == 0){
                console.log("Actualizada!");
                console.log(response.data);
                //alert("Franquicia Actualizada");
                //mensaje de exito sweet alert
                swal(
                  'Maestro dado de baja',
                  ' ',
                  'success'
                );
                    maestrosFactoria.listaMaestros().then(function(response){
                        $scope.lista = response.data;
                            if(response.code == 0){
                                console.log("Lista Exitosa");
                                console.log(response.data);
                            }
                            else
                            {
                                console.log("Fallo en la operacion");
                                console.log(response);
                            }        
                    });                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
}   //termina  actualizar alumno

    $scope.actualizarMaestroActivar = function(id, modificar){

    maestrosFactoria.updateActivar(id, modificar).then(function(response){
        
        $scope.id = response.data;

            if(response.code == 0){
                console.log("Actualizada!");
                console.log(response.data);
                //alert("Franquicia Actualizada");
                //mensaje de exito sweet alert
                swal(
                  'Maestro activado',
                  ' ',
                  'success'
                );
                    maestrosFactoria.listaMaestros().then(function(response){
                        $scope.lista = response.data;
                            if(response.code == 0){
                                console.log("Lista Exitosa");
                                console.log(response.data);
                            }
                            else
                            {
                                console.log("Fallo en la operacion");
                                console.log(response);
                            }        
                    });                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
}   //termina  actualizar alumno
    
$scope.editar = function(editar){
    //console.log(editar);
    //console.log(editar.Solicitud);
    $scope.modificar = editar;
}//editar 
$scope.editar2 = function(editar){
    $scope.modificar2 = editar;
    $scope.modificar3 = editar.ComentarioBaja;    
}//editar     
    
}]);
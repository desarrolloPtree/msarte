//modulo controller
angular.module('listaMobiliario', ['mobiliarioService'])

.controller('listaMobiliarioController', ['$scope', 'mobiliarioFactoria' ,function($scope, mobiliarioFactoria){
    console.log("Funciona Lista Mobiliario!!");

    mobiliarioFactoria.listaMobiliario().then(function(response){
        $scope.lista = response.data;
            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
    
    mobiliarioFactoria.listaFranquicias().then(function(response){
        $scope.lista2 = response.data;
            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });       
    
    $scope.registrarMobiliario = function(){
            
        mobiliarioFactoria.nuevo($scope.nuevoMobiliario).then(function(response){
            if(response.code == 0 && response.data != null){
                console.log("Guardado Exitosamente");
                //alert("Solicitud Enviada exitosamente!");
                //mensaje de exito sweet alert
                swal(
                  'Mobiliario agregado exitosamente!',
                  ' ',
                  'success'
                ).then(function () {}) //fin de sweet alert
                //Refresca la lista
                mobiliarioFactoria.listaMobiliario().then(function(response){
                    $scope.lista = response.data;
                        if(response.code == 0){
                            console.log("Lista Exitosa");
                            console.log(response.data);
                        }
                        else
                        {
                            console.log("Fallo en la operacion");
                            console.log(response);
                        }        
                });                
                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }
            
        });
    }   //termina Registrar Mobiliario
    
    $scope.actualizarMobiliario = function(id, modificar){

    mobiliarioFactoria.update(id, modificar).then(function(response){
        
        $scope.id = response.data;

            if(response.code == 0){
                console.log("Actualizada!");
                console.log(response.data);
                //alert("Franquicia Actualizada");
                //mensaje de exito sweet alert
                swal(
                  'Mobiliario actualizado',
                  ' ',
                  'success'
                );
                    mobiliarioFactoria.listaMobiliario().then(function(response){
                        $scope.lista = response.data;
                            if(response.code == 0){
                                console.log("Lista Exitosa");
                                console.log(response.data);
                            }
                            else
                            {
                                console.log("Fallo en la operacion");
                                console.log(response);
                            }        
                    });                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
}   //termina  actualizar mobiliario 

    $scope.actualizarStockMobiliario = function(id, modificar){

    mobiliarioFactoria.updateStock(id, modificar).then(function(response){
        
        $scope.id = response.data;

            if(response.code == 0){
                console.log("Actualizada!");
                console.log(response.data);
                //alert("Franquicia Actualizada");
                //mensaje de exito sweet alert
                swal(
                  'Mobiliario actualizado',
                  ' ',
                  'success'
                );
                    mobiliarioFactoria.listaMobiliario().then(function(response){
                        $scope.lista = response.data;
                            if(response.code == 0){
                                console.log("Lista Exitosa");
                                console.log(response.data);
                            }
                            else
                            {
                                console.log("Fallo en la operacion");
                                console.log(response);
                            }        
                    });                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
}   //termina  actualizar mobiliario 

    $scope.borrarMobiliario = function(id){
        
                //mensaje sweet alert
                swal({
                  title: '¿Estas seguro de eliminar este registro?',
                  text: "Esta acción no podrá revertirse",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si',
                  cancelButtonText: 'No'
                }).then(function () {
                        //Ejecuta funcion delete en caso de decir si
                        mobiliarioFactoria.delete(id).then(function(response){
                            
                            $scope.id = response.data;
                    
                                if(response.code == 0){
                                    console.log("Eliminado!");
                                    console.log(response.data);
                                    //alert("Solicitud Eliminada");
                                    mobiliarioFactoria.listaMobiliario().then(function(response){
                                        
                                        $scope.lista = response.data;
                                            if(response.code == 0){
                                                console.log("Lista Exitosa");
                                                console.log(response.data);
                                            }
                                            else
                                            {
                                                console.log("Fallo en la operacion");
                                                console.log(response);
                                            }        
                                    });                
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                    
                    //Despues de eliminado muestra mensaje de eliminado
                  swal(
                    'Eliminado!',
                    'Mobiliario eliminado.',
                    'success'
                  )
                })//Fin mensaje sweet alert        
    }//fin de funcion borrar mobiliario
    
$scope.editar = function(editar){
    //console.log(editar);
    //console.log(editar.Solicitud);
    $scope.modificar = editar;
}

            $scope.demo = function(coordinates){
            var c = coordinates.split(':');
            //alert ('Id => ' + c[0]+' Nombre => ' + c[1]);                
                $scope.nuevoMobiliario.idFranq=c[0];
                $scope.nuevoMobiliario.nombreFranq=c[1];
            } 

            $scope.demo2 = function(coordinates){
            var c = coordinates.split(':');
            //alert ('Id => ' + c[0]+' Nombre => ' + c[1]);                
                $scope.modificar.idFranq=c[0];
                $scope.modificar.nombreFranq=c[1];
            }              
    
}]);
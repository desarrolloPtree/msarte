//modulo controller
angular.module('listaAsistencia', ['QRAsistenciaService'])

.controller('listaAsistenciaController', ['$scope', 'QRAsistenciaFactoria' ,function($scope, QRAsistenciaFactoria){
    console.log("Funciona Controller de asistencia!!");
    
    QRAsistenciaFactoria.listaAlumnos().then(function(response){
        $scope.lista = response.data;
            if(response.code == 0){
                
                console.log("Lista Exitosa");
                console.log(response.data);
                 $scope.URLsearch = window.location.search;
                     $scope.URLsearch.split("=")[1];
                     var id=$scope.URLsearch.split("=")[1]
                     for(var i=0;i<$scope.lista.length;i++){
                         alert(id)
                         if(id == $scope.lista[i]._id){
                             
                             $scope.alumno=$scope.lista[i]
                             alert($scope.alumno.Nombre)
                             break;
                         }
                     }
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });// fin de obtener la lista de alumnos
    QRAsistenciaFactoria.listaAsistencia().then(function(response){
        $scope.listaAsistencia = response.data;
            if(response.code == 0){
                
                console.log("Lista Exitosa de asistencia");
                console.log(response.data);
                
            }
            else
            {
                console.log("Fallo en la operacion asistencia");
                console.log(response);
            }        
    });// fin de obtener la lista de alumnos
    
    $scope.getURL=function(){
        
         $scope.URLsearch = window.location.search;
         $scope.URLsearch.split("=")[1];
         var id=$scope.URLsearch.split("=")[1]
         for(var i=0;i<$scope.lista.length;i++){
             if(id == $scope.lista[i]._id){
                 
                 $scope.alumno=$scope.lista[i]
                 $scope.AddAsistencia();
                 break;
             }
         }
    }// fin de obtener los datos de la url
    
    $scope.AddAsistencia=function(alumno){
        $scope.asistencia="";
        for(var i=0;i<$scope.listaAsistencia.length;i++){
            if($scope.alumno.Pin==$scope.listaAsistencia[i].Pin){
                $scope.asistencia=$scope.listaAsistencia[i]
                console.log($scope.asistencia)
            }
        }
      if($scope.asistencia==""){
          //alert("Vacio")
          $scope.writeAsistencia();
      }else{
          //alert("Existe Registro")
          $scope.agregarAsistencia();
        
      }
    }
    
    $scope.writeAsistencia=function(){
        //alert("Hola2")
        $scope.nuevaAsistencia={
            NombreAlumno:$scope.alumno.Nombre +" " + $scope.alumno.Apellidos,
            Taller:$scope.alumno.taller,
            Pin:$scope.alumno.Pin,
            Asistencias:{
                fecha:"01/03/2018",
                Estatus:"Asistio"
            }
        }
        
    QRAsistenciaFactoria.nuevo($scope.nuevaAsistencia).then(function(response){
            if(response.code == 0 && response.data != null){
                console.log("Guardado Exitosamente");
                //mensaje de exito sweet alert
                $scope.nuevaAsistencia=" "
                swal(
                  'Asistencias registrado exitosamente!',
                  ' ',
                  'success'
                ).then(function () {}) //fin de sweet alert
                //Refresca la lista
                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }
            
        });
        
    }//fin de writeAsistencia
    
    $scope.agregarAsistencia=function(){
        //alert("Hola3")
        $scope.asistencia.Asistencias.push({fecha:"02/03/2018", Estatus:"Asistio"})
        console.log($scope.asistencia._id)
        QRAsistenciaFactoria.update($scope.asistencia._id, $scope.asistencia).then(function(response){

            if(response.code == 0){
                //mensaje de exito sweet alert
                swal(
                  'Asisencia Registrada',
                  ' ',
                  'success'
                );
            }
            else
            {
            //    console.log("Fallo en la operacion");
            //    console.log(response);
            }        
    });
        
    }//fin de writeAsistencia
    
}]);
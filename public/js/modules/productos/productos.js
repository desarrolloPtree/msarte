//modulo controller
angular.module('listaProductos', ['productosService'])

.controller('listaProductosController', ['$scope', 'productosFactoria' ,function($scope, productosFactoria){
    console.log("Funciona Lista Productos!!");

    productosFactoria.listaProductos().then(function(response){
        $scope.lista = response.data;
            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
    
    productosFactoria.listaFranquicias().then(function(response){
        $scope.lista2 = response.data;
            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });      
    
    $scope.registrarProducto = function(){
            
        productosFactoria.nuevo($scope.nuevoProducto).then(function(response){
            if(response.code == 0 && response.data != null){
                console.log("Guardado Exitosamente");
                //alert("Solicitud Enviada exitosamente!");
                //mensaje de exito sweet alert
                swal(
                  'Producto agregado exitosamente!',
                  ' ',
                  'success'
                ).then(function () {}) //fin de sweet alert
                //Refresca la lista
                productosFactoria.listaProductos().then(function(response){
                    $scope.lista = response.data;
                        if(response.code == 0){
                            console.log("Lista Exitosa");
                            console.log(response.data);
                        }
                        else
                        {
                            console.log("Fallo en la operacion");
                            console.log(response);
                        }        
                });                
                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }
            
        });
    }   //termina Registrar franquicia
    
    $scope.actualizarProducto = function(id, modificar){

    productosFactoria.update(id, modificar).then(function(response){
        
        $scope.id = response.data;

            if(response.code == 0){
                console.log("Actualizada!");
                console.log(response.data);
                //alert("Franquicia Actualizada");
                //mensaje de exito sweet alert
                swal(
                  'Producto actualizado',
                  ' ',
                  'success'
                );
                    productosFactoria.listaProductos().then(function(response){
                        $scope.lista = response.data;
                            if(response.code == 0){
                                console.log("Lista Exitosa");
                                console.log(response.data);
                            }
                            else
                            {
                                console.log("Fallo en la operacion");
                                console.log(response);
                            }        
                    });                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
}   //termina  actualizar franquicia 

    $scope.actualizarStock = function(id, modificar){

    productosFactoria.updateStock(id, modificar).then(function(response){
        
        $scope.id = response.data;

            if(response.code == 0){
                console.log("Actualizada!");
                console.log(response.data);
                //alert("Franquicia Actualizada");
                //mensaje de exito sweet alert
                swal(
                  'Producto actualizado',
                  ' ',
                  'success'
                );
                    productosFactoria.listaProductos().then(function(response){
                        $scope.lista = response.data;
                            if(response.code == 0){
                                console.log("Lista Exitosa");
                                console.log(response.data);
                            }
                            else
                            {
                                console.log("Fallo en la operacion");
                                console.log(response);
                            }        
                    });                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
}   //termina  actualizar stock 

    $scope.borrarProducto = function(id){
        
                //mensaje sweet alert
                swal({
                  title: '¿Estas seguro de eliminar este registro?',
                  text: "Esta acción no podrá revertirse",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si',
                  cancelButtonText: 'No'
                }).then(function () {
                        //Ejecuta funcion delete en caso de decir si
                        productosFactoria.delete(id).then(function(response){
                            
                            $scope.id = response.data;
                    
                                if(response.code == 0){
                                    console.log("Eliminado!");
                                    console.log(response.data);
                                    //alert("Solicitud Eliminada");
                                    productosFactoria.listaProductos().then(function(response){
                                        
                                        $scope.lista = response.data;
                                            if(response.code == 0){
                                                console.log("Lista Exitosa");
                                                console.log(response.data);
                                            }
                                            else
                                            {
                                                console.log("Fallo en la operacion");
                                                console.log(response);
                                            }        
                                    });                
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                    
                    //Despues de eliminado muestra mensaje de eliminado
                  swal(
                    'Eliminado!',
                    'Producto eliminado.',
                    'success'
                  )
                })//Fin mensaje sweet alert        
    }//fin de funcion borrar producto
    
$scope.editar = function(editar){
    //console.log(editar);
    //console.log(editar.Solicitud);
    $scope.modificar = editar;
}//editar
            
            $scope.demo2 = function(coordinates){
            var c = coordinates.split(':');
            //alert ('Id => ' + c[0]+' Nombre => ' + c[1]);                
                $scope.modificar.idFranq=c[0];
                $scope.modificar.nombreFranq=c[1];
            }
        /*--- agrega las franquicias que se seleccionan--*/
        $scope.demo = function(){
            var c = $scope.franq.split(':');
                $scope.SelFranq.push({idFranq:c[0],nombreFranq:c[1]})
                $scope.nuevoProducto.Franq=$scope.SelFranq
        }
        
        $scope.SelectFactquicia=function(opcion){
            $scope.SelFranq=[]
            $scope.franq=""
            $scope.nuevoProducto.Franq=""
            $("#selFrac").hide()
                        
            if(opcion=='Todas'){
                var arrAllFranquicias=[]
                for(var i=0; i<$scope.lista2.length;i++){
                    arrAllFranquicias.push({idFranq:$scope.lista2[i].No,nombreFranq:$scope.lista2[i].Nombre})
                }
                 $scope.nuevoProducto.Franq=arrAllFranquicias
                 
            }else{
                $("#selFrac").show()
            }
            
        }//fin de SelectFactquicia
}]);
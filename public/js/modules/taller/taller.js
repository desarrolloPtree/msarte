//modulo controller
angular.module('listaTalleres', ['talleresService'])

.controller('listaTalleresController', ['$scope', 'talleresFactoria' ,function($scope, talleresFactoria){
    console.log("Funciona Lista talleres!!");

    talleresFactoria.listaTalleres().then(function(response){
        $scope.lista = response.data;
            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
    talleresFactoria.listaProductos().then(function(response){
        $scope.listaP = response.data;
            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
    
     talleresFactoria.listaMaestros().then(function(response){
        $scope.lista2 = response.data;
            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
     
     $scope.registrarTaller = function(){
         $scope.obj="";
         $scope.arr=[];
         for (var i =0; i<$scope.carr.length; i++ ) {
         $scope.obj = {
              idProducto: $scope.carr[i]._id,
              cantidadProducto: $scope.carr[i].cantidadCompra,
              nombreProducto: $scope.carr[i].Nombre,
             };
             $scope.arr.push($scope.obj);
             console.log($scope.arr);
         }
         $scope.nuevoTaller.producto= $scope.arr;
             console.log($scope.nuevoTaller.producto);
            $("#myModal").modal('hide');
            $.loader.open($data);
        talleresFactoria.nuevo($scope.nuevoTaller).then(function(response){
            if(response.code == 0 && response.data != null){
                console.log("Guardado Exitosamente");
                //mensaje de exito sweet alert
                
                $.loader.close(true);
                $scope.nuevoTaller=" "
                swal(
                  'Taller registrado exitosamente!',
                  ' ',
                  'success'
                ).then(function () {}) //fin de sweet alert
                //Refresca la lista
                talleresFactoria.listaTalleres().then(function(response){
                    $scope.lista = response.data;
                        if(response.code == 0){
                            console.log("Lista Exitosa");
                            console.log(response.data);
                        }
                        else
                        {
                            console.log("Fallo en la operacion");
                            console.log(response);
                        }        
                });                
                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }
            
        });
    }   //termina Registrar taller
    
   $scope.editarTaller = function(editar){
    //console.log(editar);
    //console.log(editar.Solicitud);
    $scope.modificar = editar;
  }//editar
    
    
 $scope.actualizarTaller = function(id, modificar){
    $.loader.open($data);
    talleresFactoria.update(id, modificar).then(function(response){
        
        $scope.objE="";
         $scope.arrE=[];
         for (var i =0; i<$scope.carr.length; i++ ) {
         $scope.objE = {
              idProducto: $scope.carr[i]._id,
              cantidadProducto: $scope.carr[i].cantidadCompra,
              nombreProducto: $scope.carr[i].Nombre,
             };
             $scope.arrE.push($scope.obj);
             console.log($scope.arr);
         }
         modificar.producto= $scope.arr;
        
        $scope.id = response.data;

            if(response.code == 0){
              //  console.log("Actualizada!");
            //    console.log(response.data);
                 $.loader.close(true);
                //mensaje de exito sweet alert
                swal(
                  'Taller actualizado',
                  ' ',
                  'success'
                );
                    talleresFactoria.listaTalleres().then(function(response){
                        $scope.lista = response.data;
                            if(response.code == 0){
                             //   console.log("Lista Exitosa");
                              //  console.log(response.data);
                            }
                            else
                            {
                                //console.log("Fallo en la operacion");
                                //console.log(response);
                            }        
                    });                
            }
            else
            {
            //    console.log("Fallo en la operacion");
            //    console.log(response);
            }        
    });
}   //termina  actualizar taller

 $scope.borrarTaller = function(id){
                
                //mensaje sweet alert
                swal({
                  title: '¿Estas seguro de eliminar este registro?',
                  text: "Esta acción no podrá revertirse",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si',
                  cancelButtonText: 'No'
                }).then(function () {
                        //Ejecuta funcion delete en caso de decir si
                        talleresFactoria.delete(id).then(function(response){
                            
                            $scope.id = response.data;
                    
                                if(response.code == 0){
                                    console.log("Eliminado!");
                                    console.log(response.data);
                                    //alert("Solicitud Eliminada");
                                    talleresFactoria.listaTalleres().then(function(response){
                                        
                                        $scope.lista = response.data;
                                            if(response.code == 0){
                                                console.log("Lista Exitosa");
                                                console.log(response.data);
                                            }
                                            else
                                            {
                                                console.log("Fallo en la operacion");
                                                console.log(response);
                                            }        
                                    });                
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                    
                    //Despues de eliminado muestra mensaje de eliminado
                  swal(
                    'Eliminado!',
                    'Taller eliminado.',
                    'success'
                  )
                })//Fin mensaje sweet alert        
    }//fin de funcion borrar producto
    
     $scope.demo2 = function(maestro){
         //alert(maestro)
                $scope.nuevoTaller.Maestro=maestro
            }
    /*---- funcion para agregar los articulos al carrito----*/
    $scope.carr=[]
    $scope.addProductosCarr=function(articulo){
        var cant =  $("#cant-"+articulo._id).val();
        if (cant == '' || cant == undefined) {
            alert("Selecciona la cantidad de Producto")
        }else{
           articulo.cantidadCompra= cant;
        $scope.carr.push(articulo);
        console.log($scope.carr);
        }
        
        
    }
    
     $scope.addProductosEdt=function(articulo){
        var cant =  $("#edtCant-"+articulo._id).val();
        if (cant == '' || cant == undefined) {
            alert("Selecciona la cantidad de Producto")
        }else{
           articulo.cantidadCompra= cant;
        $scope.modificar.producto.push({idProducto:articulo._id,cantidadProducto:cant,nombreProducto:articulo.Nombre});
        console.log($scope.modificar.producto);
        }
        
        
    }
    
    /*---funcion para eliminar registro del carrito---*/
    $scope.eliminarProductosCarr=function(cart){
        var pos = $scope.carr.indexOf(cart);
        // removemos del array tareas el indice que guarda al elemento donde se hizo click
        $scope.carr.splice(pos,1);
        $scope.tProducto=0
         for (var i =0; i<$scope.carr.length; i++ ) {
            $scope.tProducto=parseInt($scope.tProducto) + parseInt($scope.carr[i].Precio)
        }
    }
    
    /*---funcion para eliminar registro del carrito al editar---*/
    $scope.eliminarProductosEdt=function(cart){
        var pos = $scope.modificar.producto.indexOf(cart);
        // removemos del array tareas el indice que guarda al elemento donde se hizo click
        $scope.modificar.producto.splice(pos,1);
    }
}]);
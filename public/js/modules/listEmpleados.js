angular.module('listEmpleados', ['listEmpleadoDirective', 'generalesService'])

.controller('listEmpleadosController', ['$scope', 'Generales', function($scope, Generales){
    console.log("Controlador de Listado de empleados");
    
    Generales.consultaEmpresas().then(function(empresas){
        $scope.empresas = empresas;
    });
    
    Generales.consultaCentros().then(function(centros){
        $scope.centros = centros;
    });
    
    Generales.consultaNominas().then(function(nominas){
        console.log(nominas);
        $scope.nominas = nominas;
    })
}]);
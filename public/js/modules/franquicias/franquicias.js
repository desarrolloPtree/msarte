//modulo controller
angular.module('listaFranquicias', ['franquiciasService'])

.controller('listaFranquiciasController', ['$scope', 'franquiciasFactoria' ,function($scope, franquiciasFactoria){
    console.log("Funciona Lista Franquicias!!");

    franquiciasFactoria.listaFranquicias().then(function(response){
        $scope.lista = response.data;
            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
    
    $scope.registrarFranquicia = function(){
          $scope.nuevaFranquicia.Estatus ="Pendiente";
          //$scope.nuevaFranquicia.contratoPromesa=$("#fileContratoPromesa").val();
          //alert($scope.nuevaFranquicia.contratoPromesa)
        franquiciasFactoria.nuevo($scope.nuevaFranquicia).then(function(response){
            if(response.code == 0 && response.data != null){
                console.log("Guardado Exitosamente");
                //alert("Solicitud Enviada exitosamente!");
                //mensaje de exito sweet alert
                $scope.nuevaFranquicia ="";
                $('#myModalRegistrar').modal("hide");
                swal(
                  'Franquicia registrada exitosamente!',
                  ' ',
                  'success'
                ).then(function () {}) //fin de sweet alert
                //Refresca la lista
                franquiciasFactoria.listaFranquicias().then(function(response){
                    $scope.lista = response.data;
                        if(response.code == 0){
                            console.log("Lista Exitosa");
                            console.log(response.data);
                        }
                        else
                        {
                            console.log("Fallo en la operacion");
                            console.log(response);
                        }        
                });                
                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }
            
        });
    }   //termina Registrar franquicia
    
    $scope.actualizarFranquicia = function(id, modificar){

    franquiciasFactoria.update(id, modificar).then(function(response){
        
        $scope.id = response.data;

            if(response.code == 0){
                console.log("Actualizada!");
                console.log(response.data);
                //alert("Franquicia Actualizada");
                //mensaje de exito sweet alert
                swal(
                  'Franquicia actualizada',
                  ' ',
                  'success'
                );
                    franquiciasFactoria.listaFranquicias().then(function(response){
                        $scope.lista = response.data;
                            if(response.code == 0){
                                console.log("Lista Exitosa");
                                console.log(response.data);
                            }
                            else
                            {
                                console.log("Fallo en la operacion");
                                console.log(response);
                            }        
                    });                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
}   //termina  actualizar franquicia    
    
$scope.editar = function(editar){
    //console.log(editar);
    //console.log(editar.Solicitud);
    $scope.modificar = editar;
}

$scope.addFile=function(archivo,documento){
    switch (documento){
        case "1":
         $scope.nuevaFranquicia.contratoPromesa=archivo;
        break;
        case "2":
         $scope.nuevaFranquicia.ContratoFranquicia=archivo;
        break;
        case "3":
         $scope.nuevaFranquicia.IdentificacionOficial=archivo;
        break;
        case "4":
         $scope.nuevaFranquicia.RFC=archivo;
        break;
        case "5":
         $scope.nuevaFranquicia.PermisoUsoComercial=archivo;
        break;
        case "6":
         $scope.modificar.PermisoApertura.archivo;
        break;
        case "7":
         $scope.modificar.ContratoArrendamiento=archivo;
        break;
        case "8":
         $scope.modificar.PolizaSeguro=archivo;
        break;
        case "9":
         $scope.modificar.LicenciaFuncionamiento=archivo;
        break;
        case "10":
         $scope.modificar.CertificadoPC=archivo;
        break;
        case "11":
         $scope.modificar.PermisoObra=archivo;
        break;
        case "12":
         $scope.nuevaFranquicia.ActaConstitutiva=archivo;
        break;
        case "13":
         $scope.nuevaFranquicia.ActosAdministrativos=archivo;
        break;
        case "14":
         $scope.nuevaFranquicia.CartaLicitos=archivo;
        break;
    }
    
}
    
}]);
//modulo controller
angular.module('listaAsistencia', ['asistenciaService'])

.controller('listaAsistenciaController', ['$scope', 'asistenciaFactoria' ,function($scope, asistenciaFactoria){
    console.log("Funciona Controller de asistencia!!");
    
    
    asistenciaFactoria.listaAsistencia().then(function(response){
        $scope.lista = response.data;
            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    }); 
    
    asistenciaFactoria.listaAsistenciaMaestro().then(function(response){
        $scope.lista2 = response.data;
            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });     
    
    $scope.registrarAsistencia = function(){
        asistenciaFactoria.nuevo($scope.nuevaAsistencia).then(function(response){
            if(response.code == 0 && response.data != null){
                console.log("Guardado Exitosamente");
                //alert("Solicitud Enviada exitosamente!");
                //mensaje de exito sweet alert
                swal(
                  'Asistencia registrada exitosamente!',
                  ' ',
                  'success'
                ).then(function () {}) //fin de sweet alert
                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }
            
        });    
    
    }
    
    $scope.asistenciaAlumno = function(id, modificar){
        
    if($scope.capturaPin.Pin != $scope.modificar.Pin)
    {
        //mensaje de exito sweet alert
        swal(
          'PIN Incorrecto',
          ' ',
          'warning'
        );
        $scope.capturaPin.Pin = "";
    }
    else
    {
        asistenciaFactoria.update(id, modificar).then(function(response){
            
            $scope.id = response.data;
    
                if(response.code == 0){
                    console.log("Actualizada!");
                    console.log(response.data);
                    //alert("Franquicia Actualizada");
                    //mensaje de exito sweet alert
                    swal(
                      'Asistencia Registrada',
                      ' ',
                      'success'
                    );
                        asistenciaFactoria.listaAsistencia().then(function(response){
                            $scope.lista = response.data;
                                if(response.code == 0){
                                    console.log("Lista Exitosa");
                                    console.log(response.data);
                                    $scope.capturaPin.Pin = "";
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                
                }
                else
                {
                    console.log("Fallo en la operacion");
                    console.log(response);
                }        
        });        
    }
}   //termina asistencia   

    $scope.asistenciaAlumno2 = function(id, modificar){
        
    if($scope.capturaPin.Pin != $scope.modificar.Pin)
    {
        //mensaje de exito sweet alert
        swal(
          'PIN Incorrecto',
          ' ',
          'warning'
        );
        $scope.capturaPin.Pin = "";
    }
    else
    {
        asistenciaFactoria.update2(id, modificar).then(function(response){
            
            $scope.id = response.data;
    
                if(response.code == 0){
                    console.log("Actualizada!");
                    console.log(response.data);
                    //alert("Franquicia Actualizada");
                    //mensaje de exito sweet alert
                    swal(
                      'Asistencia Registrada',
                      ' ',
                      'success'
                    );
                        asistenciaFactoria.listaAsistencia().then(function(response){
                            $scope.lista = response.data;
                                if(response.code == 0){
                                    console.log("Lista Exitosa");
                                    console.log(response.data);
                                    $scope.capturaPin.Pin = "";
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                
                }
                else
                {
                    console.log("Fallo en la operacion");
                    console.log(response);
                }        
        });        
    }
}   //termina asistencia

    $scope.asistenciaAlumno3 = function(id, modificar){
        
    if($scope.capturaPin.Pin != $scope.modificar.Pin)
    {
        //mensaje de exito sweet alert
        swal(
          'PIN Incorrecto',
          ' ',
          'warning'
        );
        $scope.capturaPin.Pin = "";
    }
    else
    {
        asistenciaFactoria.update3(id, modificar).then(function(response){
            
            $scope.id = response.data;
    
                if(response.code == 0){
                    console.log("Actualizada!");
                    console.log(response.data);
                    //alert("Franquicia Actualizada");
                    //mensaje de exito sweet alert
                    swal(
                      'Asistencia Registrada',
                      ' ',
                      'success'
                    );
                        asistenciaFactoria.listaAsistencia().then(function(response){
                            $scope.lista = response.data;
                                if(response.code == 0){
                                    console.log("Lista Exitosa");
                                    console.log(response.data);
                                    $scope.capturaPin.Pin = "";
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                
                }
                else
                {
                    console.log("Fallo en la operacion");
                    console.log(response);
                }        
        });        
    }
}   //termina asistencia

    $scope.asistenciaAlumno4 = function(id, modificar){
        
    if($scope.capturaPin.Pin != $scope.modificar.Pin)
    {
        //mensaje de exito sweet alert
        swal(
          'PIN Incorrecto',
          ' ',
          'warning'
        );
        $scope.capturaPin.Pin = "";
    }
    else
    {
        asistenciaFactoria.update4(id, modificar).then(function(response){
            
            $scope.id = response.data;
    
                if(response.code == 0){
                    console.log("Actualizada!");
                    console.log(response.data);
                    //alert("Franquicia Actualizada");
                    //mensaje de exito sweet alert
                    swal(
                      'Asistencia Registrada',
                      ' ',
                      'success'
                    );
                        asistenciaFactoria.listaAsistencia().then(function(response){
                            $scope.lista = response.data;
                                if(response.code == 0){
                                    console.log("Lista Exitosa");
                                    console.log(response.data);
                                    $scope.capturaPin.Pin = "";
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                
                }
                else
                {
                    console.log("Fallo en la operacion");
                    console.log(response);
                }        
        });        
    }
}   //termina asistencia

    $scope.asistenciaMaestro = function(id, modificar){
        
    if($scope.capturaPin.Pin != $scope.modificar.Pin)
    {
        //mensaje de exito sweet alert
        swal(
          'PIN Incorrecto',
          ' ',
          'warning'
        );
        $scope.capturaPin.Pin = "";
    }
    else
    {
        asistenciaFactoria.updateMaestro(id, modificar).then(function(response){
            
            $scope.id = response.data;
    
                if(response.code == 0){
                    console.log("Actualizada!");
                    console.log(response.data);
                    //alert("Franquicia Actualizada");
                    //mensaje de exito sweet alert
                    swal(
                      'Asistencia Registrada',
                      ' ',
                      'success'
                    );
                        asistenciaFactoria.listaAsistenciaMaestro().then(function(response){
                            $scope.lista2 = response.data;
                                if(response.code == 0){
                                    console.log("Lista Exitosa");
                                    console.log(response.data);
                                    $scope.capturaPin.Pin = "";
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                
                }
                else
                {
                    console.log("Fallo en la operacion");
                    console.log(response);
                }        
        });        
    }
}   //termina asistencia   

    $scope.asistenciaMaestro2 = function(id, modificar){
        
    if($scope.capturaPin.Pin != $scope.modificar.Pin)
    {
        //mensaje de exito sweet alert
        swal(
          'PIN Incorrecto',
          ' ',
          'warning'
        );
        $scope.capturaPin.Pin = "";
    }
    else
    {
        asistenciaFactoria.updateMaestro2(id, modificar).then(function(response){
            
            $scope.id = response.data;
    
                if(response.code == 0){
                    console.log("Actualizada!");
                    console.log(response.data);
                    //alert("Franquicia Actualizada");
                    //mensaje de exito sweet alert
                    swal(
                      'Asistencia Registrada',
                      ' ',
                      'success'
                    );
                        asistenciaFactoria.listaAsistenciaMaestro().then(function(response){
                            $scope.lista2 = response.data;
                                if(response.code == 0){
                                    console.log("Lista Exitosa");
                                    console.log(response.data);
                                    $scope.capturaPin.Pin = "";
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                
                }
                else
                {
                    console.log("Fallo en la operacion");
                    console.log(response);
                }        
        });        
    }
}   //termina asistencia

    $scope.asistenciaMaestro3 = function(id, modificar){
        
    if($scope.capturaPin.Pin != $scope.modificar.Pin)
    {
        //mensaje de exito sweet alert
        swal(
          'PIN Incorrecto',
          ' ',
          'warning'
        );
        $scope.capturaPin.Pin = "";
    }
    else
    {
        asistenciaFactoria.updateMaestro3(id, modificar).then(function(response){
            
            $scope.id = response.data;
    
                if(response.code == 0){
                    console.log("Actualizada!");
                    console.log(response.data);
                    //alert("Franquicia Actualizada");
                    //mensaje de exito sweet alert
                    swal(
                      'Asistencia Registrada',
                      ' ',
                      'success'
                    );
                        asistenciaFactoria.listaAsistenciaMaestro().then(function(response){
                            $scope.lista2 = response.data;
                                if(response.code == 0){
                                    console.log("Lista Exitosa");
                                    console.log(response.data);
                                    $scope.capturaPin.Pin = "";
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                
                }
                else
                {
                    console.log("Fallo en la operacion");
                    console.log(response);
                }        
        });        
    }
}   //termina asistencia

    $scope.asistenciaMaestro4 = function(id, modificar){
        
    if($scope.capturaPin.Pin != $scope.modificar.Pin)
    {
        //mensaje de exito sweet alert
        swal(
          'PIN Incorrecto',
          ' ',
          'warning'
        );
        $scope.capturaPin.Pin = "";
    }
    else
    {
        asistenciaFactoria.updateMaestro4(id, modificar).then(function(response){
            
            $scope.id = response.data;
    
                if(response.code == 0){
                    console.log("Actualizada!");
                    console.log(response.data);
                    //alert("Franquicia Actualizada");
                    //mensaje de exito sweet alert
                    swal(
                      'Asistencia Registrada',
                      ' ',
                      'success'
                    );
                        asistenciaFactoria.listaAsistenciaMaestro().then(function(response){
                            $scope.lista2 = response.data;
                                if(response.code == 0){
                                    console.log("Lista Exitosa");
                                    console.log(response.data);
                                    $scope.capturaPin.Pin = "";
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                
                }
                else
                {
                    console.log("Fallo en la operacion");
                    console.log(response);
                }        
        });        
    }
}   //termina asistencia maestro
    
$scope.asistencia = function(editar){
    $scope.modificar = editar;
}//asistencia  

$scope.falta = function(editar){
    $scope.modificar = editar;
    var id = editar._id;
    var modificar = editar;
    
        asistenciaFactoria.updateFalta(id, modificar).then(function(response){
            
            $scope.id = response.data;
    
                if(response.code == 0){
                    console.log("Actualizada!");
                    console.log(response.data);
                    //alert("Franquicia Actualizada");
                    //mensaje de exito sweet alert
                    swal(
                      'Falta Registrada',
                      ' ',
                      'success'
                    );
                        asistenciaFactoria.listaAsistencia().then(function(response){
                            $scope.lista = response.data;
                                if(response.code == 0){
                                    console.log("Lista Exitosa");
                                    console.log(response.data);
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                
                }
                else
                {
                    console.log("Fallo en la operacion");
                    console.log(response);
                }        
        });    
    
}//falta 

$scope.falta2 = function(editar){
    $scope.modificar = editar;
    var id = editar._id;
    var modificar = editar;
    
        asistenciaFactoria.updateFalta2(id, modificar).then(function(response){
            
            $scope.id = response.data;
    
                if(response.code == 0){
                    console.log("Actualizada!");
                    console.log(response.data);
                    //alert("Franquicia Actualizada");
                    //mensaje de exito sweet alert
                    swal(
                      'Falta Registrada',
                      ' ',
                      'success'
                    );
                        asistenciaFactoria.listaAsistencia().then(function(response){
                            $scope.lista = response.data;
                                if(response.code == 0){
                                    console.log("Lista Exitosa");
                                    console.log(response.data);
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                
                }
                else
                {
                    console.log("Fallo en la operacion");
                    console.log(response);
                }        
        });    
    
}//falta 

$scope.falta3 = function(editar){
    $scope.modificar = editar;
    var id = editar._id;
    var modificar = editar;
    
        asistenciaFactoria.updateFalta3(id, modificar).then(function(response){
            
            $scope.id = response.data;
    
                if(response.code == 0){
                    console.log("Actualizada!");
                    console.log(response.data);
                    //alert("Franquicia Actualizada");
                    //mensaje de exito sweet alert
                    swal(
                      'Falta Registrada',
                      ' ',
                      'success'
                    );
                        asistenciaFactoria.listaAsistencia().then(function(response){
                            $scope.lista = response.data;
                                if(response.code == 0){
                                    console.log("Lista Exitosa");
                                    console.log(response.data);
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                
                }
                else
                {
                    console.log("Fallo en la operacion");
                    console.log(response);
                }        
        });    
    
}//falta 

$scope.falta4 = function(editar){
    $scope.modificar = editar;
    var id = editar._id;
    var modificar = editar;
    
        asistenciaFactoria.updateFalta4(id, modificar).then(function(response){
            
            $scope.id = response.data;
    
                if(response.code == 0){
                    console.log("Actualizada!");
                    console.log(response.data);
                    //alert("Franquicia Actualizada");
                    //mensaje de exito sweet alert
                    swal(
                      'Falta Registrada',
                      ' ',
                      'success'
                    );
                        asistenciaFactoria.listaAsistencia().then(function(response){
                            $scope.lista = response.data;
                                if(response.code == 0){
                                    console.log("Lista Exitosa");
                                    console.log(response.data);
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                
                }
                else
                {
                    console.log("Fallo en la operacion");
                    console.log(response);
                }        
        });    
    
}//falta

$scope.faltaMaestro = function(editar){
    $scope.modificar = editar;
    var id = editar._id;
    var modificar = editar;
    
        asistenciaFactoria.updateFaltaMaestro(id, modificar).then(function(response){
            
            $scope.id = response.data;
    
                if(response.code == 0){
                    console.log("Actualizada!");
                    console.log(response.data);
                    //alert("Franquicia Actualizada");
                    //mensaje de exito sweet alert
                    swal(
                      'Falta Registrada',
                      ' ',
                      'success'
                    );
                        asistenciaFactoria.listaAsistenciaMaestro().then(function(response){
                            $scope.lista2 = response.data;
                                if(response.code == 0){
                                    console.log("Lista Exitosa");
                                    console.log(response.data);
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                
                }
                else
                {
                    console.log("Fallo en la operacion");
                    console.log(response);
                }        
        });    
    
}//falta 

$scope.faltaMaestro2 = function(editar){
    $scope.modificar = editar;
    var id = editar._id;
    var modificar = editar;
    
        asistenciaFactoria.updateFaltaMaestro2(id, modificar).then(function(response){
            
            $scope.id = response.data;
    
                if(response.code == 0){
                    console.log("Actualizada!");
                    console.log(response.data);
                    //alert("Franquicia Actualizada");
                    //mensaje de exito sweet alert
                    swal(
                      'Falta Registrada',
                      ' ',
                      'success'
                    );
                        asistenciaFactoria.listaAsistenciaMaestro().then(function(response){
                            $scope.lista2 = response.data;
                                if(response.code == 0){
                                    console.log("Lista Exitosa");
                                    console.log(response.data);
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                
                }
                else
                {
                    console.log("Fallo en la operacion");
                    console.log(response);
                }        
        });    
    
}//falta 

$scope.faltaMaestro3 = function(editar){
    $scope.modificar = editar;
    var id = editar._id;
    var modificar = editar;
    
        asistenciaFactoria.updateFaltaMaestro3(id, modificar).then(function(response){
            
            $scope.id = response.data;
    
                if(response.code == 0){
                    console.log("Actualizada!");
                    console.log(response.data);
                    //alert("Franquicia Actualizada");
                    //mensaje de exito sweet alert
                    swal(
                      'Falta Registrada',
                      ' ',
                      'success'
                    );
                        asistenciaFactoria.listaAsistenciaMaestro().then(function(response){
                            $scope.lista2 = response.data;
                                if(response.code == 0){
                                    console.log("Lista Exitosa");
                                    console.log(response.data);
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                
                }
                else
                {
                    console.log("Fallo en la operacion");
                    console.log(response);
                }        
        });    
    
}//falta 

$scope.faltaMaestro4 = function(editar){
    $scope.modificar = editar;
    var id = editar._id;
    var modificar = editar;
    
        asistenciaFactoria.updateFaltaMaestro4(id, modificar).then(function(response){
            
            $scope.id = response.data;
    
                if(response.code == 0){
                    console.log("Actualizada!");
                    console.log(response.data);
                    //alert("Franquicia Actualizada");
                    //mensaje de exito sweet alert
                    swal(
                      'Falta Registrada',
                      ' ',
                      'success'
                    );
                        asistenciaFactoria.listaAsistenciaMaestro().then(function(response){
                            $scope.lista2 = response.data;
                                if(response.code == 0){
                                    console.log("Lista Exitosa");
                                    console.log(response.data);
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                
                }
                else
                {
                    console.log("Fallo en la operacion");
                    console.log(response);
                }        
        });    
    
}//falta maesttro

    
}]);
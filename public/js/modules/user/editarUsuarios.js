//modulo controller
angular.module('list', ['userService'])

.controller('listaUsersController', ['$scope', 'User' ,function($scope, User){
    console.log("Funciona Lista Usuarios!!");

        User.listaUsuarios().then(function(response){
        
        $scope.lista = response.data;
            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
    
    User.listaFranquicias().then(function(response){
        $scope.lista2 = response.data;
            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });    
    
    $scope.cambiarRol = function(email, modificar, rol){

    User.updateRol(email, modificar, rol).then(function(response){
        
        $scope.email = response.data;

            if(response.code == 0){
                console.log("Actualizada!");
                console.log(response.data);
                //alert("Solicitud Actualizada");
                //mensaje de exito sweet alert
                swal(
                  'Rol asignado',
                  ' ',
                  'success'
                )
                        User.listaUsuarios().then(function(response){
                        
                        $scope.lista = response.data;
                            if(response.code == 0){
                                console.log("Lista Exitosa");
                                console.log(response.data);
                            }
                            else
                            {
                                console.log("Fallo en la operacion");
                                console.log(response);
                            }        
                    });                
                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
}

    $scope.cambiarRolG = function(email, modificar, rol){

    User.updateRolG(email, modificar, rol).then(function(response){
        
        $scope.email = response.data;

            if(response.code == 0){
                console.log("Actualizada!");
                console.log(response.data);
                //alert("Solicitud Actualizada");
                //mensaje de exito sweet alert
                swal(
                  'Rol asignado',
                  ' ',
                  'success'
                )
                    User.listaUsuarios().then(function(response){
                    
                    $scope.lista = response.data;
                        if(response.code == 0){
                            console.log("Lista Exitosa");
                            console.log(response.data);
                        }
                        else
                        {
                            console.log("Fallo en la operacion");
                            console.log(response);
                        }        
                });                
                
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });
}   

    $scope.actualizarPassword = function(password, confirmacion, email){
        if(password !== undefined && confirmacion !== undefined && password !== "" && confirmacion !== ""){
            if(password === confirmacion){
                User.cambiarPassword(password, email).then(function(response){
                    if(response.code < 0){
                        console.log("Fallo en la operacion");
                    } else{
                        console.log("Exito");
                        //mensaje de exito sweet alert
                        swal(
                        'Contraseña actualizada',
                        ' ',
                        'success'
                        )
                    }
                });
            } else {
                console.log("Contrasenas no coinciden");
            }
        } else {
                console.log("Se deben completar ambos campos");
        }
    }
    
    $scope.actualizarPermisos = function(email, idFranq, nombreFranq){
                User.cambiarPermisos(email, idFranq, nombreFranq).then(function(response){
                    if(response.code < 0){
                        console.log("Fallo en la operacion");
                    } else {
                        console.log("Exito");
                        //mensaje de exito sweet alert
                        swal(
                        'Franquicia asignada',
                        ' ',
                        'success'
                        )
                        
                    User.listaUsuarios().then(function(response){
                    
                    $scope.lista = response.data;
                        if(response.code == 0){
                            console.log("Lista Exitosa");
                            console.log(response.data);
                        }
                        else
                        {
                            console.log("Fallo en la operacion");
                            console.log(response);
                        }        
                    });                          
                        
                    }
                });
            } 
            
    $scope.borrar = function(email){
        
                //mensaje sweet alert
                swal({
                  title: '¿Estas seguro de eliminar este usuario?',
                  text: "Esta acción no podrá revertirse",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si',
                  cancelButtonText: 'No'
                }).then(function () {
                        //Ejecuta funcion delete en caso de decir si
                        User.delete(email).then(function(response){
                            
                            $scope.id = response.data;
                    
                                if(response.code == 0){
                                    console.log("Eliminado!");
                                    console.log(response.data);
                                    //alert("Solicitud Eliminada");
                                    User.listaUsuarios().then(function(response){
                                        
                                        $scope.lista = response.data;
                                            if(response.code == 0){
                                                console.log("Lista Exitosa");
                                                console.log(response.data);
                                            }
                                            else
                                            {
                                                console.log("Fallo en la operacion");
                                                console.log(response);
                                            }        
                                    });                
                                }
                                else
                                {
                                    console.log("Fallo en la operacion");
                                    console.log(response);
                                }        
                        });                    
                    //Despues de eliminado muestra mensaje de eliminado
                  swal(
                    'Eliminado!',
                    'Usuario eliminado.',
                    'success'
                  )
                })//Fin mensaje sweet alert        
    }//fin de funcion borrar mobiliario            

$scope.editar = function(editar){
    var mostrarBoton;
    console.log(editar.token);
    if(editar.token == undefined){
        mostrarBoton = false;
    }
    else
    {
        mostrarBoton = true;
    }
    console.log(mostrarBoton);
    $scope.modificar = editar;
    $scope.mostrarBoton = mostrarBoton;
}


    $scope.editarPass = function(editar){
        var mostrarForm;
        console.log(editar.token);
        if(editar.token == undefined){
            mostrarForm = false;
        }
        else
        {
            mostrarForm = true;
        }
        console.log(mostrarForm);
        $scope.modificar2 = editar;
        $scope.mostrarForm = mostrarForm;
    }//editar
    
    $scope.editarPermisos = function(editar){
        var mostrarForm;
        console.log(editar.token);
        if(editar.token == undefined){
            mostrarForm = false;
        }
        else
        {
            mostrarForm = true;
        }
        console.log(mostrarForm);
        $scope.modificar3 = editar;
        $scope.mostrarForm = mostrarForm;
    }//editar
    
            $scope.demo = function(coordinates){
            var c = coordinates.split(':');
            //alert ('Id => ' + c[0]+' Nombre => ' + c[1]);                
                $scope.modificar3.properties.idFranq=c[0];
                $scope.modificar3.properties.nombreFranq=c[1];
            }    


}]);
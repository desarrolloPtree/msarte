angular.module('controlUsuariosModule', ['listUsersModule'])

.controller('controlUsuariosController', ['$scope', function($scope){
    console.log("Controller de Control de usuarios.");
    
    
}])

.filter('estatusFilter', function(){
    return function(estatus){
        if(estatus === "A"){
            return "Activo";
        } else if(estatus === "N"){
            return "Nuevo";
        } else if(estatus === "S"){
            return "Suspendido";
        } else {
            return "";
        }
    }
})

.filter('colorFilter', function(){
    return function(estatus){
        if(estatus === "A"){
            return "bg-success";
        } else if(estatus === "N"){
            return "bg-info";
        } else if(estatus === "S"){
            return "bg-warning";
        }
    }
})
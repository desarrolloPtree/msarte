angular.module('listUsersModule', ['userService'])

.directive('listUsers', ['User', function(User){
    return {
        scope: {
            mensaje: '=',
        },
        link: function(scope, element, attrs){
            User.list().then(function(response){
                if(response.code < 0){
                    //TODO Mensaje Alerta
                    scope.mensaje = response.mensaje;
                } else {
                    scope.users = response.users;
                    console.log(scope.users);
                }
            });
        },
        templateUrl: '/templates/user/listUsers.html'
    }
}]);
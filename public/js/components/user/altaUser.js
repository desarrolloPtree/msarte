angular.module('altaUserDirective', ['generalesDirective', 'userService'])

.directive('altaUser', ['User', function(User){
    return {
        scope: {
        },
        link: function(scope, element, attrs){
            scope.user = {};
            var user = scope.user
            user.nominas = [];
            
    User.listaFranquicias().then(function(response){
        scope.lista = response.data;
            if(response.code == 0){
                console.log("Lista Exitosa");
                console.log(response.data);
            }
            else
            {
                console.log("Fallo en la operacion");
                console.log(response);
            }        
    });            
            
            scope.addNomina = function(){
                if(scope.tipoNomina !== undefined && !existeNomina(scope.tipoNomina.opt, user.nominas)){
                    user.nominas.push(scope.tipoNomina);
                }
            }
            
            scope.borrarNomina = function(option){
                var nominas = user.nominas;
                var indice = findIndiceNomina(option, nominas);
                if(indice >= 0){
                    nominas.splice(indice, 1);    
                }
            }
            
            scope.crearUsuario = function(){
                User.alta(scope.user).then(function(response){
                    console.log(response);
                    if(response.code === 0){
                        scope.mensaje = response.mensaje;
                        scope.funcion = "/users/editarUsuarios";
                    } else {
                        scope.mensaje = response.mensaje;
                        scope.funcion = "";
                    }
                    $("#alerta").modal();
                });
            }
            
            var existeNomina = function(option, nominas){
                var indice = findIndiceNomina(option, nominas);
                if(indice >= 0) return true;
                return false;
            }
            
            var findIndiceNomina = function(option, nominas){
                for(var i = 0; i < nominas.length; i = i + 1){
                    if(option === nominas[i].opt)
                    return i;
                }
                return -1;
            }
            
            scope.demo = function(coordinates){
            var c = coordinates.split(':');
            //alert ('Id => ' + c[0]+' Nombre => ' + c[1]);                
                user.idFranq=c[0];
                user.nombreFranq=c[1];
            }
        },
        templateUrl: '/templates/user/altaUser.html'
    }
}])
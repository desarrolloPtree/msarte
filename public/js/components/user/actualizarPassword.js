angular.module('actualizarPassword', ['userService', 'generalesDirective'])

.directive('actualizarPassword', ['User', function(User){
    return {
        replace: false,
        scope: {},
        link: function(scope, element, attrs){
            
            scope.actualizarPassword = function(){
                if(scope.password !== undefined && scope.confirmacion !== undefined && scope.password !== "" && scope.confirmacion !== ""){
                    if(scope.password === scope.confirmacion){
                        User.actualizarPassword(scope.password).then(function(response){
                            if(response.code < 0){
                                scope.mensaje = response.mensaje;
                                $("#alerta").modal();
                            } else{
                                scope.mensaje = response.mensaje;
                                scope.funcion = "/";
                                $("#alerta").modal();
                            }
                        });
                    } else {
                        scope.mensaje = "Las contraseñas no coinciden.";
                        $("#alerta").modal();
                    }
                } else {
                        scope.mensaje = "Se deben informar ambos campos.";
                        $("#alerta").modal();
                }
            }
        },
        templateUrl: '/templates/user/actualizarPassword.html',
    }
}])
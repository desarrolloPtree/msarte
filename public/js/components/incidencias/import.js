angular.module('importIncidenciasDirective', ['incidenciasServices'])

.directive('uploadFileIncidencias', ['Incidencias', function(Incidencias){
    return {
        scope:{
            selectedCentro: '=',
            selectedEmpresa: '=',
            data: '=',
            lastimportacion: '=',
            mensaje: '=',
        },
        link: function(scope, elements, attrs){
            scope.newfile;
            scope.data = "";
            scope.errores = "";
            scope.empleadosError = "";
            
            scope.uploadFile = function(){
                var file = scope.newfile[0];
                if(scope.selectedEmpresa === undefined || scope.selectedEmpresa === "" || scope.selectedEmpresa === null){
                    scope.mensaje = "Debe seleccionar una empresa."
                    $("#alert").modal();
                    return;
                } else if(scope.selectedCentro === undefined || scope.selectedCentro === "" || scope.selectedCentro === null){
                    scope.mensaje = "Debe seleccionar un centro de costos."
                    $("#alert").modal();
                    return;
                }
                if(file !== undefined && file !== null && file !== ""){
                    var idEmpresa = scope.selectedEmpresa.opt;
                    var descEmpresa  = scope.selectedEmpresa.desc;
                    var idCentro = scope.selectedCentro.id;
                    var desCentro = scope.selectedCentro.name;

                    Incidencias.import(file, 
                        idEmpresa,
                        descEmpresa,
                        idCentro,
                        desCentro).then(function(response){
                       console.log(response); 
                    });
                    
                    console.log(file, 
                        idEmpresa,
                        descEmpresa,
                        idCentro,
                        desCentro);
                    
                    
                } else {
                    scope.mensaje = "Debe seleccionar un archivo para exportar."
                    $("#alert").modal();
                    return;
                }
            }
            
            
        },
        templateUrl: '/templates/incidencias/uploadIncidencias.html'
    }
}])
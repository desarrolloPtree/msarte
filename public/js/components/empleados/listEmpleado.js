angular.module('listEmpleadoDirective', ['empleadoService', 'angular.chosen', 'generalesDirective'])

.directive("chosen", [function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var config = {
                '.chosen-select'           : {},
                '.chosen-select-deselect'  : {allow_single_deselect:true},
                '.chosen-select-no-single' : {disable_search_threshold:10},
                '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                '.chosen-select-width'     : {width:"95%"}
            };
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }
        }
    };
}])

.directive('listEmpleados', ['Empleado', function(Empleado){
    return {
        scope:{
            empresas: '=',
            centros: '=',
            nominas: '=',
        },
        link: function(scope, elements, attr){
            scope.selectedEnterprises = [];
            scope.selectedCostCenters = [];
            scope.selectedPayrollTypes = [];
            scope.txtId = "";
            scope.txtName = "";

           scope.Filter = function () {
				var Data = {};
				Data.selectedEnterprises = scope.selectedEnterprises;
				Data.selectedCostCenters = scope.selectedCostCenters;
				Data.selectedPayrollTypes = scope.selectedPayrollTypes;
				Data.txtId = scope.txtId;
				Data.txtName = scope.txtName;
                
                Empleado.listByFilters(Data).then(function(response){
                    if(response.code === 0){
                        scope.empleados = response.data;    
                    } else {
                        scope.mensaje = response.mensaje;
                        $("#alerta").modal();
                    }
                });
			}
			
			scope.borrar = function(id){
			    Empleado.delete(id).then(function(response){
			        scope.mensaje = response.mensaje;
			        $("#alerta").modal();
                    scope.Filter();
			    });
			}
			
			scope.restaurar = function(){
			    scope.txtName = "";
			    $('.chosen-select option').prop('selected', false).trigger('chosen:updated');
			    scope.selectedEnterprises = [];
                scope.selectedCostCenters = [];
                scope.selectedPayrollTypes = [];
                scope.txtId = "";
                scope.txtName = "";
			    scope.Filter();
			}
            
            var list = function(){
                Empleado.listByEmpresa({}).then(function(response){
                    if(response.code === 0){
                        scope.empleados = response.data;
                    } else {
                        scope.mensaje = response.mensaje;
                        $("#alerta").modal();
                    }
                })
            }
            
            list();
            
            $('#nombre').bind("keyup change", function(e) {
                scope.Filter();
            });
            
        },
        templateUrl: '/templates/listEmpleados.html'
    }
}]);
angular.module('uploadFileDirective', ['importacionService'])

.directive('uploadFile', ['Importacion', function(Importacion){
    return {
        scope:{
            selectedCentro: '=',
            selectedEmpresa: '=',
            errores: '=',
            empleadosError: '=',
            data: '=',
            lastimportacion: '=',
            mensaje: '=',
        },
        link: function(scope, element, attrs){
            scope.newfile;
            scope.data = "";
            scope.errores = "";
            scope.empleadosError = "";
            
            scope.uploadFile = function(){
                var file = scope.newfile[0];
                if(scope.selectedEmpresa === undefined || scope.selectedEmpresa === "" || scope.selectedEmpresa === null){
                    scope.mensaje = "Debe seleccionar una empresa."
                    $("#alert").modal();
                    return;
                } else if(scope.selectedCentro === undefined || scope.selectedCentro === "" || scope.selectedCentro === null){
                    scope.mensaje = "Debe seleccionar un centro de costos."
                    $("#alert").modal();
                    return;
                }
                if(file !== undefined && file !== null && file !== ""){
                    var idEmpresa = scope.selectedEmpresa.opt;
                    var descEmpresa  = scope.selectedEmpresa.desc;
                    var idCentro = scope.selectedCentro.id;
                    var desCentro = scope.selectedCentro.name;
                    var timeStamp = Date.now();
                    
                    Importacion.importacionCreate(
                        file, 
                        idEmpresa,
                        descEmpresa,
                        idCentro,
                        desCentro,
                        timeStamp
                    ).then(function(response){
                        console.log(response);
                        if(response.code === -1){
                            scope.errores = response.errores;
                            scope.empleadosError = response.empleados_error;
                            scope.mensaje = {
                                titulo: "Importación Errónea!",
                                numerrores: scope.empleadosError,
                                errores: scope.errores,
                            }
                            $("#imp-er").modal();
                        } else if(response.code < 0){
                            scope.mensaje = response.mensaje;
                            $("#alert").modal();
                        } else {
                            console.log(response);
                            scope.data = response.data;
                            scope.mensaje = {
                                titulo: "Importación exitosa !",
                                descripcion: scope.data + " Usuarios importados correctamentes.",
                            }
                            $("#imp-ex").modal();
                            Importacion.findLast().then(function(response){
                                var date = new Date(response.importacion.fimportacion);
                                scope.lastimportacion = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
                            });
                        }                        
                    });
                } else {
                    scope.mensaje = "Debe seleccionar un archivo para exportar."
                    $("#alert").modal();
                    return;
                }
            }
            
            Importacion.findLast().then(function(response){
                if(response.importacion != null && response.importacion != undefined){
                    var date = new Date(response.importacion.fimportacion);
                    scope.lastimportacion = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
                }
            });
        },
        templateUrl: '/templates/importEmpleados.html'
    }
}]);
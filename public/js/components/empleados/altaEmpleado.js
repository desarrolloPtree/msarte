angular.module('altaEmpleadoDirective', ['empleadoService', 'generalesDirective'])

.directive('altaEmpleado', ['$window','Empleado', function($window, Empleado){
    return {
        link: function (scope, element, attrs) {
            
            $(function () {
                $('#alta-usuario').parsley().on('field:validated', function() {
                    var ok = $('.parsley-error').length === 0;
                    $('.bs-callout-info').toggleClass('hidden', !ok);
                    $('.bs-callout-warning').toggleClass('hidden', ok);
                })
                .on('form:submit', function() {
                    scope.altaUsuario();
                    return false;
                });
            });

            scope.empleado = {};

            scope.altaUsuario = function(){
                Empleado.alta(scope.empleado).then(function(data){
                    console.log(data)
                    if(data.code === 0){
                        scope.mensaje = data.mensaje;
                        scope.funcion = '/empleados/list';
                        $("#alerta").modal();
                    } else {
                        scope.mensaje = data.mensaje;
                        scope.detalle = data.detalle;
                        $("#alerta").modal();
                    }
                });
            }
        },
        templateUrl: '/templates/altaEmpleado.html'
    }
}]);
angular.module('editEmpleadoDirective', ['empleadoService', 'generalesDirective'])

.directive('editEmpleado', ['$window','Empleado', '$timeout', function($window, Empleado, $timeout){
    return {
        scope: {
          id: '=',  
        },
        link: function (scope, element, attrs) {
            $(function () {
                $('#edit-usuario').parsley().on('field:validated', function() {
                    var ok = $('.parsley-error').length === 0;
                    $('.bs-callout-info').toggleClass('hidden', !ok);
                    $('.bs-callout-warning').toggleClass('hidden', ok);
                })
                .on('form:submit', function() {
                    scope.saveUsuario();
                    return false;
                });
            });

            scope.empleado = {};

            scope.saveUsuario = function(){
                scope.empleado.salarios.mensual = scope.empleado.salarios.mensual.toString().replace(",", "");
                scope.empleado.salarios.base = scope.empleado.salarios.base.toString().replace(",", "");
                scope.empleado.salarios.sdi = scope.empleado.salarios.sdi.toString().replace(",", "");
                Empleado.update(scope.empleado).then(function(response){
                    if(response.code === 0){
                        scope.mensaje = response.mensaje;
                        scope.funcion = '/empleados/list';
                        $("#alerta").modal();
                    } else {
                        scope.mensaje = response.mensaje;
                        $("#alerta").modal();
                    }
                });
            }
            
            scope.initUsuario = function(id){
                console.log(id);
                Empleado.findById(id).then(function(response){
                    if(response.code < 0){
                        scope.mensaje = response.mensaje;
                        $("#alerta").modal();
                    } else {
                        scope.empleado = response.empleado;
                        
                        scope.empleado.salarios.mensual = parseFloat(scope.empleado.salarios.mensual.toString().replace(/,/g, ""))
                                          .toFixed(2)
                                          .toString()
                                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                          
                        scope.empleado.salarios.base = parseFloat(scope.empleado.salarios.base.toString().replace(/,/g, ""))
                                          .toFixed(2)
                                          .toString()
                                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                          
                        scope.empleado.salarios.sdi = parseFloat(scope.empleado.salarios.sdi.toString().replace(/,/g, ""))
                                          .toFixed(2)
                                          .toString()
                                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        
                        
                        
                        $timeout(function() {
                            $("#fcontratacion-datepicker").data("DateTimePicker").date(scope.empleado.entlaboral.fcontratacion);
                            $("#fantiguedad-datepicker").data("DateTimePicker").date(scope.empleado.entlaboral.fantiguedad);
                            $("#fnacimiento-datepicker").data("DateTimePicker").date(scope.empleado.nacimiento.fecha);
                        }, 0);
                    }
                });
            }
            
            scope.initUsuario(scope.id);
        },
        templateUrl: '/templates/editEmpleado.html'
    }
}]);
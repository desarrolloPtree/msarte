angular.module('generalesDirective', ['generalesService'])

.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;
                var arr_files = [];
                element.bind('change', function(){
                    $("#subir-div").show();
                    var arrName = element.val().split("\\");
                    $("#file-name-lb").text(arrName[arrName.length - 1]);
                    angular.forEach(element[0].files, function (item) {
                        arr_files[0] = item;
                    });
                    scope.$apply(function(){
                        modelSetter(scope, arr_files);
                    });
                });
        }
    };
}])

.directive('empresaCatalogo', ['Generales', function(Generales){
    return {
        scope: {
            selectedEmpresa: '='
        },
        link: function(scope, element, attrs){
            scope.empresas = [];

            Generales.consultaEmpresas().then(function(empresas){
                scope.empresas = empresas;
            });
        },
        templateUrl: '/templates/empresaCatalogo.html'
    }
}])

.directive('cencostosCatalogo', ['Generales', function(Generales){
    return {
        scope: {
            selectedCentro: '='
        },
        link: function(scope, element, attrs){
            scope.centros = [];
            
            Generales.consultaCentros().then(function(centros){
                console.log(centros);
                scope.centros = centros;
            });
        },
        templateUrl: '/templates/centrosCatalogo.html'
    }
}])

.directive('nominasCatalogo', ['Generales', function(Generales){
    return {
        scope: {
            selectedNomina: '='
        },
        link: function(scope, element, attrs){
            scope.nominas = [];
            
            Generales.consultaNominas().then(function(nominas){
                scope.nominas = nominas;
            });
        },
        templateUrl: '/templates/nominasCatalogo.html'
    }
}])

.directive('datepickerAngular', [ '$timeout', function ($timeout){
    return {
        scope:{
            fecha: '=', 
        },
        link: function($scope, element, $attrs) {
            return $timeout(function() {
                return $(element).datetimepicker({
                        format: 'YYYY/MM/DD',
                    }).on('dp.change', function(event) {
                        $scope.$apply(function() {
                            $scope.fecha = event.target.children.dateinput.value;    
                            return;
                        });
                    });
                }
            );
        }        
    };
}])

.directive('mensajeAlerta', [ function(){
    return {
        scope:{
            idmodal: '=',
            titulo: '=',
            descripcion: '='
        },
        link: function (scope, element, attrs){
        },
        templateUrl: '/templates/mensajeAlerta.html'
    }
}])

.directive('mensajeError', [ function(){
    return {
        scope:{
            idmodal: '=',
            numerrores: '=',
            errores: '='
        },
        link: function (scope, element, attrs){
        },
        templateUrl: '/templates/mensajeErrorImportacion.html'
    }
}])

.directive('mensajeSimple', [ function(){
    return {
        scope:{
            idmodal: '=',
            mensaje: '=',
        },
        link: function (scope, element, attrs){
        },
        templateUrl: '/templates/mensajeAlertaSimple.html'
    }
}])

.directive('alertaRedireccion', [ function(){
    return {
        scope:{
            idmodal: '=',
            mensaje: '=',
            funcion: '=',
            detalle: '='
        },
        link: function (scope, element, attrs){
        },
        templateUrl: '/templates/mensajes/alertaRedireccion.html'
    }
}])

.directive('estadosCatalogo', [function(){
    return {
        scope:{
            estadoSelected: '='
        },
        link: function(scope, element, attrs){
            
scope.estados = [
            "Aguascalientes",
            "Baja California",
            "Baja California Sur",
            "Campeche",
            "Chiapas",
            "Chihuahua",
            "Coahuila",
            "Colima",
            "Distrito Federal",
            "Durango",
            "Estado de México",
            "Guanajuato",
            "Guerrero",
            "Hidalgo",
            "Jalisco",
            "Michoacán",
            "Morelos",
            "Nayarit",
            "Nuevo León",
            "Oaxaca",
            "Puebla",
            "Querétaro",
            "Quintana Roo",
            "San Luis Potosí",
            "Sinaloa",
            "Sonora",
            "Tabasco",
            "Tamaulipas",
            "Tlaxcala",
            "Veracruz",
            "Yucatán",
            "Zacatecas",
        ];
            
        },
        templateUrl: '/templates/estadosCatalogo.html'
    }
}])
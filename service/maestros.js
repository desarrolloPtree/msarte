//service
var maestrosModel = require('../models/maestros');
var tallerModel = require('../models/taller');
var franquiciasModel = require('../models/franquicias');

module.exports = {
    mostrarMaestros: function(callback){
        maestrosModel.find({},function(error,listaMaestros){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaMaestros);
        });
    }, //termina mostrarMaestros
    mostrarTalleres: function(callback){
        tallerModel.find({},function(error,listaTalleres){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaTalleres);
        });
    }, //termina mostrarTalleres
    mostrarFranquicias: function(callback){
        franquiciasModel.find({},function(error,listaFranquicias){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaFranquicias);
        });
    }, //termina mostrarFranquicias
    nuevoMaestro: function(newMaestro, callback){
        var modeloNuevoMaestro = new maestrosModel(newMaestro);
        modeloNuevoMaestro.save(function(error,maestroGuardado){
           if(error) 
           {
            return callback(error, null);   
           }
           
           callback(null, maestroGuardado);
           
        });
    }, //termina nuevoAlumno     
    update: function(updateId, updateMaestro, callback){
        console.log(updateId);
        console.log(updateMaestro);
        maestrosModel.findOne({_id: updateId},function(error,maestroToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            maestroToUpdate.FechaIng = updateMaestro.FechaIng;
            maestroToUpdate.Nombre = updateMaestro.Nombre;
            maestroToUpdate.Apellidos = updateMaestro.Apellidos;
            maestroToUpdate.FechaNac = updateMaestro.FechaNac;
            maestroToUpdate.Edad = updateMaestro.Edad;
            maestroToUpdate.Calle = updateMaestro.Calle;
            maestroToUpdate.NoExt = updateMaestro.NoExt;
            maestroToUpdate.Colonia = updateMaestro.Colonia;
            maestroToUpdate.Cp = updateMaestro.Cp;
            maestroToUpdate.Delegacion = updateMaestro.Delegacion;
            maestroToUpdate.Entidad = updateMaestro.Entidad;
            maestroToUpdate.Telefono = updateMaestro.Telefono;
            maestroToUpdate.Celular = updateMaestro.Celular;
            maestroToUpdate.Email = updateMaestro.Email;
            maestroToUpdate.ContactoE = updateMaestro.ContactoE;
            maestroToUpdate.TelefonoE = updateMaestro.TelefonoE;
            maestroToUpdate.Parentesco = updateMaestro.Parentesco;
            maestroToUpdate.Enfermedad = updateMaestro.Enfermedad;
            maestroToUpdate.Tratamiento = updateMaestro.Tratamiento;
            maestroToUpdate.Sangre = updateMaestro.Sangre;
            maestroToUpdate.Umf = updateMaestro.Umf;

                maestroToUpdate.save(function(error,maestroGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, maestroGuardado);
                   
                });            

        });
 
    }, //termina update    
    updateBaja: function(updateId, updateMaestro, callback){
        console.log(updateId);
        console.log(updateMaestro);
        maestrosModel.findOne({_id: updateId},function(error,maestroToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            updateMaestro.ComentarioBaja[0].Comentario = updateMaestro.ComentarBaja;
            updateMaestro.ComentarioBaja[0].Fecha =  new Date();
            
          
            maestroToUpdate.Estatus = "Baja";
            maestroToUpdate.ComentarioBaja.push(updateMaestro.ComentarioBaja[0]);

                maestroToUpdate.save(function(error,maestroGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, maestroGuardado);
                   
                });            

        });
 
    }, //termina updateBaja       
    updateActivar: function(updateId, updateMaestro, callback){
        console.log(updateId);
        console.log(updateMaestro);
        maestrosModel.findOne({_id: updateId},function(error,maestroToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            updateMaestro.ComentarioBaja[0].Comentario = updateMaestro.ComentarBaja;
            updateMaestro.ComentarioBaja[0].Fecha =  new Date();
            
          
            maestroToUpdate.Estatus = "Activo";
            maestroToUpdate.ComentarioBaja.push(updateMaestro.ComentarioBaja[0]);

                maestroToUpdate.save(function(error,maestroGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, maestroGuardado);
                   
                });            

        });
 
    }, //termina updateActivar      
}
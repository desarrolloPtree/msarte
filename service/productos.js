//service
var productosModel = require('../models/productos');
var franquiciasModel = require('../models/franquicias');

module.exports = {
    mostrarProductos: function(callback){
        productosModel.find({},function(error,listaProductos){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaProductos);
        });
    }, //termina mostrarProductos   
    nuevoProducto: function(newProducto, callback){
        var modeloNuevoProducto = new productosModel(newProducto);
        modeloNuevoProducto.save(function(error,productoGuardado){
           if(error) 
           {
            return callback(error, null);   
           }
           
           callback(null, productoGuardado);
           
        });
    }, //termina nuevoProducto
    update: function(updateId, updateProducto, callback){
        console.log(updateId);
        console.log(updateProducto);
        productosModel.findOne({_id: updateId},function(error,productoToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            productoToUpdate.Codigo = updateProducto.Codigo;
            productoToUpdate.Nombre = updateProducto.Nombre;
            productoToUpdate.Categoria = updateProducto.Categoria;
            productoToUpdate.Existencias = updateProducto.Existencias;
            productoToUpdate.Precio = updateProducto.Precio;
            productoToUpdate.Precioa = updateProducto.Precioa;
            productoToUpdate.Empaque = updateProducto.Empaque;
            productoToUpdate.idFranq = updateProducto.idFranq;
            productoToUpdate.nombreFranq = updateProducto.nombreFranq;

                productoToUpdate.save(function(error,productoGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, productoGuardado);
                   
                });            

        });
 
    }, //termina update
    updateStock: function(updateId, updateProducto, callback){
        console.log(updateId);
        console.log(updateProducto);
        productosModel.findOne({_id: updateId},function(error,productoToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            productoToUpdate.Existencias = parseInt(updateProducto.Existencias)+parseInt(updateProducto.Existencias2);

                productoToUpdate.save(function(error,productoGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, productoGuardado);
                   
                });            

        });
 
    }, //termina update Stock  
    mostrarFranquicias: function(callback){
        franquiciasModel.find({},function(error,listaFranquicias){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaFranquicias);
        });
    }, //termina mostrarFranquicias     
    delete: function(id, callback){
        productosModel.remove({_id: id}, function(err,removed) {
            if(err){
                callback(err);
            } else {
                callback(null,removed);
            }
        });
    }, //termina delete     
}
//service
var solicitudAyudaModel = require('../models/solicitudAyuda');

module.exports = {
    nuevaSolicitud: function(newSolicitud, callback){
        var modeloNuevaSolicitud = new solicitudAyudaModel(newSolicitud);
        modeloNuevaSolicitud.save(function(error,solicitudGuardada){
           if(error) 
           {
            return callback(error, null);   
           }
           
           callback(null, solicitudGuardada);
           
        });
    }, //termina nuevaSolicitud
    mostrarSolicitudes: function(callback){
        solicitudAyudaModel.find({},function(error,listaSolicitudes){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaSolicitudes);
        });
    }, //termina mostrarSolicitudes
    delete: function(id, callback){
        solicitudAyudaModel.remove({_id: id}, function(err,removed) {
            if(err){
                callback(err);
            } else {
                callback(null,removed);
            }
        });
    }, //termina delete  
    update: function(updateId, updateSolicitud, callback){
        console.log(updateId);
        console.log(updateSolicitud);
        solicitudAyudaModel.findOne({_id: updateId},function(error,solicitudToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            console.log(solicitudToUpdate.Comentarios.length);
            solicitudToUpdate.Solicitud = updateSolicitud.Solicitud;
            solicitudToUpdate.Nomina = updateSolicitud.Nomina;
            solicitudToUpdate.Periodo = updateSolicitud.Periodo;
            solicitudToUpdate.Estatus = updateSolicitud.Estatus;
            //solicitudToUpdate.Comentarios[0].Comentario = updateSolicitud.Comentarios[0].Comentario;
            solicitudToUpdate.Comentarios[0].Comentario = updateSolicitud.Comentarios[0].Comentario;
                solicitudToUpdate.save(function(error,solicitudGuardada){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, solicitudGuardada);
                   
                });            

        });
 
    }, //termina update
    updateAprobar: function(updateId, updateSolicitud, callback){
        console.log(updateId);
        console.log(updateSolicitud);
        solicitudAyudaModel.findOne({_id: updateId},function(error,solicitudToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            solicitudToUpdate.Solicitud = updateSolicitud.Solicitud;
            solicitudToUpdate.Nomina = updateSolicitud.Nomina;
            solicitudToUpdate.Periodo = updateSolicitud.Periodo;
            solicitudToUpdate.Estatus = "Aprobada";
            solicitudToUpdate.Comentarios[0].Comentario = updateSolicitud.Comentarios[0].Comentario;
            
                solicitudToUpdate.save(function(error,solicitudGuardada){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, solicitudGuardada);
                   
                });            

        });
 
    }, //termina updateAprobar
    updateRechazar: function(updateId, updateSolicitud, callback){
        console.log(updateId);
        console.log(updateSolicitud);
        solicitudAyudaModel.findOne({_id: updateId},function(error,solicitudToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            solicitudToUpdate.Solicitud = updateSolicitud.Solicitud;
            solicitudToUpdate.Nomina = updateSolicitud.Nomina;
            solicitudToUpdate.Periodo = updateSolicitud.Periodo;
            solicitudToUpdate.Estatus = "Cancelada";
            solicitudToUpdate.Comentarios[0].Comentario = updateSolicitud.Comentarios[0].Comentario;
            
                solicitudToUpdate.save(function(error,solicitudGuardada){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, solicitudGuardada);
                   
                });            

        });
 
    }, //termina updateRechazar
}
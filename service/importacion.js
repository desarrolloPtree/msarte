var ImportacionModel = require('../models/importacion');

module.exports = {
    create: function(importacion, next){
        var newImportacion = new ImportacionModel();
        
        newImportacion.archivo = importacion.archivo;
        newImportacion.fimportacion = importacion.fimportacion;
        newImportacion.idUsuario = importacion.idUsuario;
        newImportacion.idEmpresa = importacion.idEmpresa;
        newImportacion.idCentro = importacion.idCentro;
        newImportacion.idDriveArchivo = importacion.idDriveArchivo;
        newImportacion.idDriveEmpresa = importacion.idDriveEmpresa;
        newImportacion.idDriveCentro = importacion.idDriveCentro;

        newImportacion.save(function(err, importacion){
            if(err){
                console.log(err);
                next({
                    code: -1,
                    message: "Creación incorrecta",
                    error: err
                });
            } else {
                next(null, importacion);
            }    
        });
    },
    findLast: function(usuario, next){
        ImportacionModel.findOne({'idUsuario': usuario}).sort({fimportacion: -1}).exec( function(err, lastuser){
            if(err){
                console.log(err);
                next(err);
            } else {
                next(err, lastuser);
            }
        });
    },
    findByUser: function(usuario, next){
        ImportacionModel.find({'idUsuario': usuario}).sort({fimportacion: -1}).populate('idUsuario').exec( function(err, importaciones){
            if(err){
                console.log(err);
                next(err);
            } else {
                console.log(importaciones)
                next(err, importaciones);
            }
        });
    },
}
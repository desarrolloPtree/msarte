//service
var tallerModel = require('../models/taller');
var maestrosModel = require('../models/maestros');
var productosModel = require('../models/productos');

module.exports = {
    mostrarTalleres: function(callback){
        tallerModel.find({},function(error,listaTalleres){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaTalleres);
        });
    }, //termina mostrarTalleres
     mostrarMaestros: function(callback){
        maestrosModel.find({},function(error,listaMaestros){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaMaestros);
        });
    }, //termina mostrarMaestros
     mostrarProductos: function(callback){
        productosModel.find({},function(error,listaProductos){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaProductos);
        });
    }, //termina mostrarProductos
    nuevoTaller: function(newTaller, callback){
        var modeloNuevoTaller = new tallerModel(newTaller);
        modeloNuevoTaller.save(function(error,tallerGuardado){
           if(error) 
           {
            return callback(error, null);   
           }
           
           callback(null, tallerGuardado);
           
        });
    }, //termina nuevoTaller
   
    update: function(updateId, updateTaller, callback){
        //console.log(updateId);
        //console.log(updateTaller);
        tallerModel.findOne({_id: updateId},function(error,tallerToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            tallerToUpdate.NombreTaller = updateTaller.NombreTaller;
            tallerToUpdate.TipoTaller = updateTaller.TipoTaller;
            tallerToUpdate.Horario = updateTaller.Horario;
            tallerToUpdate.Dias = updateTaller.Dias;
            tallerToUpdate.Cupo = updateTaller.Cupo;
            tallerToUpdate.Descripcion = updateTaller.Descripcion;
            tallerToUpdate.producto = updateTaller.producto;
            

                tallerToUpdate.save(function(error,tallerGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, tallerGuardado);
                   
                });            

        });
 
    }, //termina update 
    
    delete: function(id, callback){
        tallerModel.remove({_id: id}, function(err,removed) {
            if(err){
                callback(err);
            } else {
                callback(null,removed);
            }
        });
    }, //termina delete    
}
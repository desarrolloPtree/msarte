var UserModel = require("../models/user");
var franquiciasModel = require('../models/franquicias');
var bcrypt = require("bcrypt-nodejs");
var randomstring = require("randomstring");

var nodemailer = require('nodemailer');

var smtpConfig = {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, 
    auth: {
        user: 'inexserviceptree@gmail.com',
        pass: 'dsaAsd979#$rr$#d'
    }
};

var User = {
    
    create : function(newUser, next) {
    	UserModel.findOne({'local.email': newUser.email}, function(err, user){
    		if(err){
    			return next(err);
    		} if(user) {
    			return next(null, null);
    		} else {
    		    
    			var newUserObj = new UserModel();
    			
    			newUser.estatus = "N";
    			newUser.role = 2;
    			
                newUser.password = randomstring.generate(8);
                var unhashedPass = newUser.password;
                bcrypt.hash(newUser.password, null, null, function(err, hash) {
                    if(err){
                        next(err);
                    } else {
                        newUser.password = hash;
                        newUserObj.local = newUser;
                        newUserObj.save(function(err){
            				if(err){
            					return next(err);
            				}
            				var transporter = nodemailer.createTransport(smtpConfig);
                			var mailOptions = {
                				from: 'inexserviceptree@gmail.com',
                				to: newUser.email,
                				subject : "Usuario +Arte",
                				text : "Se ha creado exitosamente su usuario",
                        		html : 
                        			"<h1>+Arte</h1><br><br>" +
                        			"<p>Se ha creado exitosamente su usuario para la plataforma +Arte, para ingresar dirigirse </p>" +
                        			"<a href='https://masarte-geoluna8.c9users.io/'>aquí</a>" +
                        			"<p>con los siguientes datos donde se le pedirá que actualice su contraseña por seguridad:</p>" +
                        			"<h3>Usuario: " + newUser.username + "</h3>" +
                        			"<h3>Password: " + unhashedPass + "</h3>"
                        			,
                			};
                			transporter.sendMail(mailOptions, function(error, info){
                                if(error){
                                    return next(error);
                                }
                            });
            				return next(null, newUser);
            			});
                    }
                });
    		}
    	});
    },
    updatePassword: function(id, password, callback){
        UserModel.findById(id, function (err, user) {
            if (err) {
                return callback(err);
            }
            bcrypt.hash(password, null, null, function(err, hash) {
                if(err){
                    return callback(err);
                } else {
                    user.local.password = hash;
                    user.local.estatus = "A";
                    user.save(function (err, updatedUser) {
                        if (err){
                            return callback(err);
                        } else {
                            return callback(null);
                        }
                    });
                }
            });
        });
    },
    listUsers: function(callback){
        var filteredUsers = [];
        UserModel.find({}, function(err, users){
            if(err){
                return callback(err);
            } else {
                for(var i = 0; i < users.length; i ++){
                    if(users[i].local !== undefined && users[i].local !== null && 
                    users[i].local.password !== undefined && users[i].local.password !== null){
                        users[i].local.password = "";
                        filteredUsers.push(users[i].local);
                    } else {
                        filteredUsers.push(users[i].google);
                    }
                }
                return callback(null, filteredUsers);
            }
        });
    },
    updateRol: function(updateEmail, updateUsuarioRol, updateRol, callback){
        console.log(updateEmail);
        console.log(updateUsuarioRol);
        console.log(updateRol);
        UserModel.findOne({ "local.email" : updateEmail},function(error,RolToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            RolToUpdate.local.role = updateRol;
                RolToUpdate.save(function(error,rolActualizado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }

                   callback(null, rolActualizado);
                   
                });            

        });
 
    }, //termina update
    updateRolG: function(updateEmail, updateUsuarioRol, updateRol, callback){
        console.log(updateEmail);
        console.log(updateUsuarioRol);
        console.log(updateRol);
        UserModel.findOne({ "google.email" : updateEmail},function(error,RolToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            RolToUpdate.google.role = updateRol;
                RolToUpdate.save(function(error,rolActualizado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }

                   callback(null, rolActualizado);
                   
                });            

        });
 
    }, //termina update
    cambiarPassword: function(email, password, callback){
        UserModel.findOne({ "local.email" : email}, function (err, user) {
            if (err) {
                return callback(err);
            }
            bcrypt.hash(password, null, null, function(err, hash) {
                if(err){
                    return callback(err);
                } else {
                    user.local.password = hash;
                    user.save(function (err, updatedUser) {
                        if (err){
                            return callback(err);
                        } else {
                            //configuracion y envio del mensaje email
            				var transporter = nodemailer.createTransport(smtpConfig);
                			var mailOptions = {
                				from: 'inexserviceptree@gmail.com',
                				to: 'programacion2@ptree.com.mx',
                				subject : "+Arte - Cambio de contraseña",
                				text : "Cambio de contraseña",
                        		html : 
"<HTML><BODY>"
+"<div style='background:#993333; width: 80%; margin-left:10%; display: block; text-size:20px; text-align:center; font-weight:bold; color:#ffffff; border: 1px solid #3b577c; border-radius: 5px;'>"
+"<p> CAMBIO DE CONTRASEÑA</p>"     
+"</div>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; float:left; padding-left:10%'>Estimado usuario, le informamos que su contraseña ha sido actualizada.</p>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; padding-left:10%'>Nombre de usuario: <label style='border-bottom:  2px dotted #000000; font-weight:normal;'>"+ user.local.username + "</label></p>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; padding-left:10%'>Nueva contraseña: <label style='border-bottom:  2px dotted #000000; font-weight:normal;'>"+ password + "</label></p>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; color:#4B494D; padding-left:10%;'>Proenfar</p>"
+ "<div  style= 'margin-left:10%; background:#666666; width: 80%; height: 5px; display: block; '></div>"
+ "<div  style= 'margin-left:10%; background:#993333; width: 80%; height: 5px; display: block; '></div>"
+ "<div  style= 'margin-left:10%; background:#666666; width: 80%; height: 5px; display: block; '></div>"
+ "</HTML></BODY>",
                			};
                			transporter.sendMail(mailOptions, function(error, info){
                                if(error){
                                    return callback(error);
                                }
                            });                   
                            //fin envio de email
                            return callback(null);
                        }
                    });
                }
            });
        });
    }, //terminar cambiarPassword
    cambiarPermisos: function(updateEmail, updateIdFranq, updateNombreFranq, callback){
        console.log(updateEmail);
        console.log(updateIdFranq);
        console.log(updateNombreFranq);
        UserModel.findOne({ "local.email" : updateEmail},function(error,PermisosToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            PermisosToUpdate.local.properties.idFranq = updateIdFranq;
            PermisosToUpdate.local.properties.nombreFranq = updateNombreFranq;
                PermisosToUpdate.save(function(error, permisoActualizado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }

                   callback(null, permisoActualizado);
                   
                });            

        });
 
    }, //termina cambiarPermisos
    mostrarFranquicias: function(callback){
        franquiciasModel.find({},function(error,listaFranquicias){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaFranquicias);
        });
    }, //termina mostrarFranquicias 
    delete: function(deleteEmail, callback){
        UserModel.remove({"local.email" : deleteEmail}, function(err,removed) {
            if(err){
                callback(err);
            } else {
                callback(null,removed);
            }
        });
    }, //termina delete     
}

module.exports = User;
var IncidenciaModel = require("../models/incidencia")

module.exports = {
    save: function(incidencia, callback){
        var newIncidencia = new IncidenciaModel(incidencia);
        
        newIncidencia.save(function(err, incidencia){
            if(err){
                console.log(err);
                callback(err);
            } else {
                callback(null, incidencia);
            }
        })
    },
}
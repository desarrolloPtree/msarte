var fs = require("fs");
var config = require("../config");

var base = './base/';
var uploads = './uploads/';

module.exports = {
    moverFileImportar: function(fileName, aplicacion, empresa, centro, callback){
        
        console.log(fileName, aplicacion, empresa, centro);
        
        var oldPath = uploads + fileName;
        base = base + aplicacion;
        var newPath = base + empresa + '/' + centro + '/' + fileName;
        verificarRutaEmpresa(base, empresa, function(err){
            if(err){
                callback(err);
            } else {
                verificarRutaCentro(base, empresa, centro, function(err){
                    if(err){
                        callback(err);
                    } else {
                        fs.rename(oldPath, newPath, function(err){
                            if(err){
                                return callback(err);
                            }
                            callback(null);
                        });
                    }
                })
            }
        });
    },
}

function verficarRuta(path, callback){
    fs.stat(path, function(err, stats){
        if(err){
            stats = null;
        }
        callback(err, stats);
    });      
}

function createFolder(path, callback){
    fs.mkdir(path, 0777, function(err){
        if(err){
            throw err;
        } else {
            callback(null);
        }
    });
}

function verificarRutaEmpresa(base, empresa, callback){
    var pathEmpresa = base + "/" + empresa;
    verficarRuta(pathEmpresa, function(err, stats){
        if(err && err.code === "ENOENT"){
            createFolder(pathEmpresa, function(err){
                if(err){
                    callback(err);
                } else {
                    callback(null);
                }
            });
        } else if(err){
            throw err;
        } else {
            callback(null, stats);
        }
    });
}
 
function verificarRutaCentro (base, empresa, centro, callback){
    var pathCentro = base + "/" + empresa + "/" + centro;
    verficarRuta(pathCentro, function(err, stats){
        if(err && err.code === "ENOENT"){
            createFolder(pathCentro, function(err){
                if(err){
                    callback(err);
                } else {
                    callback(null);
                }
            });
        } else if(err){
            throw err;
        } else {
            callback(null, stats);
        }
    });
}
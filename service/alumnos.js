//service
var alumnosModel = require('../models/alumnos');
var franquiciasModel = require('../models/franquicias');
var pagosModel = require('../models/pagos');
var tallerModel = require('../models/taller');

//variables de configuracion para enviar email
var nodemailer = require('nodemailer');
var smtpConfig = {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, 
    auth: {
        user: 'desarrollo@ptree.com.mx',
        pass: 'ptree2018'
    }
};

module.exports = {
    mostrarAlumnos: function(callback){
        alumnosModel.find({},function(error,listaAlumnos){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaAlumnos);
        });
    }, //termina mostrarAlumnos   
    mostrarFranquicias: function(callback){
        franquiciasModel.find({},function(error,listaFranquicias){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaFranquicias);
        });
    }, //termina mostrarFranquicias
    mostrarTalleres: function(callback){
        tallerModel.find({},function(error,listaTalleres){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaTalleres);
        });
    }, //termina mostrarTalleres
    nuevoAlumno: function(newAlumno, callback){
        var modeloNuevoAlumno = new alumnosModel(newAlumno);
        modeloNuevoAlumno.save(function(error,alumnoGuardado){
           if(error) 
           {
            return callback(error, null);   
           }
           
                            //configuracion y envio del mensaje email
            				var transporter = nodemailer.createTransport(smtpConfig);
                			var mailOptions = {
                				from: 'desarrollo@ptree.com.mx',
                				to: "desarrollo@ptree.com.mx,",
                				subject : "+Arte - Bienvenido a +Arte",
                				text : "Alumno dado de alta",
                        		html : 
"<HTML><BODY>"
+"<div style='background:#2b6ca3; width: 80%; margin-left:10%; display: block; text-size:20px; text-align:center; font-weight:bold; color:#ffffff; border: 1px solid #3b577c; border-radius: 5px;'>"
+"<p> Bienvenido a +Arte</p>"     
+"</div>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; float:left; padding-left:10%'>Alumno(a) "+ newAlumno.Nombre +" ha sido inscrito</p>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; float:left; padding-left:10%'>Contrato.</p>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; float:left; padding-left:10%'>Tu PIN es: " + newAlumno.Pin + "</p>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; float:left; padding-left:10%'>Tu Código de identificación: <img style='width:100px' src='https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=" + newAlumno.Nombre + newAlumno.Pin + "&choe=UTF-8'></p>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; color:#4B494D; padding-left:10%;'><img style='width:100px' src='https://static.wixstatic.com/media/13f72d_578adf481c7346faa6f2d394418e754b.png/v1/fill/w_131,h_95,al_c,usm_0.66_1.00_0.01/13f72d_578adf481c7346faa6f2d394418e754b.png'> - +Arte</p>"
+ "<div  style= 'margin-left:10%; background:#2b6ca3; width: 80%; height: 5px; display: block; '></div>"
+ "</HTML></BODY>",
                			};
                			transporter.sendMail(mailOptions, function(error, info){
                                if(error){
                                    return callback(error);
                                }
                            });                   
                            //fin envio de email           
           
           callback(null, alumnoGuardado);
           
        });
    }, //termina nuevoAlumno  
    nuevoPago: function(newPago, callback){
        var modeloNuevoPago = new pagosModel(newPago);
        modeloNuevoPago.save(function(error,pagoGuardado){
           if(error) 
           {
            return callback(error, null);   
           }
           
           callback(null, pagoGuardado);
           
        });
    }, //termina nuevoPago
    update: function(updateId, updateAlumno, callback){
        console.log(updateId);
        console.log(updateAlumno);
        alumnosModel.findOne({_id: updateId},function(error,alumnoToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            alumnoToUpdate.Nombre = updateAlumno.Nombre;
            alumnoToUpdate.Apellidos = updateAlumno.Apellidos;
            alumnoToUpdate.Curp = updateAlumno.Curp;
            alumnoToUpdate.Edad = updateAlumno.Edad;
            alumnoToUpdate.Calle = updateAlumno.Calle;
            alumnoToUpdate.NoExt = updateAlumno.NoExt;
            alumnoToUpdate.NoInt = updateAlumno.NoInt;
            alumnoToUpdate.Colonia = updateAlumno.Colonia;
            alumnoToUpdate.Municipio = updateAlumno.Municipio;
            alumnoToUpdate.Entidad = updateAlumno.Entidad;
            alumnoToUpdate.Cp = updateAlumno.Cp;
            alumnoToUpdate.Telefono = updateAlumno.Telefono;
            alumnoToUpdate.Movil = updateAlumno.Movil;
            alumnoToUpdate.Email = updateAlumno.Email;
            alumnoToUpdate.Facebook = updateAlumno.Facebook;
            alumnoToUpdate.Instagram = updateAlumno.Instagram;
            alumnoToUpdate.Foto = updateAlumno.Foto;
            alumnoToUpdate.idFranq = updateAlumno.idFranq;
            alumnoToUpdate.nombreFranq = updateAlumno.nombreFranq;
            alumnoToUpdate.NombreTutor = updateAlumno.NombreTutor;
            alumnoToUpdate.ApellidosTutor = updateAlumno.ApellidosTutor;
            alumnoToUpdate.CurpTutor = updateAlumno.CurpTutor;
            alumnoToUpdate.Rfc = updateAlumno.Rfc;
            alumnoToUpdate.TelefonoTutor = updateAlumno.TelefonoTutor;
            alumnoToUpdate.MovilTutor = updateAlumno.MovilTutor;
            alumnoToUpdate.EmailTutor = updateAlumno.EmailTutor;

                alumnoToUpdate.save(function(error,alumnoGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, alumnoGuardado);
                   
                });            

        });
 
    }, //termina update   
    updateBaja: function(updateId, updateAlumno, callback){
        console.log(updateId);
        console.log(updateAlumno);
        alumnosModel.findOne({_id: updateId},function(error,alumnoToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            updateAlumno.ComentarioBaja[0].Comentario = updateAlumno.ComentarBaja;
            updateAlumno.ComentarioBaja[0].Fecha =  new Date();
            
          
            alumnoToUpdate.Estatus = "Baja";
            alumnoToUpdate.ComentarioBaja.push(updateAlumno.ComentarioBaja[0]);

                alumnoToUpdate.save(function(error,alumnoGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                            //configuracion y envio del mensaje email
            				var transporter = nodemailer.createTransport(smtpConfig);
                			var mailOptions = {
                				from: 'desarrollo@ptree.com.mx',
                				to: "desarrollo@ptree.com.mx,",
                				subject : "+Arte - Alumno dado de baja",
                				text : "Alumno dado de baja",
                        		html : 
"<HTML><BODY>"
+"<div style='background:#2b6ca3; width: 80%; margin-left:10%; display: block; text-size:20px; text-align:center; font-weight:bold; color:#ffffff; border: 1px solid #3b577c; border-radius: 5px;'>"
+"<p> Alumno dado de baja</p>"     
+"</div>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; float:left; padding-left:10%'>El alumno "+ updateAlumno.Nombre +" ha sido dado de baja</p>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; float:left; padding-left:10%'>Encuesta.</p>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; color:#4B494D; padding-left:10%;'><img style='width:100px' src='https://static.wixstatic.com/media/13f72d_578adf481c7346faa6f2d394418e754b.png/v1/fill/w_131,h_95,al_c,usm_0.66_1.00_0.01/13f72d_578adf481c7346faa6f2d394418e754b.png'> - +Arte</p>"
+ "<div  style= 'margin-left:10%; background:#2b6ca3; width: 80%; height: 5px; display: block; '></div>"
+ "</HTML></BODY>",
                			};
                			transporter.sendMail(mailOptions, function(error, info){
                                if(error){
                                    return callback(error);
                                }
                            });                   
                            //fin envio de email 
                   
                   callback(null, alumnoGuardado);
                   
                });            

        });
 
    }, //termina updateBaja       
    updateActivar: function(updateId, updateAlumno, callback){
        console.log(updateId);
        console.log(updateAlumno);
        alumnosModel.findOne({_id: updateId},function(error,alumnoToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
          //  updateAlumno.ComentarioBaja[0].Comentario = updateAlumno.ComentarBaja;
          //  updateAlumno.ComentarioBaja[0].Fecha =  new Date();
            
          
            alumnoToUpdate.Estatus = "Activo";
           // alumnoToUpdate.ComentarioBaja.push(updateAlumno.ComentarioBaja[0]);

                alumnoToUpdate.save(function(error,alumnoGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, alumnoGuardado);
                   
                });            

        });
 
    }, //termina updateActivar 
    updateFirma: function(updateId, updateAlumno, callback){
        console.log(updateId);
        console.log(updateAlumno);
        alumnosModel.findOne({_id: updateId},function(error,alumnoToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
           
            alumnoToUpdate.FirmaTutor = updateAlumno.FirmaTutor;

                alumnoToUpdate.save(function(error,alumnoGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, alumnoGuardado);
                   
                });            

        });
 
    }, //termina updateFirma    
}
//service
var franquiciasModel = require('../models/franquicias');

module.exports = {
    mostrarFranquicias: function(callback){
        franquiciasModel.find({},function(error,listaFranquicias){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaFranquicias);
        });
    }, //termina mostrarFranquicias   
    nuevaFranquicia: function(newFranquicia, callback){
        var modeloNuevaFranquicia = new franquiciasModel(newFranquicia);
        modeloNuevaFranquicia.save(function(error,franquiciaGuardada){
           if(error) 
           {
            return callback(error, null);   
           }
           
           callback(null, franquiciaGuardada);
           
        });
    }, //termina nuevaFranquicia
    update: function(updateId, updateFranquicia, callback){
        console.log(updateId);
        console.log(updateFranquicia);
        franquiciasModel.findOne({_id: updateId},function(error,franquiciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            franquiciaToUpdate.No = updateFranquicia.No;
            franquiciaToUpdate.Nombre = updateFranquicia.Nombre;
            franquiciaToUpdate.Direccion= updateFranquicia.Direccion;
            franquiciaToUpdate.FechaAp = updateFranquicia.FechaAp;
            franquiciaToUpdate.Estatus = updateFranquicia.Estatus;
                franquiciaToUpdate.save(function(error,franquiciaGuardada){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, franquiciaGuardada);
                   
                });            

        });
 
    }, //termina update    
}
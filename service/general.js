var multer = require('multer');

var storage = multer.diskStorage({
    destination: function (request, file, callback){
        callback(null, './uploads');
    },
    filename: function (request, file, callback){
        file.originalname = Date.now() + "-" + file.originalname;
        callback(null, file.originalname);
    }
});

var upload = multer({ storage : storage }).array('file',1);

module.exports = {
    uploadFile: function(request, response, callback){
        upload(request, response, function(err) {
            if(err){
                return callback(JSON.stringify({'error': 'Upload Falló.'}));
            }
            callback(null);
        });
    },
    consultaNominas: function(callback){
        var resultado = [
            {desc: "Tipo 1", opt: 1},
            {desc: "Tipo 2", opt: 2},
            {desc: "Tipo 3", opt: 3},
            {desc: "Tipo 4", opt: 4},
            {desc: "Tipo 5", opt: 5},
        ];
        callback(resultado);
    },
    getNominaByDecripcion: function(descripcion){
        this.consultaNominas(function(nominas){
            for(var i in nominas){
                if(nominas[i].desc === descripcion){
                    return nominas.opt;
                }
            }
        });
        return 0;
    }
}
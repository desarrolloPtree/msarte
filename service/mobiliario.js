//service
var mobiliarioModel = require('../models/mobiliario');
var franquiciasModel = require('../models/franquicias');

module.exports = {
    mostrarMobiliario: function(callback){
        mobiliarioModel.find({},function(error,listaMobiliario){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaMobiliario);
        });
    }, //termina mostrarMobiliario   
    nuevoMobiliario: function(newMobiliario, callback){
        var modeloNuevoMobiliario = new mobiliarioModel(newMobiliario);
        modeloNuevoMobiliario.save(function(error,mobiliarioGuardado){
           if(error) 
           {
            return callback(error, null);   
           }
           
           callback(null, mobiliarioGuardado);
           
        });
    }, //termina nuevoMobiliario
    update: function(updateId, updateMobiliario, callback){
        console.log(updateId);
        console.log(updateMobiliario);
        mobiliarioModel.findOne({_id: updateId},function(error,mobiliarioToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            mobiliarioToUpdate.Area = updateMobiliario.Area;
            mobiliarioToUpdate.Nombre = updateMobiliario.Nombre;
            mobiliarioToUpdate.Cantidad = updateMobiliario.Cantidad;
            mobiliarioToUpdate.Costo = updateMobiliario.Costo;
            mobiliarioToUpdate.idFranq = updateMobiliario.idFranq;
            mobiliarioToUpdate.nombreFranq = updateMobiliario.nombreFranq;

                mobiliarioToUpdate.save(function(error,mobiliarioGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, mobiliarioGuardado);
                   
                });            

        });
 
    }, //termina update 
    mostrarFranquicias: function(callback){
        franquiciasModel.find({},function(error,listaFranquicias){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaFranquicias);
        });
    }, //termina mostrarFranquicias    
    updateStock: function(updateId, updateMobiliario, callback){
        console.log(updateId);
        console.log(updateMobiliario);
        mobiliarioModel.findOne({_id: updateId},function(error,mobiliarioToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            mobiliarioToUpdate.Cantidad = parseInt(updateMobiliario.Cantidad)+parseInt(updateMobiliario.Cantidad2);

                mobiliarioToUpdate.save(function(error,mobiliarioGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, mobiliarioGuardado);
                   
                });            

        });
 
    }, //termina updateStock    
    delete: function(id, callback){
        mobiliarioModel.remove({_id: id}, function(err,removed) {
            if(err){
                callback(err);
            } else {
                callback(null,removed);
            }
        });
    }, //termina delete     
}
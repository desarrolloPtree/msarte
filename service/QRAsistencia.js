var alumnosModel = require('../models/alumnos');
var asistenciaModel = require('../models/asistencia');

module.exports = {
    mostrarAlumnos: function(callback){
        alumnosModel.find({},function(error,listaAlumnos){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaAlumnos);
        });
    }, //termina mostrarAlumnos
    
    mostrarAsistencia: function(callback){
        asistenciaModel.find({},function(error,listaAsistencia){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaAsistencia);
        });
    }, //termina mostrarAlumnos
    nuevaAsistencia: function(newAsistencia, callback){
        var modeloNuevaAsistencia = new asistenciaModel(newAsistencia);
        modeloNuevaAsistencia.save(function(error,asistenciaGuardada){
           if(error) 
           {
            return callback(error, null);   
           }
           
           callback(null, asistenciaGuardada);
           
        });
    }, //termina nuevaAsistencia
    update: function(updateId, updateAsistencia, callback){
        //console.log(updateId);
        //console.log(updateTaller);
        asistenciaModel.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.NombreAlumno = updateAsistencia.NombreAlumno;
            asistenciaToUpdate.Taller = updateAsistencia.Taller;
            asistenciaToUpdate.Pin = updateAsistencia.Pin;
            asistenciaToUpdate.Asistencias = updateAsistencia.Asistencias;
            

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina update 
}
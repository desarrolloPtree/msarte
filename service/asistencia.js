//service
var asistenciaModel = require('../models/asistencia');
var asistenciaModelMaestro = require('../models/asistenciaMaestro');
var alumnosModel = require('../models/alumnos');

//variables de configuracion para enviar email
var nodemailer = require('nodemailer');
var smtpConfig = {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, 
    auth: {
        user: 'desarrollo@ptree.com.mx',
        pass: 'ptree2018'
    }
};

module.exports = {
    mostrarAsistencia: function(callback){
        asistenciaModel.find({},function(error,listaAsistencia){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaAsistencia);
        });
    }, //termina mostrarProductos
    mostrarAsistenciaMaestro: function(callback){
        asistenciaModelMaestro.find({},function(error,listaAsistencia){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaAsistencia);
        });
    }, //termina mostrarProductos    
    mostrarAlumnos: function(callback){
        alumnosModel.find({},function(error,listaAlumnos){
           if(error)
           {
               return callback(error, null);
           }
           
           callback(null, listaAlumnos);
        });
    }, //termina mostrarProductos
    nuevaAsistencia: function(newAlumno, callback){
        var modeloNuevoMaestro = new asistenciaModelMaestro(newAlumno);
        modeloNuevoMaestro.save(function(error,alumnoGuardado){
           if(error) 
           {
            return callback(error, null);   
           }
           
           callback(null, alumnoGuardado);
           
        });
    }, //termina nuevoAlumno      
    update: function(updateId, updateAsistencia, callback){
        console.log(updateId);
        console.log(updateAsistencia);
        asistenciaModel.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.Clase1 = "Asistio";
            asistenciaToUpdate.Fecha1 = new Date();

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                if(asistenciaToUpdate.Estatus == "Baja")
                {
                            //configuracion y envio del mensaje email
            				var transporter = nodemailer.createTransport(smtpConfig);
                			var mailOptions = {
                				from: 'inexserviceptree@gmail.com',
                				to: "programacion2@ptree.com.mx,",
                				subject : "+Arte - Recordatorio, pago de inscripción",
                				text : "Alumno dado de alta",
                        		html : 
"<HTML><BODY>"
+"<div style='background:#2b6ca3; width: 80%; margin-left:10%; display: block; text-size:20px; text-align:center; font-weight:bold; color:#ffffff; border: 1px solid #3b577c; border-radius: 5px;'>"
+"<p> Recordatorio de pago</p>"     
+"</div>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; float:left; padding-left:10%'>Le recordamos que el alumno(a) "+ asistenciaToUpdate.Nombre +" asiste a clases pero presenta adeudo en su pago de inscripción, le invitamos a realizar el pago a la brevedad para evitar contratiempos, Gracias.</p>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; color:#4B494D; padding-left:10%;'><img style='width:100px' src='https://static.wixstatic.com/media/13f72d_578adf481c7346faa6f2d394418e754b.png/v1/fill/w_131,h_95,al_c,usm_0.66_1.00_0.01/13f72d_578adf481c7346faa6f2d394418e754b.png'> - +Arte</p>"
+ "<div  style= 'margin-left:10%; background:#2b6ca3; width: 80%; height: 5px; display: block; '></div>"
+ "</HTML></BODY>",
                			};
                			transporter.sendMail(mailOptions, function(error, info){
                                if(error){
                                    return callback(error);
                                }
                            });                   
                            //fin envio de email 
                }
                else
                {
                    console.log("No envia correo");
                }
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina update 
    update2: function(updateId, updateAsistencia, callback){
        console.log(updateId);
        console.log(updateAsistencia);
        asistenciaModel.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.Clase2 = "Asistio";
            asistenciaToUpdate.Fecha2 = new Date();

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                if(asistenciaToUpdate.Estatus == "Baja")
                {
                            //configuracion y envio del mensaje email
            				var transporter = nodemailer.createTransport(smtpConfig);
                			var mailOptions = {
                				from: 'inexserviceptree@gmail.com',
                				to: "programacion2@ptree.com.mx,",
                				subject : "+Arte - Recordatorio, pago de inscripción",
                				text : "Alumno dado de alta",
                        		html : 
"<HTML><BODY>"
+"<div style='background:#2b6ca3; width: 80%; margin-left:10%; display: block; text-size:20px; text-align:center; font-weight:bold; color:#ffffff; border: 1px solid #3b577c; border-radius: 5px;'>"
+"<p> Recordatorio de pago</p>"     
+"</div>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; float:left; padding-left:10%'>Le recordamos que el alumno(a) "+ asistenciaToUpdate.Nombre +" asiste a clases pero presenta adeudo en su pago de inscripción, le invitamos a realizar el pago a la brevedad para evitar contratiempos, Gracias.</p>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; color:#4B494D; padding-left:10%;'><img style='width:100px' src='https://static.wixstatic.com/media/13f72d_578adf481c7346faa6f2d394418e754b.png/v1/fill/w_131,h_95,al_c,usm_0.66_1.00_0.01/13f72d_578adf481c7346faa6f2d394418e754b.png'> - +Arte</p>"
+ "<div  style= 'margin-left:10%; background:#2b6ca3; width: 80%; height: 5px; display: block; '></div>"
+ "</HTML></BODY>",
                			};
                			transporter.sendMail(mailOptions, function(error, info){
                                if(error){
                                    return callback(error);
                                }
                            });                   
                            //fin envio de email 
                }
                else
                {
                    console.log("No envia correo");
                }                   
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina update 
    update3: function(updateId, updateAsistencia, callback){
        console.log(updateId);
        console.log(updateAsistencia);
        asistenciaModel.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.Clase3 = "Asistio";
            asistenciaToUpdate.Fecha3 = new Date();

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                if(asistenciaToUpdate.Estatus == "Baja")
                {
                            //configuracion y envio del mensaje email
            				var transporter = nodemailer.createTransport(smtpConfig);
                			var mailOptions = {
                				from: 'inexserviceptree@gmail.com',
                				to: "programacion2@ptree.com.mx,",
                				subject : "+Arte - Recordatorio, pago de inscripción",
                				text : "Alumno dado de alta",
                        		html : 
"<HTML><BODY>"
+"<div style='background:#2b6ca3; width: 80%; margin-left:10%; display: block; text-size:20px; text-align:center; font-weight:bold; color:#ffffff; border: 1px solid #3b577c; border-radius: 5px;'>"
+"<p> Recordatorio de pago</p>"     
+"</div>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; float:left; padding-left:10%'>Le recordamos que el alumno(a) "+ asistenciaToUpdate.Nombre +" asiste a clases pero presenta adeudo en su pago de inscripción, le invitamos a realizar el pago a la brevedad para evitar contratiempos, Gracias.</p>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; color:#4B494D; padding-left:10%;'><img style='width:100px' src='https://static.wixstatic.com/media/13f72d_578adf481c7346faa6f2d394418e754b.png/v1/fill/w_131,h_95,al_c,usm_0.66_1.00_0.01/13f72d_578adf481c7346faa6f2d394418e754b.png'> - +Arte</p>"
+ "<div  style= 'margin-left:10%; background:#2b6ca3; width: 80%; height: 5px; display: block; '></div>"
+ "</HTML></BODY>",
                			};
                			transporter.sendMail(mailOptions, function(error, info){
                                if(error){
                                    return callback(error);
                                }
                            });                   
                            //fin envio de email 
                }
                else
                {
                    console.log("No envia correo");
                }                   
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina update 
    update4: function(updateId, updateAsistencia, callback){
        console.log(updateId);
        console.log(updateAsistencia);
        asistenciaModel.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.Clase4 = "Asistio";
            asistenciaToUpdate.Fecha4 = new Date();

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                if(asistenciaToUpdate.Estatus == "Baja")
                {
                            //configuracion y envio del mensaje email
            				var transporter = nodemailer.createTransport(smtpConfig);
                			var mailOptions = {
                				from: 'inexserviceptree@gmail.com',
                				to: "programacion2@ptree.com.mx,",
                				subject : "+Arte - Recordatorio, pago de inscripción",
                				text : "Alumno dado de alta",
                        		html : 
"<HTML><BODY>"
+"<div style='background:#2b6ca3; width: 80%; margin-left:10%; display: block; text-size:20px; text-align:center; font-weight:bold; color:#ffffff; border: 1px solid #3b577c; border-radius: 5px;'>"
+"<p> Recordatorio de pago</p>"     
+"</div>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; float:left; padding-left:10%'>Le recordamos que el alumno(a) "+ asistenciaToUpdate.Nombre +" asiste a clases pero presenta adeudo en su pago de inscripción, le invitamos a realizar el pago a la brevedad para evitar contratiempos, Gracias.</p>"
+ "<p style='width:40%; font-size:16px; font-weight:bold; color:#4B494D; padding-left:10%;'><img style='width:100px' src='https://static.wixstatic.com/media/13f72d_578adf481c7346faa6f2d394418e754b.png/v1/fill/w_131,h_95,al_c,usm_0.66_1.00_0.01/13f72d_578adf481c7346faa6f2d394418e754b.png'> - +Arte</p>"
+ "<div  style= 'margin-left:10%; background:#2b6ca3; width: 80%; height: 5px; display: block; '></div>"
+ "</HTML></BODY>",
                			};
                			transporter.sendMail(mailOptions, function(error, info){
                                if(error){
                                    return callback(error);
                                }
                            });                   
                            //fin envio de email 
                }
                else
                {
                    console.log("No envia correo");
                }                   
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina update     
    updateFalta: function(updateId, updateAsistencia, callback){
        console.log(updateId);
        console.log(updateAsistencia);
        asistenciaModel.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.Clase1 = "Falta";
            asistenciaToUpdate.Fecha1 = new Date();

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina updateFalta
    updateFalta2: function(updateId, updateAsistencia, callback){
        console.log(updateId);
        console.log(updateAsistencia);
        asistenciaModel.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.Clase2 = "Falta";
            asistenciaToUpdate.Fecha2 = new Date();

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina updateFalta
    updateFalta3: function(updateId, updateAsistencia, callback){
        console.log(updateId);
        console.log(updateAsistencia);
        asistenciaModel.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.Clase3 = "Falta";
            asistenciaToUpdate.Fecha3 = new Date();

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina updateFalta
    updateFalta4: function(updateId, updateAsistencia, callback){
        console.log(updateId);
        console.log(updateAsistencia);
        asistenciaModel.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.Clase4 = "Falta";
            asistenciaToUpdate.Fecha4 = new Date();

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina updateFalta  
    updateMaestro: function(updateId, updateAsistencia, callback){
        console.log(updateId);
        console.log(updateAsistencia);
        asistenciaModelMaestro.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.Clase1 = "Asistio";
            asistenciaToUpdate.Fecha1 = new Date();

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina update 
    updateMaestro2: function(updateId, updateAsistencia, callback){
        console.log(updateId);
        console.log(updateAsistencia);
        asistenciaModelMaestro.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.Clase2 = "Asistio";
            asistenciaToUpdate.Fecha2 = new Date();

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina update 
    updateMaestro3: function(updateId, updateAsistencia, callback){
        console.log(updateId);
        console.log(updateAsistencia);
        asistenciaModelMaestro.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.Clase3 = "Asistio";
            asistenciaToUpdate.Fecha3 = new Date();

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina update 
    updateMaestro4: function(updateId, updateAsistencia, callback){
        console.log(updateId);
        console.log(updateAsistencia);
        asistenciaModelMaestro.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.Clase4 = "Asistio";
            asistenciaToUpdate.Fecha4 = new Date();

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina update maestro    
    updateFaltaMaestro: function(updateId, updateAsistencia, callback){
        console.log(updateId);
        console.log(updateAsistencia);
        asistenciaModelMaestro.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.Clase1 = "Falta";
            asistenciaToUpdate.Fecha1 = new Date();

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina updateFalta
    updateFaltaMaestro2: function(updateId, updateAsistencia, callback){
        console.log(updateId);
        console.log(updateAsistencia);
        asistenciaModelMaestro.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.Clase2 = "Falta";
            asistenciaToUpdate.Fecha2 = new Date();

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina updateFalta
    updateFaltaMaestro3: function(updateId, updateAsistencia, callback){
        console.log(updateId);
        console.log(updateAsistencia);
        asistenciaModelMaestro.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.Clase3 = "Falta";
            asistenciaToUpdate.Fecha3 = new Date();

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina updateFalta
    updateFaltaMaestro4: function(updateId, updateAsistencia, callback){
        console.log(updateId);
        console.log(updateAsistencia);
        asistenciaModelMaestro.findOne({_id: updateId},function(error,asistenciaToUpdate){
           if(error)
           {
               return callback(error, null);
           }
           //CAMPOS QUE SE EDITAN
            asistenciaToUpdate.Clase4 = "Falta";
            asistenciaToUpdate.Fecha4 = new Date();

                asistenciaToUpdate.save(function(error,asistenciaGuardado){
                   if(error) 
                   {
                    return callback(error, null);   
                   }
                   
                   callback(null, asistenciaGuardado);
                   
                });            

        });
 
    }, //termina updateFalta maestro    
}
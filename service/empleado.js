var EmpleadoModel = require('../models/empleado');
var SolicitudModel = require('../models/solicitud');
var ValidacionesEmpleado = require('../models/empleadoValidaciones');

module.exports = {
    create: function(empleado, solicitud, next){
        var newEmpleado = new EmpleadoModel(empleado);
        var e = newEmpleado;
        var sFechaNacimiento = e.nacimiento.fecha.getFullYear() + "/" + (e.nacimiento.fecha.getMonth() + 1) + "/" + e.nacimiento.fecha.getDate();
        if(!ValidacionesEmpleado.rfc(e.identificaciones.rfc, e.nombre.appaterno, 
                e.nombre.apmaterno, e.nombre.nombre, sFechaNacimiento)){
            return next({
                code: -1,
                message: "Existe un error en el RFC."
            });
        }
        if(!ValidacionesEmpleado.curp(e.identificaciones.curp, e.nombre.appaterno, 
                e.nombre.apmaterno, e.nombre.nombre, sFechaNacimiento)){
            return next({
                code: -1,
                message: "Existe un error en el CURP."
            });
        }
        
        
        var newSolicitud = new SolicitudModel(solicitud);
        newSolicitud.save(function(err, solCreated){
            if(err){
                next(err);
            } else {
                newEmpleado.idSolicitud = solCreated.id;
                newEmpleado.falta = Date.now();
                newEmpleado.save(function(err, empleado){
                    if(err){
                        if(err.name === "ValidationError"){
                            return next({
                                code: -2,
                                message: "Los siguientes campos contienen información incorrecta:",
                                error: err
                            });
                        }
                        return next({
                            code: -1,
                            message: "Ocurrió un error al intentar crear el usuario.",
                            error: err
                        });
                    } else {
                        odooService.cargaEmpleado(6, empleado, function(err, res){
                            if(err){ return next(err) } 
                            return next(null, empleado);
                        });
                    }
                });
            }
        });
    },
    listByEmpresa: function(empresa, next){
        EmpleadoModel.find({}, function(err, empleados){
            if(err){
                console.log(err);
                next({
                    code: -1,
                    message: "Error al buscar",
                    error: err
                });
            } else {
                next(null, empleados);
            }
        });
    },
    listByFilters: function(filters, callback){
        EmpleadoModel.find(filters, function (err, result) {
		  var Data = {};
		  if (err) {
		      callback(err);
		  } else if (result.length) {
		      callback(null, result);
		  } else {
			callback(null, []);
		  }
		});
    },
    createMasivo: function(list, idSolicitud, empresa, centro, callback){
        var campos = new camposConstructor();
        for(var i in list){
            var data = list[i];
            var empleado = {};
            for(var campo in campos){
                if(campo === "tipoNomina"){
                    var nomina = centro;
                    Object.byString(empleado, campos[campo], nomina);
                } else {
                    Object.byString(empleado, campos[campo], data[campo]);
                }
            }
            var errores = false;
            empleado.empresa = empresa;
            empleado.centro = centro;
            empleado.falta = Date.now();
            nuevoEmpleado(empleado, idSolicitud, function(err, empleadoCreated){
                if(err){
                    errores = true;
                }
            });
        }
        if(errores){
            callback({estatus:"error"});
        } else {
            callback(null);
        }
    },
    delete: function(id, callback){
        EmpleadoModel.remove({_id: id}, function(err,removed) {
            if(err){
                callback(err);
            } else {
                callback(null,removed);
            }
        });
    },
    findById: function(id, callback){
        EmpleadoModel.findOne({ _id: id}, function(err, empleado){
            if(err){
                return callback(err);
            }
            return callback(null, empleado);
        });
    },
    update: function(empleado, callback){
        var e = empleado;
        var sFechaNacimiento = e.nacimiento.fecha;
        if(!ValidacionesEmpleado.rfc(e.identificaciones.rfc, e.nombre.appaterno, 
                e.nombre.apmaterno, e.nombre.nombre, sFechaNacimiento)){
            return callback({
                code: -1,
                message: "Existe un error en el RFC.",
                name: 'ValidationError'
            });
        }
        if(!ValidacionesEmpleado.curp(e.identificaciones.curp, e.nombre.appaterno, 
                e.nombre.apmaterno, e.nombre.nombre, sFechaNacimiento)){
            return callback({
                code: -1,
                message: "Existe un error en el CURP.",
                name: 'ValidationError'
            });
        }
        var id = empleado._id;
        this.findById(id, function(err, empOld){
           if(err){
               console.log(err);
               return callback(err);
           }  else {
               empOld.nombre = empleado.nombre;
               empOld.entlaboral = empleado.entlaboral;
               empOld.banco = empleado.banco;
               empOld.nacimiento = empleado.nacimiento;
               empOld.edoCivil = empleado.edoCivil;
               empOld.sexo = empleado.sexo;
               empOld.afiliacion = empleado.afiliacion;
               empOld.salarios = empleado.salarios;
               empOld.identificaciones = empleado.identificaciones;
               empOld.direccion = empleado.direccion;
               empOld.telefono = empleado.telefono;
               empOld.email = empleado.email;
               empOld.tipoNomina = empleado.tipoNomina;
               empOld.falta = empleado.falta;
               empOld.idSolicitud = empleado.idSolicitud;
               empOld.empresa = empleado.empresa;
               empOld.centro = empleado.centro;
               empOld.save(function(err, empNew){
                   if(err){
                       console.log(err);
                       callback(err);
                   } else {
                       callback(null, empNew);
                   }
               });
           }
        });
    },
}

var camposConstructor = function(){
    this.fecha ="falta";
    this.idSolicitud = "idSolicitud";
    this.appaterno ="nombre.appaterno";
    this.apmaterno ="nombre.apmaterno";
    this.nombre ="nombre.nombre";
    this.ubicacion ="entlaboral.ubicacion";
    this.depto ="entlaboral.depto";
    this.puesto ="entlaboral.puesto";
    this.fcontratacion ="entlaboral.fcontratacion";
    this.fantiguedad ="entlaboral.fantiguedad";
    this.banco ="banco.bid";
    this.cuenta ="banco.cuenta";
    this.clabe ="banco.clabe";
    this.fnacimiento ="nacimiento.fecha";
    this.lugarnac ="nacimiento.idLugar";
    this.sexo ="sexo";
    this.edoCivil ="edoCivil";
    this.afiliacion ="afiliacion";
    this.salmensual ="salarios.mensual";
    this.salbase = "salarios.base";
    this.sdi = "salarios.sdi";
    this.rfc = "identificaciones.rfc";
    this.curp = "identificaciones.curp";
    this.calle = "direccion.calle";
    this.noext = "direccion.noext";
    this.noint ="direccion.noint";
    this.colonia ="direccion.colonia";
    this.cp ="direccion.cp";
    this.ciudad ="direccion.ciudad";
    this.estado ="direccion.estado";
    this.telefono ="telefono";
    this.tipoNomina ="tipoNomina";
    this.email ="email";
}

Object.byString = function(o, s, valor) {
    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    s = s.replace(/^\./, '');           // strip a leading dot
    var a = s.split('.');
    if(a.length === 2){
        if(o[a[0]] == undefined){
            o[a[0]] = {};    
        }
        o[a[0]][a[1]] = valor;
    } else if(a.length === 1) {
        o[a[0]] = valor;
    }
    
    return o;
}

function nuevoEmpleado(empleado, idSolicitud, next){
    var newEmpleado = new EmpleadoModel(empleado);
    
    newEmpleado.idSolicitud = idSolicitud;
    newEmpleado.save(function(err, empleado){
        if(err){
            next(err);
        } else {
            next(null, empleado);
        }
    });
}
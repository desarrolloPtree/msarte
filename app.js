var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');

var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var session = require('express-session');
var passport = require('passport');
var flash    = require('connect-flash');

var configExternalAuth = require('./config/externalauth');
var index = require('./routes/index');
var setup = require('./config/setup');

var app = express();
var config = require('./config');

app.set('dbURL', config.db[app.settings.env] || 'mongodb://ptree:ptreema@ds139446.mlab.com:39446/masarte');


var mongoose = require('mongoose');
mongoose.connect(app.get('dbURL'), function(){});

const MongoStore = require('connect-mongo')(session);

var base = __dirname + '/base';
var uploads = __dirname + '/uploads';
var incidencias = base + '/incidencias';
setup(base, uploads, incidencias, function(){
  var users = require('./routes/users');
  //var empresas = require('./routes/empresas');
  var empleados = require('./routes/empleados');
  //var cencostos = require('./routes/cencostos');
  var dashboard = require('./routes/dashboard');
  var mesaAyuda = require('./routes/mesaAyuda');
  var clientes = require('./routes/clientes');
  var proveedores = require('./routes/proveedores');
  var periodos = require('./routes/periodos');
  var incidencias = require('./routes/incidencias');
  var importaciones = require('./routes/importaciones');
  var generales = require('./routes/generales');
  var ctrlempresas = require('./routes/ctrlempresas');
  var regservicio = require('./routes/regservicio');
  var ingresos = require('./routes/ingresos');
  var egresos = require('./routes/egresos');
  var presupuesto = require('./routes/presupuesto');
  var franquicias = require('./routes/franquicias');
  var productos = require('./routes/productos');
  var mobiliario = require('./routes/mobiliario');
  var alumnos = require('./routes/alumnos');
  var maestros = require('./routes/maestros');
  var asistencia = require('./routes/asistencia');
  var taller = require('./routes/taller');//el servidor reconoce la ruta 
  var QRAsistencia = require('./routes/QRAsistencia');//el servidor reconoce la ruta 
  
  var auth = require('./routes/auth')(passport);
  
  
  var configAuthEnv = configExternalAuth[app.settings.env];
  require('./config/passport')(passport, configAuthEnv)
  
  // view engine setup
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'jade');

  // uncomment after placing your favicon in /public
  //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
  app.use(logger('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(cookieParser());
  
  app.use(session({ secret: 'inexbyptreejcss', resave: false, saveUninitialized: true, 
    cookie: { maxAge: 60000 * 10},// cambia el tiempo de inicio de sesion
    store: new MongoStore({ 
      mongooseConnection: mongoose.connection,
      autoRemove: 'interval',
      autoRemoveInterval: 1, // In minutes. Default
    })
  }));
  
  app.use(function(req, res, next) {
    req.session._garbage = Date();
    req.session.touch();
    next();
  });
  
  app.use(passport.initialize());
  app.use(passport.session()); // persistent login sessions
  app.use(flash());
  
  app.use(express.static(path.join(__dirname, 'public')));
  
  
  app.use('/', index);
  app.use('/auth', auth);
  
  
  //Validación de las api, mandar mensaje correctos
  app.use("*/api/*", function(req, res, next){
    if(req.isAuthenticated()){
      next();
    } else {
        res.send({code: -10, mensaje: "Su sesión ha expirado."});
    }
  });
  
  //Validación de la autenticación para cualquier ruta
  //Se colocan las variables del usuario si está autenticado.
  app.use("/*", function(req, res, next){
    if(req.isAuthenticated()){
        if(req.user.google.name !== "" && req.user.google.name !== null && req.user.google.name !== undefined){
          app.locals.nombre = req.user.google.name;
          app.locals.user = req.user.google;
        } else {
          app.locals.nombre = req.user.local.name;
          app.locals.user = req.user.local;
          res.cookie('user', app.locals.nombre); // se coloca la cookie en la variable name
        }
        next();
    } else {
        res.redirect("/");
    }
  });
  
  //Validación del estatus
  app.use("/*", function(req, res, next){
    var user = app.locals.user;
    if(user.estatus === "A"){
      next();
    } else if(user.estatus === "N"){
      if(req.baseUrl === "/users/api/updatepass"){
        next();
      } else {
        res.render("users/actualizarpass");
      }
    } else {
      //TODO Estatus inválido, terminar la sesion
      next();
    }
  });
  
  
  app.use('/users/*', function(req, res, next){
    app.locals.menu = 'users';
    next();
  });
  app.use('/users', users);
  app.use('/empleados/*', function(req, res, next){
    app.locals.menu = 'empleados';
    next();
  });
  app.use('/empleados', empleados);
  app.use('/empresas/*', function(req, res, next){
    app.locals.menu = 'empresas';
    next();
  });
 /* app.use('/empresas', empresas);
  app.use('/cencostos/*', function(req, res, next){
    app.locals.menu = 'cencostos';
    next();
  });*/
  
  app.use('/dashboard', dashboard);
  app.use('/dashboard/*', function(req, res, next){
    app.locals.menu = 'dashboard';
    next();
  });
  app.use('/dashboard', dashboard);
  
  app.use('/mesaAyuda/*', function(req, res, next){
    app.locals.menu = 'mesaAyuda';
    next();
  });
  app.use('/mesaAyuda', mesaAyuda);
  
  app.use('/clientes/*', function(req, res, next){
    app.locals.menu = 'clientes';
    next();
  });
  app.use('/clientes', clientes);  
  
  app.use('/proveedores/*', function(req, res, next){
    app.locals.menu = 'proveedores';
    next();
  });
  app.use('/proveedores', proveedores);    
  
  app.use('/regservicio/*', function(req, res, next){
    app.locals.menu = 'regservicio';
    next();
  });
  app.use('/regservicio', regservicio);  
  
  app.use('/ingresos/*', function(req, res, next){
    app.locals.menu = 'ingresos';
    next();
  });
  app.use('/ingresos', ingresos);    
  
  app.use('/egresos/*', function(req, res, next){
    app.locals.menu = 'egresos';
    next();
  });
  app.use('/egresos', egresos); 
  
  app.use('/presupuesto/*', function(req, res, next){
    app.locals.menu = 'presupuesto';
    next();
  });
  app.use('/presupuesto', presupuesto);   
  
  app.use('/franquicias/*', function(req, res, next){
    app.locals.menu = 'franquicias';
    next();
  });
  app.use('/franquicias', franquicias);
  
  app.use('/productos/*', function(req, res, next){
    app.locals.menu = 'productos';
    next();
  });
  app.use('/productos', productos); 
  
  app.use('/mobiliario/*', function(req, res, next){
    app.locals.menu = 'mobiliario';
    next();
  });
  app.use('/mobiliario', mobiliario); 
  
  app.use('/alumnos/*', function(req, res, next){
    app.locals.menu = 'alumnos';
    next();
  });
  app.use('/alumnos', alumnos); 
  
  app.use('/asistencia/*', function(req, res, next){
    app.locals.menu = 'asistencia';
    next();
  });
  app.use('/asistencia', asistencia); 
  
  app.use('/taller/*', function(req, res, next){
    app.locals.menu = 'taller';
    next();
  });
  app.use('/taller', taller);// el servidor reconoce la ruta 
  
  app.use('/QRAsistencia', function(req, res, next){
    app.locals.menu = 'QRAsistencia';
    next();
  });
  app.use('/QRAsistencia', QRAsistencia);// el servidor reconoce la ruta
  
  app.use('/maestros/*', function(req, res, next){
    app.locals.menu = 'maestros';
    next();
  });
  app.use('/maestros', maestros);   
  
  app.use('/periodos/*', function(req, res, next){
    app.locals.menu = 'periodos';
    next();
  });
  app.use('/periodos', periodos);
  app.use('/incidencias/*', function(req, res, next){
    app.locals.menu = 'incidencias';
    next();
  });
  app.use('/incidencias', incidencias);
  app.use('/importaciones/*', function(req, res, next){
    app.locals.menu = 'importaciones';
    next();
  });
  app.use('/importaciones', importaciones);
  app.use('/ctrlempresas/*', function(req, res, next){
    app.locals.menu = 'ctrlempresas';
    next();
  });
  app.use('/ctrlempresas', ctrlempresas);
  app.use('/generales/*', function(req, res, next){
    next();
  });
  app.use('/generales', generales);
  
  app.get('/cookie',function(req, res){
     res.cookie('name' , 'app.locals.nombre').send('Cookie is set');
  });
  
  app.locals.version = config.version;
  
  
  // catch 404 and forward to error handler
  app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });
  
  // error handler
  app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
  
    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });
});

  
module.exports = app;

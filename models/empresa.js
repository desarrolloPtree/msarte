var mongoose = require("mongoose");

var EmpresaSchema = mongoose.Schema({
    nombre: {
        type: String
    },
    id: {
        type: Number,
        index: true
    }
});

var Empresa = mongoose.model('empresa', EmpresaSchema);

module.exports = Empresa;
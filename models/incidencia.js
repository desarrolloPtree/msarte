var mongoose = require("mongoose");

var IncidenciaSchema = mongoose.Schema({
    noEmpleado: {
        type: String,
    },
    tipo: {
        type: String,
    },
    descripcion: {
        type: String,
    },
})

var Incidencia = mongoose.model('incidencia', IncidenciaSchema);

module.exports = Incidencia;
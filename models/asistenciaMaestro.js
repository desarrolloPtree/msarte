//Modelo
var mongoose = require('mongoose');

var asistenciamaestroSchema = mongoose.Schema({
    Nombre:{
        type: String
    },
    Clase1:{
        type: String   
    },
    Fecha1:{
        type: Date
    },
    Clase2:{
        type: String   
    },
    Fecha2:{
        type: Date
    },    
    Clase3:{
        type: String   
    },
    Fecha3:{
        type: Date
    },    
    Clase4:{
        type: String   
    },
    Fecha4:{
        type: Date
    }
});

var asistenciamaestro = mongoose.model('asistenciamaestro', asistenciamaestroSchema);  //data access object DAO

module.exports = asistenciamaestro;
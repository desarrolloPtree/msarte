module.exports = function(){
    this.noEmpleado = {
        value: null,
        etiqueta: "Número de Empleado",
        condicion:  function(valor){
                        return /.+/.test(valor)
                    }
    };
    this.tipo = {
        value: null,
        etiqueta: "Tipo",
        condicion:  function(valor){
                        return /.+/.test(valor)
                    } 
    };
    this.descripcion = {
        value: null,
        etiqueta: "Descripción",
        condicion:  function(valor){
                        return /.+/.test(valor)
                    }
    }
}
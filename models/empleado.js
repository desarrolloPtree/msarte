var mongoose = require('mongoose');
var validaciones = require('./empleadoValidaciones');

var EmpleadoSchema = mongoose.Schema({
    nombre: {
        nombre: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.nombre,
                message: '{VALUE} contiene caracteres inválidos.'
            },
        },
        appaterno: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.appaterno,
                message: '{VALUE} contiene caracteres inválidos.'
            },
        },
        apmaterno: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.nombre,
                message: '{VALUE} contiene caracteres inválidos.'
            },
        }
    },
    entlaboral: {
        ubicacion:{
            type: String,
            required: true,
            validate: {
                validator: validaciones.ubicacion,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
        depto: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.depto,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
        puesto: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.puesto,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
        fcontratacion: {
            type: Date,
            required: true,
            validate: {
                validator: validaciones.fcontratacion,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
        fantiguedad: {
            type: Date,
            required: true,
            validate: {
                validator: validaciones.fantiguedad,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        }
    },
    banco:{
        bid: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.banco,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
        cuenta: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.cuenta,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
        clabe: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.clabe,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        }
    },
    nacimiento:{
        fecha: {
            type: Date,
            required: true,
            validate: {
                validator: validaciones.fnacimiento,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
        idLugar: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.lugarnac,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        }
    },
    edoCivil: {
        type: String,
        required: true,
        validate: {
                validator: validaciones.edoCivil,
                message: '{VALUE} contiene caracteres inválidos.'
            }
    },
    sexo: {
        type: String,
        required: true,
        validate: {
                validator: validaciones.sexo,
                message: '{VALUE} contiene caracteres inválidos.'
            }
    },
    afiliacion: {
        type: String,
        required: true,
        validate: {
                validator: validaciones.afiliacion,
                message: '{VALUE} contiene caracteres inválidos.'
            }
    },
    salarios: {
        base: {
            type: Number,
            required: true,
            validate: {
                validator: validaciones.salbase,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
        
        mensual: {
            type: Number,
            required: true,
            validate: {
                validator: validaciones.salmensual,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
        sdi: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.sdi,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
    },
    identificaciones: {
        rfc: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.rfcMongoose,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
        curp: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.curpMongoose,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        }
    },
    direccion:{
        calle: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.calle,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
        noext: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.noext,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
        noint: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.noint,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
        colonia: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.colonia,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
        cp: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.cp,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
        estado: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.estado,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
        ciudad: {
            type: String,
            required: true,
            validate: {
                validator: validaciones.ciudad,
                message: '{VALUE} contiene caracteres inválidos.'
            }
        },
    },
    telefono: {
        type: String,
        required: true,
        validate: {
            validator: validaciones.telefono,
            message: '{VALUE} contiene caracteres inválidos.'
        }
    },
    email: {
        type: String,
        required: true,
        validate: {
            validator: validaciones.email,
            message: '{VALUE} contiene caracteres inválidos.'
        }
    },
    tipoNomina: {
        type: mongoose.Schema.Types.Mixed,
        required: true,
    },
    falta: {
        type: Date,
        //required: true,
    },
    idSolicitud:{
        type: mongoose.Schema.Types.ObjectId,
        //required: true,
        ref: 'solicitud',
        index: true,
    }, 
    empresa:{
        type: mongoose.Schema.Types.Mixed,
        required: true,
    },
    centro:{
        type: mongoose.Schema.Types.Mixed,
        required: true,
    }
});

var Empleado = mongoose.model('empleado', EmpleadoSchema);

module.exports = Empleado;
var mongoose = require("mongoose");


var SolicitudSchema = mongoose.Schema({
    fsolicitud: {
        type: Date,
        required: true,
    },
    idUsuario: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'user',
        index: true
    },
    idEmpresa: {
        type: String,
    },
    idCentro: {
        type: String,
    },
});

var Importacion = mongoose.model('solicitud', SolicitudSchema);

module.exports = Importacion;
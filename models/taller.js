//Modelo
var mongoose = require('mongoose');

var tallerSchema = mongoose.Schema({
    NombreTaller:{
        type: String
    },
    TipoTaller:{
        type: String
    },
    InicioHorario:{
        type: String
    },
    FinHorario:{
        type: String
    },
    Dias:{
        type: String
    },
    Cupo:{
        type: String
    },
    Descripcion:{
        type: String
    },
     producto:[{
        idProducto: {type:String},
        cantidadProducto: {type:String},
        nombreProducto: {type:String},
    }]
    
    
});

var taller = mongoose.model('taller', tallerSchema);  //data access object DAO

module.exports = taller;
var rfcUtils = require("../helpers/rfc.js");
var clabeUtils = require("../helpers/clabe.js");

module.exports = {
    consecutivo: function(valor){
        return /^[0-9]+$/.test(valor);
    },
    estatus: function(valor){
        return /.+/.test(valor)
    },
    fecha: function(valor){
        return /.+/.test(valor)
    },
    appaterno: function(valor){
        return /^([a-zA-ZñÑáÁéÉÁíÍóÓúÚ]+\s*[a-zA-ZñÑáÁéÉÁíÍóÓúÚ]*)+$/.test(valor)
    },
    apmaterno: function(valor){
        return /^([a-zA-ZñÑáÁéÉÁíÍóÓúÚ]+\s*[a-zA-ZñÑáÁéÉÁíÍóÓúÚ]*)+$/.test(valor)
    },
    nombre: function(valor){
        return /^([a-zA-ZñÑáÁéÉÁíÍóÓúÚ]+\s*[a-zA-ZñÑáÁéÉÁíÍóÓúÚ]*)+$/.test(valor)
    },
    ubicacion: function(valor){
        return /^([a-zA-Z0-9ñÑáÁéÉÁíÍóÓúÚ]+\s*[a-zA-Z0-9ñÑáÁéÉÁíÍóÓúÚ]*)+$/.test(valor)
    },
    depto: function(valor){
        return /^([a-zA-Z0-9ñÑáÁéÉÁíÍóÓúÚ]+\s*[a-zA-Z0-9ñÑáÁéÉÁíÍóÓúÚ]*)+$/.test(valor)
    },
    puesto: function(valor){
        return /^([a-zA-Z0-9ñÑáÁéÉÁíÍóÓúÚ]+\s*[a-zA-Z0-9ñÑáÁéÉÁíÍóÓúÚ]*)+$/.test(valor)
    },
    fcontratacion: function(valor){
        return /.+/.test(valor)
    },
    fantiguedad: function(valor){
        return /.+/.test(valor)
    },
    banco: function(valor){
        return /^([a-zA-Z0-9]+\s*[a-zA-Z0-9]*)+$/.test(valor)
    },
    cuenta: function(valor){
        return /^[0-9]+$/.test(valor)
    },
    clabe: function(valor){
        if(/^([0-9]){18}$/.test(valor)){
            return clabeUtils.validacionCLABE(valor.split(""));
        }
        return false;
    },
    fnacimiento: function(valor){
        return /.+/.test(valor)
    },
    lugarnac: function(valor){
        return /^([a-zA-Z0-9ñÑáÁéÉÁíÍóÓúÚ]+\s*[a-zA-Z0-9ñÑáÁéÉÁíÍóÓúÚ]*)+$/.test(valor)
    },
    sexo: function(valor){
        return /^([a-zA-Z]+\s*[a-zA-Z]*)+$/.test(valor)
    },
    edoCivil: function(valor){
        return /^([a-zA-Z]+\s*[a-zA-Z]*)+$/.test(valor)
    },
    afiliacion: function(valor){
        return /^([0-9]{11})$/.test(valor)
    },
    salmensual: function(valor){
        if(/^(\d+\.?\d*|\.\d+)$/.test(valor)){
            if((valor + 0) > 0) return true;
        }
        return false;
    },
    salbase: function(valor){
        if(/^(\d+\.?\d*|\.\d+)$/.test(valor)){
            if((valor + 0) > 0) return true;
        }
        return false;    },
    sdi: function(valor){
        if(/^(\d+\.?\d*|\.\d+)$/.test(valor)){
            if((valor + 0) > 0) return true;
        }
        return false;
    },
    rfc: function(valor, appaterno, apmaterno, nombre, fnacimiento){
        if(/^([a-zA-Z0-9]{13})$/.test(valor)){
            var valido = rfcUtils.validarRFC(valor.substr(0, 10), fnacimiento, nombre, appaterno, apmaterno);
            return valido;
        }
        return false;
    },
    rfcMongoose: function(valor){
        return /^([a-zA-Z0-9]{13})$/.test(valor)
    },
    curp: function(valor, appaterno, apmaterno, nombre, fnacimiento){
        if(/^([a-zA-Z0-9]{18})$/.test(valor)){
            var valido = rfcUtils.validarRFC(valor.substr(0, 10), fnacimiento, nombre, appaterno, apmaterno);
            return valido;
        }
        return false;
    },
    curpMongoose: function(valor){
        return /^([a-zA-Z0-9]{18})$/.test(valor)
    },
    calle: function(valor){
        return /^([a-zA-Z0-9ñÑáÁéÉÁíÍóÓúÚ]+\s*[a-zA-Z0-9ñÑáÁéÉÁíÍóÓúÚ]*)+$/.test(valor)
    },
    noext: function(valor){
        return /^([a-zA-Z0-9]+\s*[a-zA-Z0-9]*)+$/.test(valor)
    },
    noint: function(valor){
        return /^([a-zA-Z0-9]+\s*[a-zA-Z0-9]*)+$/.test(valor)
    },
    colonia: function(valor){
        return /^([a-zA-Z0-9ñÑáÁéÉÁíÍóÓúÚ]+\s*[a-zA-Z0-9ñÑáÁéÉÁíÍóÓúÚ]*)+$/.test(valor)
    },
    cp: function(valor){
        return /(^[0-9]{5})$/.test(valor)
    },
    ciudad: function(valor){
        return /^([a-zA-Z0-9ñÑáÁéÉÁíÍóÓúÚ]+\s*[a-zA-Z0-9ñÑáÁéÉÁíÍóÓúÚ]*)+$/.test(valor)
    },
    estado: function(valor){
        return /^([a-zA-Z0-9ñÑáÁéÉÁíÍóÓúÚ]+\s*[a-zA-Z0-9ñÑáÁéÉÁíÍóÓúÚ]*)+$/.test(valor)
    },
    telefono: function(valor){
        return /^([0-9]+\s*[0-9]*)+$/.test(valor)
    },
    tipoNomina: function(valor){
        return /^([a-zA-Z0-9]+\s*[a-zA-Z0-9]*)+$/.test(valor)
    },
    email: function(valor){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(valor)
    },
}
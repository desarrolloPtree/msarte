//Modelo
var mongoose = require('mongoose');

var asistenciaSchema = mongoose.Schema({
    NombreAlumno:{
        type: String
    },
    Taller:{
        type: String   
    },
    Pin:{
        type: String   
    },
    Asistencias:[{
        fecha: {type: String},
        Estatus: {type: String}
    }]
    
});

var asistencia = mongoose.model('asistencia', asistenciaSchema);  //data access object DAO

module.exports = asistencia;
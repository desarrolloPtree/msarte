//Modelo
var mongoose = require('mongoose');

var pagosSchema = mongoose.Schema({
    Concepto:[{
        Pagode:{type:String}
    }],
    Alumno:{
        type: String
    },
    FormaPago:{
        type: String  
    },
    Descuento:{
        type: String
    },
    PagoIva:{
        type: String
    },
    TotalPago:{
        type: String
    },
     allProductos:[{
        idProducto:{type:String},
        cantidadCompra:{type:String},
    }],
    
    

    
});

var pagos = mongoose.model('pagos', pagosSchema);  //data access object DAO

module.exports = pagos;
var mongoose = require("mongoose");


var ImportSchema = mongoose.Schema({
    archivo: {
        type: String,
    },
    fimportacion: {
        type: Date,
    },
    idUsuario: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        index: true
    },
    idEmpresa: {
        type: String,
    },
    idCentro: {
        type: String,
    },
    idDriveArchivo: {
        type: String,
    },
    idDriveEmpresa: {
        type: String,
    },
    idDriveCentro: {
        type: String,
    },
});

var Importacion = mongoose.model('importacion', ImportSchema);

module.exports = Importacion;
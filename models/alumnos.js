//Modelo
var mongoose = require('mongoose');

var alumnosSchema = mongoose.Schema({
    Nombre:{
        type: String
    },
    Apellidos:{
        type: String
    },
    Curp:{
        type: String
    },
    Edad:{
        type: String
    },
    Calle:{
        type: String
    },
    NoExt:{
        type: String
    },
    NoInt:{
        type: String
    },    
    Colonia:{
        type: String
    },
    Municipio:{
        type: String
    },
    Entidad:{
        type: String
    },
    Cp:{
        type: String
    },
    Telefono:{
        type: String
    },
    Movil:{
        type: String
    },
    Email:{
        type: String
    },
    Facebook:{
        type: String
    }, 
    Instagram:{
        type: String
    },    
    Estatus:{
        type: String
    },
    Pin:{
        type: String
    }, 
    Foto:{
        type: String
    },    
	idFranq:{
		type:String,
	},
	nombreFranq:{
		type:String,
	},   
	idTaller:{
		type:String,
	},
	taller:{
        type: String
    },
    NombreTutor:{
        type: String
    },
    ApellidosTutor:{
        type: String
    },  
    CurpTutor:{
        type: String
    },  
    Rfc:{
        type: String
    },
    TelefonoTutor:{
        type: String
    },
    MovilTutor:{
        type: String
    },
    EmailTutor:{
        type: String
    },
    FirmaTutor:{
        type: String
    },
    FechaAlta:{
        type:String
    },
    ComentarioBaja:[{
        Fecha: {type:Date},
        Comentario: {type:String},
    }]   
});

var alumnos = mongoose.model('alumnos', alumnosSchema);  //data access object DAO

module.exports = alumnos;
//Modelo
var mongoose = require('mongoose');

var maestrosSchema = mongoose.Schema({
    FechaIng:{
        type: Date
    },
    Nombre:{
        type: String
    },  
    Apellidos:{
        type: String
    },    
    FechaNac:{
        type: Date
    },
    Edad:{
        type: String
    },
    Calle:{
        type: String
    },
    NoExt:{
        type: String
    },
    NoInt:{
        type: String
    },
    Colonia:{
        type: String
    },
    Cp:{
        type: String
    },
    Delegacion:{
        type: String
    }, 
    Entidad:{
        type: String
    },   
    Telefono:{
        type: String
    },
    Celular:{
        type: String
    }, 
    Email:{
        type: String
    },  
    ContactoE:{
        type: String
    },    
    TelefonoE:{
        type: String
    },
    Parentesco:{
        type: String
    },
    Enfermedad:{
        type: String
    }, 
    Tratamiento:{
        type: String
    },
    Sangre:{
        type: String
    }, 
    Umf:{
        type: String
    },   
    taller:{
        type: String
    },
    franquicia:{
        type: String
    },
    Estatus:{
        type: String  
    },
    ComentarioBaja:[{
        Fecha: {type:Date},
        Comentario: {type:String},
    }]    
});

var maestros = mongoose.model('maestros', maestrosSchema);  //data access object DAO

module.exports = maestros;
//Modelo
var mongoose = require('mongoose');

var productosSchema = mongoose.Schema({
    Codigo: {
        type: String
    },
    Nombre:{
        type: String
    },
    Descripcion:{
        type: String
    },
    Categoria:{
        type: String
    },    
    Existencias:{
        type: Number
    },
    Franq:[{
    	idFranq:{
    		type:String,
    	},
    	nombreFranq:{
    		type:String,
    	}
    }],
    Precio:{
        type: String
    },
    Precioa:{
        type: String
    },
    Totalpp:{
        type: String
    },    
    Totalpa:{
        type: String
    },
    Empaque:{
        type: String
    }    
});

var productos = mongoose.model('productos', productosSchema);  //data access object DAO

module.exports = productos;
//Modelo
var mongoose = require('mongoose');

var mobiliarioSchema = mongoose.Schema({
    Area: {
        type: String
    },
    Nombre:{
        type: String
    },
    Cantidad:{
        type: Number
    },
	idFranq:{
		type:String,
	},
	nombreFranq:{
		type:String,
	},    
    Costo:{
        type: String
    }
});

var mobiliario = mongoose.model('mobiliario', mobiliarioSchema);  //data access object DAO

module.exports = mobiliario;
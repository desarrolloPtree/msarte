//Modelo
var mongoose = require('mongoose');

var solicitudSchema = mongoose.Schema({
    Fecha: {
        type: Date
    },
    Solicitud:{
        type: String
    },
    Estatus:{
        type: String
    },
    Usuario:{
        type: String
    },
    Nomina:{
        type: String
    },
    Periodo:{
        type: String
    },
    Empleado:{
        type: String
    },
    UltimoArchivoId:{
        type: String
    },
    Comentarios:[{
        Fecha: {type:Date},
        LeidoAdmin: {type:String},
        LeidoUser: {type:String},
        Comentario: {type:String},
        Usuario: {type:String}
    }]
});

var solicitudAyuda = mongoose.model('solicitudAyuda', solicitudSchema);  //data access object DAO

module.exports = solicitudAyuda;
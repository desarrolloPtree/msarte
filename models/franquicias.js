//Modelo
var mongoose = require('mongoose');

var franquiciaSchema = mongoose.Schema({
    No: {
        type: String
    },
    Nombre:{
        type: String
    },
    Direccion:{
        type: String
    },
    Estatus:{
        type: String
    },
    FechaAp:{
        type: Date
    },
    contratoPromesa:{
        type: String
    },
    ContratoFranquicia:{
        type: String
    },
    IdentificacionOficial:{
        type: String
    },
    RFC:{
        type: String
    },
    PermisoUsoComercial:{
        type: String
    },
    PermisoApertura:{
        type: String
    },
    ContratoArrendamiento:{
        type: String
    },
    PolizaSeguro:{
        type: String
    },
    LicenciaFuncionamiento:{
        type: String
    },
    CertificadoPC:{
        type: String
    },
    PermisoObra:{
        type: String
    },
    ActaConstitutiva:{
        type: String
    },
    ActosAdministrativos:{
        type: String
    },
    CartaLicitos:{
        type: String
    }
});

var franquicias = mongoose.model('franquicias', franquiciaSchema);  //data access object DAO

module.exports = franquicias;
var mongoose = require('mongoose');
var bcrypt = require("bcrypt-nodejs");

var UserSchema = mongoose.Schema({
	local: {
		username: {
			type: String,
			index: true
		},
		password: {
			type: String
		},
		email: {
			type: String
		},
		name: {
			type: String
		},
		role: {
			type: Number
		},
		properties: {
			telefono: {
				type: Number,
			},
			address: {
				type: String,
			},
			puesto: {
				type: String,
			},
			area: {
				type: String,
			},
			idFranq:{
				type:String,
			},
			nombreFranq:{
				type:String,
			},
			nominas: [{}]
		},
		estatus: {
			type: String
		}
	},
	google: {
		id: {
			type: String
		},
		token: {
			type: String
		},
		email: {
			type: String,
			index: true
		},
		name: {
			type: String
		},
		role: {
			type: Number,
		},
		estatus: {
			type: String
		}
	}
});

UserSchema.methods.validPassword = function(password, callback) {
	bcrypt.compare(password, this.local.password, function(err, res) {
		if(err){
			console.log("Error al desencriptar el password")
			callback(err);
		} else {
			console.log("REPUESTA DESENCRIPT: " + res + "2" + password);
			callback(null, res);
		}
	});
};

var User = mongoose.model('user', UserSchema);

module.exports = User;